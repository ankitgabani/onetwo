//
//  ExistingTeamPlayerVC.swift
//  OneTwo
//
//  Created by appt on 16/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import Contacts

protocol ContactAccess {
    func selectAccessContact()
}
@available(iOS 13.0, *)
class ExistingTeamPlayerVC: BaseViewController {
    
    @IBOutlet weak var accessContactsLbl: UILabel!
    @IBOutlet weak var existingTeamLbl: UILabel!
    @IBOutlet weak var viewOne: UIView!
    @IBOutlet weak var existingPlayerLbl: UILabel!
    var delegate: ContactAccess!
    
    var isNotTeamNow = false
    override func viewDidLoad() {
        super.viewDidLoad()
        SetUPLbl()
        ExixtingPlayerLabelTap()
        ExixtingTeamLabelTap()
        AccessContactLabelTap()
        hideKeyboardWhenTappedAround()
        
        callGetTeamListAPI()
    }
    @IBAction func closeBtn(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func selectionTeamsList(_ sender: UIButton) {
        
    }
    
    func SetUPLbl() {
        viewOne.layer.cornerRadius = 15
        existingTeamLbl.layer.cornerRadius = 19
        existingTeamLbl.layer.borderWidth = 2
        existingTeamLbl.layer.borderColor = UIColor.oceanColor.cgColor
        accessContactsLbl.layer.cornerRadius = 19
        accessContactsLbl.layer.borderWidth = 2
        accessContactsLbl.layer.borderColor = UIColor.oceanColor.cgColor
        existingPlayerLbl.layer.cornerRadius = 19
        existingPlayerLbl.layer.borderWidth = 2
        existingPlayerLbl.layer.borderColor = UIColor.oceanColor.cgColor
    }
    @objc func playerLblTapped(_ sender: UITapGestureRecognizer) {
        
        weak var pvc = self.presentingViewController
        
        self.dismiss(animated: true, completion: {
            let vc = self.storyboard?.instantiateViewController(identifier: "PlayerListBookVC") as! PlayerListBookVC
            vc.modalTransitionStyle = .coverVertical
            vc.modalPresentationStyle = .overCurrentContext
            pvc?.present(vc, animated: true, completion: nil)
        })
        
    }
    @objc func teamLblTapped(_ sender: UITapGestureRecognizer) {
        
        if isNotTeamNow == false {
            
            weak var pvc = self.presentingViewController
            
            self.dismiss(animated: true, completion: {
                let vc = self.storyboard?.instantiateViewController(identifier: "ExistingTeamListVC") as! ExistingTeamListVC
                vc.isFromBooking = true
                vc.modalTransitionStyle = .coverVertical
                vc.modalPresentationStyle = .overCurrentContext
                 pvc?.present(vc, animated: true, completion: nil)
            })
        } else {
            
            weak var pvc = self.presentingViewController
            
            self.dismiss(animated: true, completion: {
                let vc = self.storyboard?.instantiateViewController(identifier: "NoTeamListVC") as! NoTeamListVC
                vc.modalTransitionStyle = .coverVertical
                vc.modalPresentationStyle = .overCurrentContext
                 pvc?.present(vc, animated: true, completion: nil)
            })
            
        }
        
        
    }
    @objc func accessContactLblTapped(_ sender: UITapGestureRecognizer) {
        
        weak var pvc = self.presentingViewController
        
        self.dismiss(animated: true, completion: {
            let store = CNContactStore()
            store.requestAccess(for: .contacts) { granted, error in
                guard granted else {
                    self.presentSettingsAlert()
                    return
                }
                
                // get the contacts
                
                let request = CNContactFetchRequest(keysToFetch: [CNContactFormatter.descriptorForRequiredKeys(for: .fullName), CNContactPhoneNumbersKey as CNKeyDescriptor])
                do {
                    try store.enumerateContacts(with: request) { contact, stop in
                        let name = CNContactFormatter.string(from: contact, style: .fullName)
                        print(name ?? "")
                    }
                    
                    DispatchQueue.main.async {
                        let vc = self.storyboard?.instantiateViewController(identifier: "ContactListVC") as! ContactListVC
                        vc.modalTransitionStyle = .coverVertical
                        vc.modalPresentationStyle = .overCurrentContext
                        pvc?.present(vc, animated: true, completion: nil)
                    }
                    
                    
                } catch {
                    print(error)
                }
                
            }
            
        })
        
        
    }
    
    private func presentSettingsAlert() {
        let settingsURL = URL(string: UIApplication.openSettingsURLString)!
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Permission to Contacts", message: "This app needs access to contacts in order to ...", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Go to Settings", style: .default) { _ in
                UIApplication.shared.openURL(settingsURL)
            })
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
            self.present(alert, animated: true)
        }
    }
    
    func ExixtingPlayerLabelTap() {
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(self.playerLblTapped(_:)))
        self.existingPlayerLbl.isUserInteractionEnabled = true
        self.existingPlayerLbl.addGestureRecognizer(viewTap)
    }
    func ExixtingTeamLabelTap() {
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(self.teamLblTapped(_:)))
        self.existingTeamLbl.isUserInteractionEnabled = true
        self.existingTeamLbl.addGestureRecognizer(viewTap)
    }
    func AccessContactLabelTap() {
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(self.accessContactLblTapped(_:)))
        self.accessContactsLbl.isUserInteractionEnabled = true
        self.accessContactsLbl.addGestureRecognizer(viewTap)
    }
    
    func callGetTeamListAPI() {
           
          let userID = UserDefaults.standard.value(forKey: "user_id") as? Int
           
           self.showLoader()
           
           let param = ["user_id": userID ?? 0]
           
           print(param)
           APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(GET_EXITING_TEAM_LIST, parameters: param, completionHandler: { (response, error, statusCode) in
               
               if error == nil {
                   print("STATUS CODE \(String(describing: statusCode))")
                   print("Response \(String(describing: response))")
                   
                   let message = response?["message"] as? String ?? ""
                   let code = response?["code"] as? Int ?? 0
                   
                   self.hideLoader()
                   if statusCode == 200 {
                       
                       if code == 1 {
                           
                           
                           
                       } else {
                          //self.view.makeToast(message)
                           self.isNotTeamNow = true

                       }
                       
                   } else {
                       
                       self.view.makeToast(message)
                   }
                   
               } else {
                   self.hideLoader()
                   print("Response \(String(describing: response))")
                   let message = response?["message"] as? String ?? ""
                   self.view.makeToast(message)
               }
           })
       }
}
