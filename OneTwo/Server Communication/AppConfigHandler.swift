//
//  AppConfigHandler.swift
//  Steerboost
//
//  Created by Prashant on 18/04/19.
//  Copyright © 2019 Apptology. All rights reserved.
//

import UIKit

enum enumAppConfig:String {
    case baseUrl = "baseUrlDEV"
    case namespace = "namespace"
    case clientAutherization = "clientAuth"
}

struct Platform {
    static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0 // Use this line in Xcode 7 or newer
        //return TARGET_IPHONE_SIMULATOR != 0 // Use this line in Xcode 6
    }
}
struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
}
class AppConfigModel:NSObject{
    var access_token:String? = ""
    var userId:String? = ""
    
}

class AppConfigHandler: NSObject {
    class func valueForKey(key:String) -> Any? {
        var result:Any? = "" as Any?;
        if let path = Bundle.main.path(forResource: "AppConfig", ofType: "plist") {
            if let dictPlist = NSDictionary(contentsOfFile: path){
                result = dictPlist.object(forKey: key)
            }
        }
        return result;
    }
   
    //MARK:- Operations App Setup
    func setupAppConfiguredUser(dictLogin:NSDictionary){
        let token = dictLogin.object(forKey: "api_token") as? String
        print(token ?? "")
        UserDefaults.standard.set(token ?? "", forKey: "auth_token")

    }
    func getAppConfiguredUser() -> AppConfigModel? {
        let appSettings:AppConfigModel = AppConfigModel();
        appSettings.access_token = UserDefaults.standard.string(forKey: "auth_token")

        print("Access Token Saved .. \(String(describing: appSettings.access_token))");
        return appSettings;
    }
    
    // MARK:- DeviceID
    class func getDeviceId() ->String {
        return UserDefaults.standard.string(forKey: "device_ID")! as String
    }
    class func setDeviceId(){

        if Platform.isSimulator {
            UserDefaults.standard.set("SIMULATOR_IPHONE_6_NSTINDIA", forKey: "device_ID")
        }
        else {
    UserDefaults.standard.set(UIDevice.current.identifierForVendor?.uuidString, forKey: "device_ID")
        }
    }
}
