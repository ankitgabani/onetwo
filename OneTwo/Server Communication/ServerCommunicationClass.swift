
//  ServerCommunicationClass.swift
//
//
//  Created by Prashant on 10/09/18.
//  Copyright © 2018 Apptology. All rights reserved.

import Foundation
import Alamofire

//internet connectivity
class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

//------------------------------Server Request Object ----------------------
open class ServerRequest{
    var method:String? = "";
//    var infoParam:NSDictionary = NSDictionary();
    var infoParam:AnyObject? = NSDictionary();

    //var requestType:String = "Multipart";
    
    var requestType:String = "POST";
    var requestHttpType:String = "";
    var operation:String = ""
    var image:UIImage? = nil
    var image_path:UIImage? = nil
    var imgData:Data? = nil;
    var mimeType:String = ""

    var content:AnyObject? = nil;
}
//------------------------------Server Response Object ---------------------
open class ServerResponse{
    var content:AnyObject? = nil;
    var infoResponse:AnyObject?;
    var responseCode:Int?;
    var method:String?;
    var operation:String?;
    var errorMessage:String?
    var errorServerValidation:String?
    var errorCodeDescription:String{
        set{
            
        }
        get{
            var val:String = "response"
                        switch responseCode! {
                        case 400:
                            val = ""
                            if let errorDescription = self.infoResponse!.object(forKey:"message_description") as? String
                            {
                                val = String(format: "%@", errorDescription)
                            }
                            else if let errorDescription = self.infoResponse!.object(forKey:"error_description") as? String{
                                val = String(format: "%@", errorDescription)
                            }
                        default:
                            val = "Server Error"
                        }
            return val;
        }
    }
}
//------------------------------ Protocol ---------------------
protocol ServerCommunicationDelegate : class {
    func didFinishServerCommunicationWithSuccess(_ response:ServerResponse);
    func didFinishServerCommunicationWithError(_ response:ServerResponse);
}

open class ServerCommunicationClass: NSObject, NSURLConnectionDelegate {
    
    weak var delegate:ServerCommunicationDelegate?
    var responseJR:ServerResponse = ServerResponse();
    var urlGlobal:String = "";
    
    convenience init(del:ServerCommunicationDelegate) {
        self.init()
        self.delegate = del;
    }
    
    
    // MARK: - executors NSURLConnection
    open func executeRequerstOnServerURL(_ request : ServerRequest, url:String){
        //1. Collect Info URL
        var link = url
        if !link.contains("http://"){
            link = String(format:"http://%@",url)
        }
        urlGlobal = link;
        
        //2. Execute on Server
        self.executeRequerstOnServer(request);
    }

    open func executeAppleRequerstOnServer(_ request : ServerRequest) {
        let locale = LanguageManager.sharedInstance.getSelectedLocale()
        let appConfigUser = AppConfigHandler().getAppConfiguredUser();
        
        //0. Collecting initial info
        self.responseJR = ServerResponse();
        self.responseJR.method = request.method;
        self.responseJR.operation = request.operation
        
        
        //1. Setting the URL For call
        var strBaseURL:String = AppConfigHandler.valueForKey(key: enumAppConfig.baseUrl.rawValue) as! String
        var strNameSpace:String = AppConfigHandler.valueForKey(key: enumAppConfig.namespace.rawValue) as! String
        
        let encodedParam = request.method?.encodeURL()
        if urlGlobal != "" {
            strBaseURL = urlGlobal;
            strNameSpace = "";
        }
        urlGlobal = "";
        let strMethod = String(format: "%@%@%@",strBaseURL,strNameSpace,encodedParam!)
        var tokenString = ""
        if appConfigUser != nil {
            tokenString = String(format: appConfigUser?.access_token ?? "");
        }

        let strCompleteURL:String = String(format:"%@", strMethod)
        let requestUrl = URL(string: strCompleteURL)
        
        //2. Collect Info for request
        let urlRequest = NSMutableURLRequest(url: requestUrl! as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 1000.0)
        urlRequest.httpMethod = request.requestType
        var headers = [
            "content-type": "application/json",
            "accept": "application/json",
            "cache-control": "no-cache",
           "lang" : locale
        ]
        if tokenString != ""{
            headers["api_token"] = tokenString
        }
        urlRequest.allHTTPHeaderFields = headers
        let data = try? JSONSerialization.data(withJSONObject: request.infoParam ?? "", options: [])
        urlRequest.httpBody = data
        
        //6. Send request to server
        let session = URLSession.shared
        urlRequest.timeoutInterval = 30000
        let dataTask = session.dataTask(with: urlRequest as URLRequest, completionHandler: { (data, response, error) -> Void in
            NSLog("Got Response")
            if (error != nil) {
                print(error!)
                self.responseJR.errorMessage = error?.localizedDescription
                self.responseJR.infoResponse = nil;
                DispatchQueue.main.async {
                    self.delegate?.didFinishServerCommunicationWithError(self.responseJR)
                }
            } else {
                let httpResponse:HTTPURLResponse = response as! HTTPURLResponse
                self.responseJR.responseCode = httpResponse.statusCode;
                self.processResult(data);
            }
        })
        dataTask.resume()
    }
    
    
    open func executeRequerstOnServer(_ request : ServerRequest){
    let locale = LanguageManager.sharedInstance.getSelectedLocale()
        
        NSLog("Request ********* \(request.operation)")

        self.responseJR = ServerResponse();
        self.responseJR.method = request.method;
        self.responseJR.operation = request.operation
        self.responseJR.content = request.content

        let strBaseURL:String = AppConfigHandler.valueForKey(key: enumAppConfig.baseUrl.rawValue) as! String
        let strNameSpace:String = AppConfigHandler.valueForKey(key: enumAppConfig.namespace.rawValue) as! String
        let strMethod = String(format: "%@%@%@",strBaseURL,strNameSpace,(request.method)!)
        var strCompleteURL:String = String(format:"%@", strMethod)
        
        
        if urlGlobal != "" {
            strCompleteURL = urlGlobal;
        }
        urlGlobal = "";
        
        let requestUrl = URL(string: strCompleteURL)
        if requestUrl == nil {
            DispatchQueue.main.async {
                self.delegate?.didFinishServerCommunicationWithError(self.responseJR)
            }
            return;
        }

        
        //5. Add Autherisation Token
        var strToken : String = ""
        if let access_token = UserDefaults.standard.string(forKey: "auth_token"){
            if access_token != ""{
                let tokenValue = String(format:access_token);
                strToken = "Bearer " + tokenValue
                
                print(strToken)
            }
        }
        
        var headers: HTTPHeaders = [
            "Authorization": strToken,
            "lang" : locale,
            "Content-Type": "application/json",
        ]
        if strToken == ""{
            headers = [
                "Content-Type": "application/json",
               "lang" : locale
            ]
        }
        
        //3. Manage for specific request
        let data = try? JSONSerialization.data(withJSONObject: request.infoParam as Any, options: [])
        let req = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as String
        print("Request ********* \(strCompleteURL)")
        print("Param  ********** \(req)")
        
        //4. Adding HTTP Body For GET
        
        
        //6 Request using Alamofire
        //let params = request.infoParam as! [String : AnyObject]
        if request.requestHttpType == ""{
            request.requestHttpType = request.requestType
        }
        
        if (request.requestHttpType == "POST" || request.requestHttpType == "PUT" || request.requestHttpType == "PATCH" || request.requestHttpType == "DELETE")
        {
            do{
                let url = try strCompleteURL.asURL()
                
                Alamofire.request(url, method: HTTPMethod(rawValue:request.requestType)!, parameters: request.infoParam as! [String : AnyObject], encoding: JSONEncoding.default, headers: headers).responseJSON { (responseObject) -> Void in
                    if responseObject.result.isSuccess {
                        if let data = responseObject.data {
                            self.responseJR.responseCode = responseObject.response?.statusCode
                            self.processResult(data);
                        }
                    }
                    if responseObject.result.isFailure {
                        let error : Error = responseObject.result.error!
                        print("Server Communication ERROR \(error.localizedDescription)")
                        self.responseJR.errorMessage = error.localizedDescription;
                        if let res = String(data: data!, encoding: String.Encoding.utf8)
                        {
                            self.responseJR.infoResponse = res as AnyObject?;
                        }
                        self.delegate?.didFinishServerCommunicationWithError(self.responseJR)
                    }
                }
            }
            catch{
                
            }
        }
        if (request.requestHttpType == "GET")
        {
            var strToken : String = ""
            if let access_token = UserDefaults.standard.string(forKey: "auth_token"){
                let tokenValue = String(format: access_token);
                strToken = "Bearer " + tokenValue
            }
            
            let headers: HTTPHeaders = [
                "Authorization": strToken,
               "lang" : locale,
                "Accept": "application/json"
                ]
            
            
            Alamofire.request(strCompleteURL, method: .get, parameters: nil, encoding:JSONEncoding.default, headers:headers)
                .responseJSON { response in
                    //                    print("Response_Almofire **********", (self.responseJR.operation ?? ""), response.result)
                    if response.result.isSuccess {
                        if let data = response.data{
                            self.responseJR.responseCode = response.response?.statusCode
                            self.processResult(data);
                        }
                        else{
                            
                        }
                    }
                    else if response.result.isFailure {
                        if let error : Error = response.result.error{
                            self.responseJR.errorMessage = error.localizedDescription;
                        }
                        if let res = String(data: data!, encoding: String.Encoding.utf8)
                        {
                            self.responseJR.infoResponse = res as AnyObject?;
                        }
                        self.delegate?.didFinishServerCommunicationWithError(self.responseJR)
                    }
            }
        }
    }
    
    
    //MARK:- Creation of multi part request
    class func createRequestToUploadMultipleData(dataDict : Dictionary<String,Any>, apiName: String, completionBlock: @escaping (AnyObject?, Error?, Int) -> Void) ->Void {
        
        var stringURL = "http://148.72.92.9/~boobvpfzc6et//api/v1/"
        stringURL.append(apiName)
        print(stringURL)
        
        let request = NSMutableURLRequest(url:URL(string: stringURL)!)
        request.httpMethod = "POST"
        
        let boundary = "Boundary-\(UUID().uuidString)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        if UserDefaults.standard.value(forKey: "auth_token") != nil {
            request.setValue(UserDefaults.standard.value(forKey: "auth_token") as? String, forHTTPHeaderField: "api_token")
        }
        
        request.httpBody = createMultipleBody(dict: dataDict, boundary: boundary)
        
        
        // Excute HTTP Request
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if response != nil {
                //let _ =  response as!  HTTPURLResponse
                
                // Check for error
                if error != nil {
                    completionBlock(nil, error, 9999)
                    return
                }
                
                // Print out response string
                let dataString = String(data: data!, encoding: .utf8)
                print(dataString as Any)
                
                // Convert server json response to NSDictionary
                do {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? Dictionary<String, AnyObject> {
                        completionBlock(convertedJsonIntoDict as AnyObject, error, 9999)
                    }
                } catch let error as NSError {
                    completionBlock(nil, error, 9999)
                }
            } else {
                //clouser(nil, UInt16(0),NSError.init(domain: "com.mobiloitte", code: 0, userInfo: nil))
                completionBlock(nil, error, 9999)
                
            }
            
            
        }
        task.resume()
    }
    class private func createMultipleBody(dict : Dictionary<String,Any>, boundary : String) -> Data{
        let body = NSMutableData()
        for (key, value) in dict {
            if (value is String || value is NSString){
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            } else if (value is Data){
                body.appendString("--\(boundary)\r\n")
                let mimetype = "image/jpg"
                //                let data = UIImageJPEGRepresentation(value as! UIImage,0.1);
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\("\(NSDate().timeIntervalSince1970)image.jpeg")\"\r\n")
                body.appendString("Content-Type: \(mimetype)\r\n\r\n")
                body.append(value as! Data)
                body.appendString("\r\n")
                
            } else if (value is Array<String>){
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }
        
        
        body.appendString("--\(boundary)--\r\n")
        
        return body as Data
    }

    
    // MARK: - Delegates NSURLConnection
    open func connection(_ connection: NSURLConnection, didFailWithError error: Error) {
        responseJR.errorCodeDescription = "There is some communication error with our server. Please try again.";
        responseJR.errorMessage = "There is some communication error with our server. Please try again.";
        DispatchQueue.main.async {
            self.delegate?.didFinishServerCommunicationWithError(self.responseJR)
        }
    }
    
    // MARK: - Operations
    func serverValidationError(response:ServerResponse) -> String{
        var message  = "Server Error"
        if let result = response.infoResponse as? NSDictionary{
            var validation = ""
            if let errors = result.object(forKey: "errors") as? NSDictionary{
                for eachKey in errors.allKeys{
                    let error = errors.object(forKey: eachKey) as? NSArray
                    validation += String(format:"%@:", (eachKey as! String).capitalized)
                    for eachError in error! {
                        validation += String(format:"\n  - %@",eachError as! CVarArg)
                    }
                    validation += String(format:"\n")
                }
            }
            let mess = validation != "" ? validation : response.errorMessage ?? ""
            
            if mess.lengthOfBytes(using: .utf8) > 0 {
                message = mess
            }
        }
        
        return message
    }
    func processResult(_ data:Data?){
        do {
            if responseJR.responseCode! > 199 && responseJR.responseCode! < 299{
                if let info = try? JSONSerialization.jsonObject(with: data! as Data){
                    responseJR.infoResponse = info as AnyObject?;
                }
                else if let res = String(data: data!, encoding: String.Encoding.utf8)
                {
                    responseJR.infoResponse = res as AnyObject?;
                }
                DispatchQueue.main.async {
                    self.delegate?.didFinishServerCommunicationWithSuccess(self.responseJR)
                }
            }
            else if responseJR.responseCode! > 299 && responseJR.responseCode! < 399{
                if let info = try? JSONSerialization.jsonObject(with: data! as Data){
                    responseJR.infoResponse = info as AnyObject?;
                }
                else if let res = String(data: data!, encoding: String.Encoding.utf8)
                {
                    responseJR.infoResponse = res as AnyObject?;
                }
                self.delegate?.didFinishServerCommunicationWithSuccess(self.responseJR)
            }
            else {
                if let info = try? JSONSerialization.jsonObject(with: data! as Data){
                    
                    let infoResponse:AnyObject! = info as AnyObject?;
                    if let errorMessage = infoResponse.object(forKey:"Message") as? String{
                        responseJR.errorMessage = errorMessage
                    }
                    else if let errorMessage = infoResponse.object(forKey:"message") as? String{
                        responseJR.errorMessage = errorMessage
                    }
                    else if let errorMessage = infoResponse.object(forKey:"error_description") as? String{
                        responseJR.errorMessage = errorMessage
                    }
                    if let errorMessage = infoResponse.object(forKey:"message") as? String{
                        responseJR.errorMessage = errorMessage
                    }
                    
                    responseJR.infoResponse = infoResponse;
                }
                DispatchQueue.main.async {
                    self.responseJR.errorServerValidation = self.serverValidationError(response: self.responseJR)
                    self.delegate?.didFinishServerCommunicationWithError(self.responseJR)
                }
            }
        }
        catch let error as NSError {
            responseJR.errorCodeDescription = "";
            responseJR.infoResponse = error.localizedDescription as AnyObject?;
            responseJR.errorMessage = error.localizedDescription;
            DispatchQueue.main.async {
                self.responseJR.errorServerValidation = self.serverValidationError(response: self.responseJR)
                self.delegate?.didFinishServerCommunicationWithError(self.responseJR)
            }
        }
    }
    func getFileName(mime:String) -> String{
        switch mime {
        case "application/pdf":
            return "file.pdf"
        default:
           return "file.png"
        }
    }
}
