//
//  BookNowVC.swift
//  OneTwo
//
//  Created by appt on 15/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
@available(iOS 13.0, *)
class BookNowVC: BaseViewController,ContactAccess {
    @IBOutlet weak var lblPitchTym: UILabel!
    @IBOutlet weak var popUpView: UIView!
    
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var popUp: UIView!
    
    
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnJoin: UIButton!
    
    
    var objPitch_id: String?
    var objPitch_Size_id: String?
    var objTimeSlotId: String?
    var objPicthcName: String?
    var objTime: String?
    
    var objStrPlayerID: String?
    @IBAction func skipBtn(_ sender: Any) {
        
         callBookingOderAPI()
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
             viewBG.isHidden = true
             popUp.isHidden = true
      }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewBG.isHidden = true
        popUp.isHidden = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        viewBG.addGestureRecognizer(tap)
        viewBG.isUserInteractionEnabled = true
        
        UserDefaults.standard.setValue(objPitch_id, forKey: "objPitch_id")
        UserDefaults.standard.setValue(objPitch_Size_id, forKey: "objPitch_Size_id")
        UserDefaults.standard.setValue(objTimeSlotId, forKey: "objTimeSlotId")
        UserDefaults.standard.setValue(objPicthcName, forKey: "objPicthcName")
        UserDefaults.standard.setValue(objTime, forKey: "objTime")
        UserDefaults.standard.synchronize()

        
        let date = UserDefaults.standard.value(forKey: "SelectedDateHome") as? String
        
        var onjSeledate = Int()
        var dateFromString = ""
        if let createDateStr = date {
            let arrSplit1 = createDateStr.components(separatedBy: "-")
            let strMonth = arrSplit1[1] as! String
            let strDay = arrSplit1[2] as! String
            
            onjSeledate = Int(strDay)!
            
            let monthNumber = (Int)(strMonth)
            
            let fmt = DateFormatter()
            //fmt.dateFormat = "MMM"
            fmt.dateStyle = .medium
            let strMonthName = fmt.monthSymbols[monthNumber! - 1]
            
            dateFromString = String.init(format: "%@ ",strMonthName.prefix(3) as CVarArg)
        }
        
        
        let objAllCap = "\(objPicthcName ?? "") @ \(objTime ?? ""), \(dateFromString)\(onjSeledate.ordinal)"
        self.lblPitchTym.text = objAllCap.uppercased()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onNotification(notification:)), name: Notification.Name.myNotificationKey12, object: nil)
        
        lblPitchTym.layer.borderColor = AppColors.sharedInstance.grey.hexToColor.cgColor
        lblPitchTym.layer.borderWidth = 1.5
        viewTap()
    }
    
    @IBAction func clickedLogin(_ sender: Any) {
        let home = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: LoginVC.self)) as! LoginVC
        home.isFromHome = true
        self.navigationController?.pushViewController(home, animated: true)

    }
    
    
    @IBAction func clickedJoin(_ sender: Any) {
        let home = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: RegisterVC.self)) as! RegisterVC
        self.navigationController?.pushViewController(home, animated: true)

    }
    
    //MARK:- Notification
    @objc func onNotification(notification:Notification)
    {
        objStrPlayerID = notification.userInfo!["text"] as? String
        
        
        print(objStrPlayerID ?? "")
        
        //self.lblText.text = notification.userInfo!["text"] as? String
    }
    
    @IBAction func didTapBack(_ sender: UIButton) {
        self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    @objc func viewTapped(_ sender: UITapGestureRecognizer) {
        
        if let userLoggedIn = UserDefaults.standard.value(forKey: "isUserLoggedIn") as? Bool
        {
            if userLoggedIn == true {
                let vc = self.storyboard?.instantiateViewController(identifier: "ExistingTeamPlayerVC") as! ExistingTeamPlayerVC
                vc.modalTransitionStyle = .coverVertical
                vc.modalPresentationStyle = .overCurrentContext
                self.present(vc, animated: true, completion: nil)
            } else {
                viewBG.isHidden = false
                popUp.isHidden = false
            }
            
        } else {
            viewBG.isHidden = false
            popUp.isHidden = false
        }
        
    }
    func viewTap() {
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(self.viewTapped(_:)))
        self.popUpView.isUserInteractionEnabled = true
        self.popUpView.addGestureRecognizer(viewTap)
    }
    func selectAccessContact() {
        let vc = self.storyboard?.instantiateViewController(identifier: "ContactListVC") as! ContactListVC
        vc.modalTransitionStyle = .coverVertical
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    func callBookingOderAPI() {
        
        let userID = UserDefaults.standard.value(forKey: "user_id") as? Int
        let date = UserDefaults.standard.value(forKey: "SelectedDateHome") as? String
        
        self.showLoader()
        
        let param = ["user_id": userID ?? 0,"pitch_id": objPitch_id ?? "","timeslot_id": objTimeSlotId ?? "","pitch_sizes_id": objPitch_Size_id ?? "","player_list": objStrPlayerID ?? "","booking_date": date ?? ""] as [String : Any]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(BOOKING, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                if statusCode == 200 {
                    
                    if code == 1 {
                        let dicPlayload = response?.value(forKey: "payload") as? NSDictionary
                        
                        let str_BookID = dicPlayload?.value(forKey: "booking_id") as? Int
                        
                        UserDefaults.standard.set(str_BookID, forKey: "booking_id")
                        UserDefaults.standard.synchronize()
                        
                        let vc = self.storyboard?.instantiateViewController(identifier: "BookingDetailsVC") as! BookingDetailsVC
                        vc.objTop = "\(self.objPicthcName ?? "") @ \(self.objTime ?? "")"
                        vc.objDate = date
                        vc.modalTransitionStyle = .coverVertical
                        vc.modalPresentationStyle = .overCurrentContext
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                        self.hideKeyboardWhenTappedAround()
                        
                        self.view.makeToast(message)
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
}
extension Int {

    var ordinal: String {
        var suffix: String
        let ones: Int = self % 10
        let tens: Int = (self/10) % 10
        if tens == 1 {
            suffix = "th"
        } else if ones == 1 {
            suffix = "st"
        } else if ones == 2 {
            suffix = "nd"
        } else if ones == 3 {
            suffix = "rd"
        } else {
            suffix = "th"
        }
        return "\(self)\(suffix)"
    }

}
