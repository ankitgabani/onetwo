//
//  CancelBookingVC.swift
//  OneTwo
//
//  Created by appt on 17/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import SideMenuSwift

@available(iOS 13.0, *)
class CancelBookingVC: BaseViewController {
    @IBOutlet weak var yesView: UIView!
    @IBOutlet weak var viewOne: UIView!
    @IBOutlet weak var noView: UIView!
    
     var isFromNoti = false

    override func viewDidLoad() {
        super.viewDidLoad()

       SetUpView()
        
    }
    func SetUpView() {
           viewOne.layer.cornerRadius = 15
           noView.layer.cornerRadius = 15
           noView.layer.borderColor = UIColor.redColor.cgColor
           noView.layer.borderWidth = 2
           yesView.layer.cornerRadius = 15
           yesView.layer.borderColor = UIColor.oceanColor.cgColor
           yesView.layer.borderWidth = 2
         hideKeyboardWhenTappedAround()
       }

     @IBAction func closeBtn(_ sender: UIButton) {
           dismiss(animated: true, completion: nil)
       }
    
    @IBAction func yesBooking(_ sender: UIButton) {
                
        callBookingCancelAPI()
    }
    
    @IBAction func noBooking(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:- API
       func callBookingCancelAPI() {
           
           let booking_id = UserDefaults.standard.value(forKey: "booking_id") as? Int
           
           self.showLoader()
           
           let param = ["booking_id": booking_id ?? 0]
           
           print(param)
           APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(CANCEL_BOOKING, parameters: param, completionHandler: { (response, error, statusCode) in
               
               if error == nil {
                   print("STATUS CODE \(String(describing: statusCode))")
                   print("Response \(String(describing: response))")
                   
                   let message = response?["message"] as? String ?? ""
                   let code = response?["code"] as? Int ?? 0
                   
                   self.hideLoader()
                   if statusCode == 200 {
                       
                    if code == 1 {
                        
                        NotificationCenter.default.post(name: Notification.Name.myNotificationKeyCancelBook, object: nil, userInfo:["text": message]) // Notification
                        
                        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: SideMenuController.self)) as! SideMenuController
                        SideMenuController.preferences.basic.menuWidth = self.view.frame.width
                        SideMenuController.preferences.basic.statusBarBehavior = .slide
                        SideMenuController.preferences.basic.position = .above
                        SideMenuController.preferences.basic.direction = .left
                        SideMenuController.preferences.basic.supportedOrientations = .portrait
                        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
                        let window = UIApplication.shared.windows.first { $0.isKeyWindow }
                        window?.rootViewController = vc
                        window?.makeKeyAndVisible()
                        
                    } else {
                           self.view.makeToast(message)
                    }
                       
                   } else {
                       
                       self.view.makeToast(message)
                   }
                   
               } else {
                   self.hideLoader()
                   print("Response \(String(describing: response))")
                   let message = response?["message"] as? String ?? ""
                   self.view.makeToast(message)
               }
           })
       }
}
