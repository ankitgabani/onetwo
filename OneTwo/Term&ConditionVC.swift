//
//  Term&ConditionVC.swift
//  OneTwo
//
//  Created by appt on 19/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import WebKit
@available(iOS 13.0, *)
class Term_ConditionVC: BaseViewController {
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var headerLbl: UILabel!
    
    var isFromRegister = false
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        
        callTermsConditionsAPI()
        
        webView.navigationDelegate = self
       
        // Do any additional setup after loading the view.
        setupViews()
    }
    private func setupViews() {
        headerLbl.font = UIFont(name: "JerseyM54", size: 26.0)
    }
    
    @IBAction func menuBtn(_ sender: UIButton) {
        
        if isFromRegister == true {
            self.navigationController?.popViewController(animated: true)

        } else {
            self.navigationController?.sideMenuController?.revealMenu()

        }
    }
    @IBAction func notificationView(_ sender: UIButton) {
       if let user_id = UserDefaults.standard.value(forKey: "user_id") as? Int {
            
            if user_id != 0 {
                
                let vc = self.storyboard?.instantiateViewController(identifier: "NotificationVC") as! NotificationVC
                let transition = CATransition()
                transition.duration = 0.5
                transition.type = CATransitionType.fade
                transition.subtype = CATransitionSubtype.fromLeft
                transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
                view.window!.layer.add(transition, forKey: kCATransition)
                vc.modalPresentationStyle = .overFullScreen
                
                self.sideMenuController?.present(vc, animated: false, completion: nil)
            }
        }
    }
    
    //MARK:- API
    func callTermsConditionsAPI() {
        
        self.showLoader()
        
        let param = ["": ""]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGetingLINK("http://onetwoco.inpro3.fcomet.com/dev/api/terms_conditions", parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                if statusCode == 200 {
                    
                    if code == 1 {
                        
                         let dicResponseData = response?.value(forKey: "payload") as? NSDictionary
                        
                        let str = dicResponseData?.value(forKey: "html_content") as? String
                        
                        self.webView.loadHTMLString("\(str ?? "")", baseURL: nil)
                        self.webView.scrollView.layer.masksToBounds = true
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
}
@available(iOS 13.0, *)
extension Term_ConditionVC: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        let js = "document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust='240%'"//dual size
        webView.evaluateJavaScript(js, completionHandler: nil)
    }
}
