//
//  SettingVC.swift
//  OneTwo
//
//  Created by appt on 10/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import MobileCoreServices
import SideMenuSwift

protocol LLForDelegate
{
    func onLLForDelegateReady(type: String)
}

@available(iOS 13.0, *)
class SettingVC: BaseViewController,LLForDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate {
 
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var dobField: UITextField!
    @IBOutlet weak var emailFld: UITextField!
    @IBOutlet weak var phoneNumFld: UITextField!
    @IBOutlet weak var passwordFld: UITextField!
    @IBOutlet weak var nameFld: UITextField!
    @IBOutlet weak var viewChangeDOB: UIView!
    @IBOutlet weak var viewPasswordTap: UIView!
    @IBOutlet weak var viewNameTap: UIView!
    @IBOutlet weak var viewChangeEmail: UIView!
    @IBOutlet weak var viewChangePhone: UIView!
    
    @IBOutlet weak var viewLogoutPopup: UIView!
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnLogout: UIButton!
    
    @IBOutlet weak var viewBG: UIView!
    
    var objOldFirst: String?
       
    var objOldMiddle: String?
       
    var objOldLast: String?
    
    var selectedImage = UIImage()
       var fileName = String()
       var imagePicker = UIImagePickerController()
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        viewBG.isHidden = true
        viewLogoutPopup.isHidden = true
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBG.isHidden = true
        viewLogoutPopup.isHidden = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        viewBG.addGestureRecognizer(tap)
        viewBG.isUserInteractionEnabled = true
        
        viewLogoutPopup.layer.cornerRadius = 5
        viewLogoutPopup.clipsToBounds = true
        
        btnCancel.layer.cornerRadius = 5
        btnCancel.clipsToBounds = true
        
        btnLogout.layer.cornerRadius = 5
        btnLogout.clipsToBounds = true
        
        setupViewTap()
        passwordViewTap()
        phoneViewTap()
        emailViewTap()
        DateViewTap()
        hideKeyboardWhenTappedAround()
        setupViews()
        
        imagePicker.delegate = self
        imgProfile.layer.cornerRadius = imgProfile.frame.height / 2
        imgProfile.clipsToBounds = true

        callShowProfileAPI()
    }
    private func setupViews() {
        dobField.placeholderColor(color: AppColors.sharedInstance.lightGrey.hexToColor)
        emailFld.placeholderColor(color: AppColors.sharedInstance.lightGrey.hexToColor)
        phoneNumFld.placeholderColor(color: AppColors.sharedInstance.lightGrey.hexToColor)
        passwordFld.placeholderColor(color: AppColors.sharedInstance.lightGrey.hexToColor)
        nameFld.placeholderColor(color: AppColors.sharedInstance.lightGrey.hexToColor)
    }
    
    @IBAction func btnChoosePhot(_ sender: Any) {
        openActionSheet()
    }
    
    @IBAction func didTapBack(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "HomeVC") as! HomeVC
        (self.sideMenuController?.contentViewController as? UINavigationController)?.viewControllers[0] = vc
        self.sideMenuController?.hideMenu()
    }
    @objc func viewPhoneTapped(_ sender: UITapGestureRecognizer) {
    self.view.hideAllToasts()

        self.view.makeToast("You can't change your phone number!", duration: 1.5, title: "", completion: nil)
    }
    @objc func viewEmailTapped(_ sender: UITapGestureRecognizer) {
        self.view.hideAllToasts()

        self.view.makeToast("You can't change your email id!", duration: 1.5, title: "", completion: nil)
    }
    @objc func viewTapped(_ sender: UITapGestureRecognizer) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ChangeNameVC") as! ChangeNameVC
        vc.delegate = self
        vc.objOldFirst = self.objOldFirst
        vc.objOldLast = self.objOldLast
        vc.objOldMiddle = self.objOldMiddle
        vc.modalTransitionStyle = .coverVertical
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    @objc func viewDateTapped(_ sender: UITapGestureRecognizer) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ChangeDobVC") as! ChangeDobVC
        vc.delegate = self
        vc.modalTransitionStyle = .coverVertical
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    @objc func viewPassTapped(_ sender: UITapGestureRecognizer) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ChangePasswordVC") as! ChangePasswordVC
        vc.delegate = self
        vc.modalTransitionStyle = .coverVertical
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    func setupViewTap() {
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(self.viewTapped(_:)))
        self.viewNameTap.isUserInteractionEnabled = true
        self.viewNameTap.addGestureRecognizer(viewTap)
    }
    func passwordViewTap() {
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(self.viewPassTapped(_:)))
        self.viewPasswordTap.isUserInteractionEnabled = true
        self.viewPasswordTap.addGestureRecognizer(viewTap)
    }
    func phoneViewTap() {
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(self.viewPhoneTapped(_:)))
        self.viewChangePhone.isUserInteractionEnabled = true
        self.viewChangePhone.addGestureRecognizer(viewTap)
    }
    func emailViewTap() {
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(self.viewEmailTapped(_:)))
        self.viewChangeEmail.isUserInteractionEnabled = true
        self.viewChangeEmail.addGestureRecognizer(viewTap)
    }
    func DateViewTap() {
           let viewTap = UITapGestureRecognizer(target: self, action: #selector(self.viewDateTapped(_:)))
           self.viewChangeDOB.isUserInteractionEnabled = true
           self.viewChangeDOB.addGestureRecognizer(viewTap)
       }
    func onLLForDelegateReady(type: String) {
         self.view.makeToast("Profile updated successfully")
     }
    
    @IBAction func clickedCancel(_ sender: Any) {
        viewBG.isHidden = true
        viewLogoutPopup.isHidden = true

    }
    
    @IBAction func clickedLogoutPop(_ sender: Any) {
        
        viewBG.isHidden = true
        viewLogoutPopup.isHidden = true
        
        UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
        UserDefaults.standard.set("", forKey: "token_type")
        UserDefaults.standard.set(0, forKey: "user_id")
        UserDefaults.standard.set("", forKey: "username")
        UserDefaults.standard.set("", forKey: "token")
        UserDefaults.standard.set("", forKey: "phone")
        UserDefaults.standard.set("", forKey: "m_name")
        UserDefaults.standard.set("", forKey: "l_name")
        UserDefaults.standard.set("", forKey: "f_name")
        UserDefaults.standard.set("", forKey: "email")
        UserDefaults.standard.synchronize()
        
        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: SideMenuController.self)) as! SideMenuController
        SideMenuController.preferences.basic.menuWidth = self.view.frame.width
        SideMenuController.preferences.basic.statusBarBehavior = .slide
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.supportedOrientations = .portrait
        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
        let window = UIApplication.shared.windows.first { $0.isKeyWindow }
        window?.rootViewController = vc
        window?.makeKeyAndVisible()
        
    }
    
    @IBAction func btnLogout(_ sender: Any) {
        
        viewBG.isHidden = false
        viewLogoutPopup.isHidden = false

    }
    
    // MARK: - API Call
    func callShowProfileAPI() {
        
        self.showLoader()
        
        let player_id = UserDefaults.standard.value(forKey: "user_id") as? Int
        
        let param = ["player_id": player_id ?? 0]
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(SHOW_PROFILE, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let IsSuccess = response?["success"] as? Bool
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                
                if statusCode == 200 {
                    
                    if code == 1 {
                        
                        let objPayload = response?["payload"] as? NSDictionary
                        let email = objPayload?.value(forKey: "email") as? String
                        let f_name = objPayload?.value(forKey: "f_name") as? String
                        let l_name = objPayload?.value(forKey: "l_name") as? String
                        let m_name = objPayload?.value(forKey: "m_name") as? String
                        let phone = objPayload?.value(forKey: "phone") as? String
                        let picture = objPayload?.value(forKey: "picture") as? String
                        let picture_url = objPayload?.value(forKey: "picture_url") as? String
                        let dob = objPayload?.value(forKey: "dob") as? String
                        
                        let url = URL(string: "http://one-two.com.kw/dev/public/user_picture/\(picture ?? "")")
                        self.imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "setting_footballmenmore"))

                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
   func randomString(length: Int) -> String {
       let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
       return String((0..<length).map{ _ in letters.randomElement()! })
     }
     
    func startUploadImageVideo() {
        
        let player_id = UserDefaults.standard.value(forKey: "user_id") as? Int
        
        let param = ["player_id": player_id ?? 0]
        self.showLoader()
        print(param)
        
        let randomString = self.randomString(length: 15)
 
        print("\(self.fileName).jpg")
        APIClient.sharedInstance.postImageToServerWithOutToken(MY_PROFILE, fileName: "Profile\(randomString).jpg", image: selectedImage, parameters: param) { (response, error, statusCode) in
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            self.hideLoader()
            if error == nil {
                
                let message = response?["message"] as? String ?? ""
                let IsSuccess = response?["success"] as? Bool
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                
                if statusCode == 200 {
                    
                    if code == 1 {
                        self.imgProfile.image = self.selectedImage
                        self.view.makeToast(message)
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        }
        
    }
    
    private func openActionSheet(){
          
          let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
          
          alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
              self.openGallary()
          }))
          alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
              self.openCamera()
          }))
          alert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler:{ (UIAlertAction)in
          }))
          self.present(alert, animated: true, completion: nil)
      }
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.mediaTypes = ["public.image"]
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func openCamera()
       {
           if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
           {
               UIView.animate(withDuration: 10.5) {
                   self.view.endEditing(true)
               }
               imagePicker.sourceType = UIImagePickerController.SourceType.camera
               imagePicker.allowsEditing = true
               imagePicker.mediaTypes = [kUTTypeImage as String]
               self.present(imagePicker, animated: true, completion: nil)
           }
           else
           {
               UIView.animate(withDuration: 10.5) {
                   self.view.endEditing(true)
               }
               self.view.makeToast("You don't have camera")
           }
       }

    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            selectedImage = image
            
            imgProfile.layer.cornerRadius = imgProfile.frame.height / 2
            imgProfile.clipsToBounds = true
            
        }
        else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            selectedImage = image
            imgProfile.layer.cornerRadius = imgProfile.frame.height / 2
            imgProfile.clipsToBounds = true
        }
        
//        let url = info[UIImagePickerController.InfoKey.imageURL] as? URL
//        let objfileName = url!.lastPathComponent
//
//        let s12 = removeSpecialCharsFromString(text: objfileName) // "pre"
//
//        self.fileName = s12
        
        startUploadImageVideo()

        picker.dismiss(animated: true, completion: nil)
    }
    
   func removeSpecialCharsFromString(text: String) -> String {
        let okayChars = Set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890")
        return text.filter {okayChars.contains($0) }
    }

}
