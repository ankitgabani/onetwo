//
//  AboutUsVC.swift
//  OneTwo
//
//  Created by appt on 17/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import WebKit
@available(iOS 13.0, *)
class AboutUsVC: BaseViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var headerLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        //let webView2 = WKWebView.init(frame: CGRect.init(x: 10, y: 120, width: self.view.frame.width, height: self.view.frame.height
        //  ))Futura-MediumItalic
        webView.navigationDelegate = self
        callAboutAPI()
        setupViews()
    }
    private func setupViews() {
        headerLbl.font = UIFont(name: "JerseyM54", size: 26.0)
    }
    @IBAction func menuBtn(_ sender: UIButton) {
        self.navigationController?.sideMenuController?.revealMenu()
    }
    
    @IBAction func notificationView(_ sender: UIButton) {
        if let user_id = UserDefaults.standard.value(forKey: "user_id") as? Int {
            
            if user_id != 0 {
                
                let vc = self.storyboard?.instantiateViewController(identifier: "NotificationVC") as! NotificationVC
                let transition = CATransition()
                transition.duration = 0.5
                transition.type = CATransitionType.fade
                transition.subtype = CATransitionSubtype.fromLeft
                transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
                view.window!.layer.add(transition, forKey: kCATransition)
                vc.modalPresentationStyle = .overFullScreen
                
                self.sideMenuController?.present(vc, animated: false, completion: nil)
            }
        }
    }
    
    //MARK:- API
    func callAboutAPI() {
        
        self.showLoader()
        
        let param = ["": ""]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGetingLINK("http://onetwoco.inpro3.fcomet.com/dev/api/about_us", parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                if statusCode == 200 {
                    
                    if code == 1 {
                        
                         let dicResponseData = response?.value(forKey: "payload") as? NSDictionary
                        
                        let str = dicResponseData?.value(forKey: "html_content") as? String
                        
                        self.webView.loadHTMLString("\(str ?? "")", baseURL: nil)
                        self.webView.scrollView.layer.masksToBounds = true
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
}
@available(iOS 13.0, *)
extension AboutUsVC: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        let js = "document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust='245%'"//dual size
        webView.evaluateJavaScript(js, completionHandler: nil)
    }
}
