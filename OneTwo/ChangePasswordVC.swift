//
//  ChangePasswordVC.swift
//  OneTwo
//
//  Created by appt on 10/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
@available(iOS 13.0, *)
class ChangePasswordVC: BaseViewController,UITextFieldDelegate {
    
    @IBOutlet weak var txtOldPass: UITextField!
    @IBOutlet weak var txtNewPass: UITextField!
    
    @IBOutlet weak var savebtn: UIButton!
    @IBOutlet weak var viewOne: UIView!
    
    var delegate: LLForDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtOldPass.delegate = self
        txtNewPass.delegate = self
        
        viewOne.layer.cornerRadius = 20
        savebtn.layer.cornerRadius = 20
        savebtn.layer.borderColor = UIColor.white.cgColor
        savebtn.layer.borderWidth = 3
        hideKeyboardWhenTappedAround()
    }
    
    // MARK: - TextField Delegate Methods
       func textFieldShouldReturn(_ textField: UITextField) -> Bool {
           if textField == txtOldPass {
               txtNewPass.becomeFirstResponder()
               return false
           }
           textField.resignFirstResponder()
           return true
       }
       
       
       func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
           return true
       }
    @IBAction func backBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK:- Validation
    func isValidatedLogin() -> Bool {
        if txtOldPass.text == "" {
            self.view.makeToast("Please enter your password")
            return false
        } else if txtNewPass.text == "" {
            self.view.makeToast("Please enter your new password")
            return false
        }
        return true
    }
    
    @IBAction func saveBtnAct(_ sender: UIButton) {
        
        if self.isValidatedLogin() {
             callChangePassAPI()
         }

    }
    
    func callChangePassAPI() {
        
        self.showLoader()
        
         let userID = UserDefaults.standard.value(forKey: "user_id") as? Int
        
        let param = ["player_id": userID ?? 0, "old_password": txtOldPass.text!,"new_password": txtNewPass.text!] as [String : Any]
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(MY_PROFILE, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let IsSuccess = response?["success"] as? Bool
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                
                if statusCode == 200 {
                    
                    if code == 1 {
                        self.delegate?.onLLForDelegateReady(type: message)
                        self.dismiss(animated: true, completion: nil)
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
    
}
