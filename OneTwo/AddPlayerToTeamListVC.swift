//
//  AddPlayerToTeamListVC.swift
//  OneTwo
//
//  Created by Ankit on 07/08/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

extension Notification.Name {
    
    public static let myNotificationKey = Notification.Name(rawValue: "myNotificationKey")
}

import UIKit
@available(iOS 13.0, *)
class AddPlayerToTeamListVC: BaseViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    
     @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var TBLVIEW: UITableView!
     @IBOutlet weak var lblTotalCount: UILabel!
    @IBOutlet weak var insertName: UITextField!
    @IBOutlet weak var addPlayerView: UIView!
    @IBOutlet weak var insertNameHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var viewOne: UIView!
    var delegate: AddPlyerTeamForDelegate?
    var objArrLangColl = NSMutableArray()
    var objArrNameColl = NSMutableArray()
    var objArrNameCollSearching = NSMutableArray()
    var objTeamId: String?
    
    var phoneStr = String()
    var isSearching : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
                
        if objTeamId == nil {
            
            DispatchQueue.main.async { [weak self] in
                self!.insertNameHeightConstraint.constant = 40
            }
            
        } else {
            DispatchQueue.main.async { [weak self] in
                self!.insertNameHeightConstraint.constant = 0
            }
        }
        
        let set = NSSet(array: objArrLangColl as! [Any])
        print(set)
        let strings1 = set.allObjects as? [String] // or as!
        
        let finalStr = strings1?.joined(separator: ",")
        self.phoneStr = finalStr!
        
        self.lblTotalCount.text = " TOTAL NO. OF SELECTIONS (\(objArrLangColl.count))"
               
        searchView.layer.cornerRadius = 20
        viewOne.layer.cornerRadius = 25
        //setupViewTap()
        hideKeyboardWhenTappedAround()
        insertNameHeightConstraint.constant = 0
    }

    @IBAction func backBtn(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func addPlayer(_ sender: Any) {
        
        if objTeamId == nil {
            
            if self.insertName.text! == "" {
                self.view.makeToast("please enter your team name")
            } else {
                 callCreateTeamWithPlayerAPI()
            }
            
        } else {
            callCreateTeamWithPlayerAPI()
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objArrNameColl.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddPlayerToTeamListCell", for: indexPath) as! AddPlayerToTeamListCell
        
        let objName = objArrNameColl[indexPath.row]
        
        cell.lblName.text = objName as? String

        cell.viewCell.layer.cornerRadius = 18
        cell.viewCell.layer.backgroundColor = UIColor.oceanColor.cgColor
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    //MARK:- API
    func callCreateTeamWithPlayerAPI() {
        
       let userID = UserDefaults.standard.value(forKey: "user_id") as? Int
        
        self.showLoader()
        
        var param = [String: Any]()
        
        if objTeamId == nil {
            param = ["user_id": userID ?? 0,"team_name_en": self.insertName.text!,"player_id": self.phoneStr] as [String : Any]
        } else {
            param = ["user_id": userID ?? 0,"player_id": self.phoneStr,"team_id": objTeamId ?? ""] as [String : Any]
        }
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(CREATE_TEAM_WITH_PLAYERS, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                if statusCode == 200 {
                    
                    if code == 1 {
                       
                        NotificationCenter.default.post(name: Notification.Name.myNotificationKey, object: nil, userInfo:["text": message]) // Notification

                        self.dismiss(animated: true, completion: nil)
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
    
}

class AddPlayerToTeamListCell : UITableViewCell {
    @IBOutlet weak var viewCell: UIView!
    
    @IBOutlet weak var lblName: UILabel!
}
