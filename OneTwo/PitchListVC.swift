//
//  PitchListVC.swift
//  OneTwo
//
//  Created by appt on 10/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import SDWebImage

protocol PitchForDelegate
{
    func onPitchForDelegateReady(time: String,size: String,venue: String)
}

@available(iOS 13.0, *)
class PitchListVC: BaseViewController, UITableViewDelegate,UITableViewDataSource,PitchForDelegate {
   
    
    @IBOutlet weak var lblResult: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewOne: UIView!
    
    var objPitchDate: String?
    
    var arrProgramList: [GetPitchesList] = [GetPitchesList]()
    
    var objVanue: String?
    var objTime: String?
    var objSize: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
         callGetPitchesAPI()
    }
    
    func onPitchForDelegateReady(time: String, size: String, venue: String) {
        callGetPitchesFilterAPI(time: time, size: size, venue: venue)
    }
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func menuBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func filterBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "FilterVC") as! FilterVC
        vc.delegate = self
        vc.modalTransitionStyle = .coverVertical
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrProgramList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PitchListCell", for: indexPath) as! PitchListCell
        cell.viewOne.layer.cornerRadius = 25
        
        let objOrdersList = self.arrProgramList[indexPath.row]
        let name = objOrdersList.name ?? ""
        cell.lblNAme.text = name.uppercased()
        cell.lblAddress.text = objOrdersList.address ?? ""
        cell.lblFare.text = "\(objOrdersList.fare ?? "") KD"
        
        
        DispatchQueue.main.async { [weak self] in
            cell.pitchImage.layer.cornerRadius = cell.pitchImage.frame.height / 2
            cell.pitchImage.clipsToBounds = true
        }
        
        let image = objOrdersList.image ?? ""
        let images_path = objOrdersList.images_path ?? ""
        
        let finalStr = "http://onetwoco.inpro3.fcomet.com/dev/\(images_path)/\(image)"
        
        if (finalStr != "")
        {
            if let url = URL(string: finalStr)
            {
                cell.pitchImage.sd_setImage(with: url, placeholderImage: UIImage(named: "pitchcircle"))
            } else
            {
                cell.pitchImage.image = UIImage(named: "pitchcircle")
            }
            
        }else
        {
            cell.pitchImage.image = UIImage(named: "pitchcircle")
        }
                
        cell.pitchImageAction = { [weak self] in
            let vc = self?.storyboard?.instantiateViewController(identifier: "ImageViewerVC") as? ImageViewerVC
            vc?.objPithID = objOrdersList.pitch_id ?? ""
            vc?.modalPresentationStyle = .overFullScreen
            self?.present(vc!, animated: true, completion: nil)
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let objOrdersList = self.arrProgramList[indexPath.row]
        
        DispatchQueue.main.async {
            let vc = self.storyboard?.instantiateViewController(identifier: "MoreVC") as! MoreVC
            vc.objDetails = objOrdersList.descriptions ?? ""
            vc.objAddress = objOrdersList.address ?? ""
            vc.venue_pitch_id = objOrdersList.pitch_id ?? ""
            vc.objPitch_Size_id = objOrdersList.pitch_size_id ?? ""
            vc.objPitchName = objOrdersList.name ?? ""
            
            UserDefaults.standard.set(objOrdersList.fare ?? "", forKey: "fareKD")
            UserDefaults.standard.synchronize()
            
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    //MARK:- API
    func callGetPitchesAPI() {
        
        self.showLoader()
        
        let param = ["date": objPitchDate ?? ""]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(GET_VENUE_LIST, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                if statusCode == 200 {
                    
                    if code == 1 {
                        
                        self.arrProgramList.removeAll()
                        let dicResponseData = response?.value(forKey: "payload") as? NSDictionary
                        
                        let arrRespose = dicResponseData?.value(forKey: "pitches") as? NSArray
                        
                        for cartList in arrRespose! {
                            let list = GetPitchesList(GetPitchesDictionary: cartList as? NSDictionary)
                            self.arrProgramList.append(list)
                        }
                        
                        if self.arrProgramList.count == 1 {
                            self.lblResult.text = "\(self.arrProgramList.count) result found"

                        } else {
                            self.lblResult.text = "\(self.arrProgramList.count) results found"
                        }
                        
                        self.tblView.reloadData()
                        
                    } else {
                        self.arrProgramList.removeAll()
                        self.tblView.reloadData()
                        self.lblResult.text = "0 result found"
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
   
    func callGetPitchesFilterAPI(time: String, size: String, venue: String) {
          
          self.showLoader()
          
        let param = ["date": objPitchDate ?? "", "pitch_id": venue, "pitch_sizes_id": size,"time": time]
          
          print(param)
          APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(FILTER_PITCH_LIST, parameters: param, completionHandler: { (response, error, statusCode) in
              
              if error == nil {
                  print("STATUS CODE \(String(describing: statusCode))")
                  print("Response \(String(describing: response))")
                  
                  let message = response?["message"] as? String ?? ""
                  let code = response?["code"] as? Int ?? 0
                  
                  self.hideLoader()
                  if statusCode == 200 {
                      
                      if code == 1 {
                          
                          self.arrProgramList.removeAll()
                          let dicResponseData = response?.value(forKey: "payload") as? NSDictionary
                          
                          let arrRespose = dicResponseData?.value(forKey: "pitches") as? NSArray
                          
                          for cartList in arrRespose! {
                              let list = GetPitchesList(GetPitchesDictionary: cartList as? NSDictionary)
                              self.arrProgramList.append(list)
                          }
                          
                          if self.arrProgramList.count == 1 {
                              self.lblResult.text = "\(self.arrProgramList.count) result found"

                          } else {
                              self.lblResult.text = "\(self.arrProgramList.count) results found"
                          }
                        self.tblView.reloadData()
                          
                      } else {
                        self.arrProgramList.removeAll()
                        self.lblResult.text = "0 result found"
                        self.tblView.reloadData()
                        self.view.makeToast(message)
                      }
                      
                  } else {
                      
                      self.view.makeToast(message)
                  }
                  
              } else {
                  self.hideLoader()
                  print("Response \(String(describing: response))")
                  let message = response?["message"] as? String ?? ""
                  self.view.makeToast(message)
              }
          })
      }
}
//MARK:- CELL

class PitchListCell : UITableViewCell {
    
    @IBOutlet weak var pitchImage: UIImageView!
    @IBOutlet weak var viewOne: UIView!
    
    @IBOutlet weak var lblNAme: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblFare: UILabel!
    
    
    var pitchImageAction: (() -> Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        let gesture = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        pitchImage.isUserInteractionEnabled = true
        pitchImage.addGestureRecognizer(gesture)
    }
    @objc func imageTapped() {
        pitchImageAction?()
    }
}
