//
//  NoTeamListVC.swift
//  OneTwo
//
//  Created by appt on 16/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class NoTeamListVC: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var viewOne: UIView!
    
    var isPayer = false
    var isTeam = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isPayer == true {
            lblTitle.text = "You have no existing player list."
        } else {
            lblTitle.text = "You have no existing teams list."
        }
        
        viewOne.layer.cornerRadius = 15
         hideKeyboardWhenTappedAround()
    }
    

    @IBAction func didTapBack(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    

}
