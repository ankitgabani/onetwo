//
//  TeamPlayerVC.swift
//  OneTwo
//
//  Created by appt on 11/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import SideMenuSwift
import Contacts
protocol SlideDelegate: class {
    func menuBtnPressed()
}

protocol AddPlyerTeamForDelegate
{
    func onAddPlyerTeamForDelegateReady(type: String)
}

protocol RemovedForDelegate
{
    func onRemovedForDelegateReady(type: String)
}

@available(iOS 13.0, *)
class TeamPlayerVC: BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,RemovedForDelegate,AddPlyerTeamForDelegate {
    
    @IBOutlet weak var leftPlayerBtn: UIButton!
    @IBOutlet weak var playerCollectionView: UICollectionView!
    @IBOutlet weak var teamCollectionView: UICollectionView!
    var playerIndex = 0
    var teamIndex = 0
    @IBOutlet weak var rightPlayerBtn: UIButton!
    weak var sideMenuDelegate: SlideDelegate?
    
    var arrExistingTeamList: [GetExistingPlayer] = [GetExistingPlayer]()
    var arrExistingPlayerList: [GetExistingPlayer] = [GetExistingPlayer]()
    
    var isNotTeamNow = false
    var isNotPayerNow = false

    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(onNotification(notification:)), name: Notification.Name.myNotificationKey, object: nil)
    }
    
    private func presentSettingsAlert() {
        let settingsURL = URL(string: UIApplication.openSettingsURLString)!
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Permission to Contacts", message: "This app needs access to contacts in order to ...", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Go to Settings", style: .default) { _ in
                UIApplication.shared.openURL(settingsURL)
            })
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
            self.present(alert, animated: true)
        }
    }
    
    //MARK:- Notification
    @objc func onNotification(notification:Notification)
    {
        callGetPlayerListAPI()
        callGetTeamListAPI()
        self.view.makeToast(notification.userInfo!["text"] as? String)
    }
    
    @IBAction func menuBtn(_ sender: UIButton) {
        self.navigationController?.sideMenuController?.revealMenu()
    }
    override func viewWillAppear(_ animated: Bool) {
        callGetPlayerListAPI()
        callGetTeamListAPI()
        self.view.setNeedsLayout()
    }
    @IBAction func addPlayer(_ sender: UIButton) {
 
        if isNotTeamNow == true {
            let vc = self.storyboard?.instantiateViewController(identifier: "NoTeamListVC") as! NoTeamListVC
            vc.isPayer = true
            vc.modalTransitionStyle = .coverVertical
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true, completion: nil)
            
        } else {
            let vc = self.storyboard?.instantiateViewController(identifier: "ExistingPlayerVC") as! ExistingPlayerVC
            vc.isFromPlyaerList = true
            vc.modalTransitionStyle = .coverVertical
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true, completion: nil)
        }
        

        
//        let store = CNContactStore()
//        store.requestAccess(for: .contacts) { granted, error in
//            guard granted else {
//                self.presentSettingsAlert()
//                return
//            }
//
//            // get the contacts
//
//            let request = CNContactFetchRequest(keysToFetch: [CNContactFormatter.descriptorForRequiredKeys(for: .fullName), CNContactPhoneNumbersKey as CNKeyDescriptor])
//            do {
//                try store.enumerateContacts(with: request) { contact, stop in
//                    let name = CNContactFormatter.string(from: contact, style: .fullName)
//                    print(name ?? "")
//                }
//
//                DispatchQueue.main.async {
//                    let vc = self.storyboard?.instantiateViewController(identifier: "ContactListVC") as! ContactListVC
//                    vc.modalTransitionStyle = .coverVertical
//                    vc.modalPresentationStyle = .overCurrentContext
//                    self.present(vc, animated: true, completion: nil)
//                }
//
//
//            } catch {
//                print(error)
//            }
//
//        }
        
    }
    
    @IBAction func notificationBtn(_ sender: Any) {
         if let user_id = UserDefaults.standard.value(forKey: "user_id") as? Int {
                   
                   if user_id != 0 {
                       
                       let vc = self.storyboard?.instantiateViewController(identifier: "NotificationVC") as! NotificationVC
                       let transition = CATransition()
                       transition.duration = 0.5
                       transition.type = CATransitionType.fade
                       transition.subtype = CATransitionSubtype.fromLeft
                       transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
                       view.window!.layer.add(transition, forKey: kCATransition)
                       vc.modalPresentationStyle = .overFullScreen
                       
                       self.sideMenuController?.present(vc, animated: false, completion: nil)
                   }
               }
    }
    @IBAction func rightClick(_ sender: UIButton) {
        if playerIndex <= arrExistingPlayerList.count - 2 {
            playerIndex+=1
            self.playerCollectionView.scrollToItem(at: IndexPath.init(row: playerIndex, section: 0), at: .right, animated: true)
        }
    }
    @IBAction func leftClick(_ sender: UIButton) {
        if playerIndex != 0 {
            playerIndex-=1
            self.playerCollectionView.scrollToItem(at: IndexPath.init(row: playerIndex, section: 0), at: .left, animated: true)
        }
    }
    @IBAction func rightBtn(_ sender: UIButton) {
        if teamIndex <= arrExistingTeamList.count - 2 {
            teamIndex+=1
            self.teamCollectionView.scrollToItem(at: IndexPath.init(row: teamIndex, section: 0), at: .right, animated: true)
        }
    }
    @IBAction func leftBtn(_ sender: UIButton) {
        if teamIndex != 0 {
            teamIndex-=1
            self.teamCollectionView.scrollToItem(at: IndexPath.init(row: teamIndex, section: 0), at: .left, animated: true)
        }
    }
    @IBAction func addTeamBtn(_ sender: Any) {
        
        if isNotTeamNow == true {
            let vc = self.storyboard?.instantiateViewController(identifier: "NoTeamListVC") as! NoTeamListVC
            vc.isTeam = true
            vc.modalTransitionStyle = .coverVertical
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true, completion: nil)
            
        } else {
            let vc = self.storyboard?.instantiateViewController(identifier: "ExistingTeamListVC") as! ExistingTeamListVC
            vc.delegate = self
            vc.modalTransitionStyle = .coverVertical
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true, completion: nil)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == playerCollectionView {
            
            if arrExistingPlayerList.count == 0 {
                return 1
                
            } else {
                return arrExistingPlayerList.count
                
            }
        }
        else {
            if arrExistingTeamList.count == 0 {
                return 1
                
            } else {
                return arrExistingTeamList.count
                
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == playerCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlayerCollectionCell", for: indexPath) as! PlayerCollectionCell
            
            if arrExistingPlayerList.count == 0 {
                cell.lblName.text = ""
                cell.imgPlayer.image = UIImage(named: "football_men")
                
            } else {
                let objOrdersList = self.arrExistingPlayerList[indexPath.row]
                let name = objOrdersList.name ?? ""
                let picture = objOrdersList.picture ?? ""
                let imgUrl = objOrdersList.picture_url ?? ""
                
                cell.lblName.text = name.uppercased()
                
                cell.imgPlayer.layer.cornerRadius = cell.imgPlayer.frame.height / 2
                cell.imgPlayer.clipsToBounds = true
                
                
                var finalStr = "http://one-two.com.kw/dev/public/user_picture/\(picture)"
                finalStr = finalStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                if (imgUrl != "")
                {
                    if let url = URL(string: finalStr)
                    {
                        cell.imgPlayer.sd_setImage(with: url, placeholderImage: UIImage(named: "football_men"))
                    } else
                    {
                        cell.imgPlayer.image = UIImage(named: "football_men")
                    }
                    
                }else
                {
                    cell.imgPlayer.image = UIImage(named: "football_men")
                }
                
            }
            
            
            
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TeamCollectionCell", for: indexPath) as! TeamCollectionCell
            
            
            if arrExistingTeamList.count == 0 {
                cell.lblName.text = ""
                cell.imgTema.image = UIImage(named: "couple_men")
                cell.imgTema.contentMode = .scaleAspectFill
            } else {
                let objOrdersList = self.arrExistingTeamList[indexPath.row]
                let name = objOrdersList.name ?? ""
                
                cell.lblName.text = name.uppercased()
                cell.imgTema.contentMode = .scaleAspectFill

 
                //let picture = objOrdersList.picture ?? ""
               // let imgUrl = objOrdersList.picture_url ?? ""
                
//                cell.imgTema.layer.cornerRadius = cell.imgTema.frame.height / 2
//                cell.imgTema.clipsToBounds = true
                
//                var finalStr = "http://one-two.com.kw/dev/public/user_picture/\(picture)"
//                finalStr = finalStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
//
//                if (finalStr != "")
//                {
//                    if let url = URL(string: finalStr)
//                    {
//                        cell.imgTema.sd_setImage(with: url, placeholderImage: UIImage(named: "couple_men"))
//                    } else
//                    {
//                        cell.imgTema.image = UIImage(named: "couple_men")
//                    }
//
//                }else
//                {
//                    cell.imgTema.image = UIImage(named: "couple_men")
//                }
                
            }
 
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == playerCollectionView {
            
            if arrExistingPlayerList.count == 0 {
                
            } else {
                let objOrdersList = self.arrExistingPlayerList[indexPath.row]
                let name = objOrdersList.name ?? ""
                let id = objOrdersList.player_id ?? ""
                let picture = objOrdersList.picture ?? ""
                let vc = self.storyboard?.instantiateViewController(identifier: "PlayerDetailsVC") as! PlayerDetailsVC
                vc.objUserName = name
                vc.objUserID = id
                vc.dalagate = self
                vc.objProfile = picture
                vc.modalTransitionStyle = .coverVertical
                vc.modalPresentationStyle = .overCurrentContext
                self.present(vc, animated: true, completion: nil)
                
            }
            
            
        }
        else {
            
            if arrExistingTeamList.count == 0 {
                
                
            } else {
                let vc = self.storyboard?.instantiateViewController(identifier: "ChatVC") as! ChatVC
                vc.modalTransitionStyle = .coverVertical
                vc.modalPresentationStyle = .overCurrentContext
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
                
            }
            
            
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func onRemovedForDelegateReady(type: String) {
        callGetPlayerListAPI()
        callGetTeamListAPI()
        self.view.makeToast(type)
    }
    
    func onAddPlyerTeamForDelegateReady(type: String) {
        
        self.view.makeToast(type)
    }
    
    
    //MARK:- API
    func callGetPlayerListAPI() {
        
        let userID = UserDefaults.standard.value(forKey: "user_id") as? Int
        
        self.showLoader()
        
        let param = ["user_id": userID ?? 0]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(GET_EXITING_PLAYER_LIST, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                if statusCode == 200 {
                    
                    if code == 1 {
                        
                        self.arrExistingPlayerList.removeAll()
                        let arrResponseData = response?.value(forKey: "payload") as? NSArray
                        
                        for cartList in arrResponseData! {
                            let list = GetExistingPlayer(GetExistingPlayerDic: cartList as? NSDictionary)
                            self.arrExistingPlayerList.append(list)
                        }
                        
                        print(self.arrExistingPlayerList)
                        self.playerCollectionView.reloadData()
                        
                    } else {
                        //self.view.makeToast(message)
                        self.isNotPayerNow = true
                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
    
    func callGetTeamListAPI() {
        
        let userID = UserDefaults.standard.value(forKey: "user_id") as? Int
        
        self.showLoader()
        
        let param = ["user_id": userID ?? 0]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(GET_EXITING_TEAM_LIST, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                if statusCode == 200 {
                    
                    if code == 1 {
                        
                        self.arrExistingTeamList.removeAll()
                        let arrResponseData = response?.value(forKey: "payload") as? NSArray
                        
                        for cartList in arrResponseData! {
                            let list = GetExistingPlayer(GetExistingPlayerDic: cartList as? NSDictionary)
                            self.arrExistingTeamList.append(list)
                        }
                        
                        self.teamCollectionView.reloadData()
                        
                    } else {
                        //self.view.makeToast(message)
                        self.isNotTeamNow = true
                        
                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
    
}

class PlayerCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgPlayer: UIImageView!
    
    
}

class TeamCollectionCell : UICollectionViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgTema: UIImageView!
    
    
}
