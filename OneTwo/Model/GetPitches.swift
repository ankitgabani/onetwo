//
//  GetPitches.swift
//  OneTwo
//
//  Created by Ankit on 31/07/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import Foundation

class GetPitchesList: NSObject {
    
    var pitch_id: String?
    var pitch_size_id: String?
    var name: String?
    var pitch_type: String?
    var fare: String?
    var descriptions: String?
    var address: String?
    var image: String?
    var images_path: String?
    
    var id: String?
    var name_eng: String?
    var name_arb: String?
    var width: String?
    var length: String?
    var is_active: String?
    var created_at: String?
    var updated_at: String?
    
    var venue_pitch_id: String?
    var day: String?
    var slot: String?
    var is_booked: String?
    var time_token: String?
    
    var images: String?
    
    override init() {
        super.init()
    }
    
    init(GetPitchesDictionary: NSDictionary?) {
        super.init()
        
        images = GetPitchesDictionary?.value(forKey: "images") as? String

        
        pitch_id = GetPitchesDictionary?.value(forKey: "pitch_id") as? String
        pitch_size_id = GetPitchesDictionary?.value(forKey: "pitch_size_id") as? String
        name = GetPitchesDictionary?.value(forKey: "name") as? String
        pitch_type = GetPitchesDictionary?.value(forKey: "pitch_type") as? String
        fare = GetPitchesDictionary?.value(forKey: "fare") as? String
        descriptions = GetPitchesDictionary?.value(forKey: "description") as? String
        address = GetPitchesDictionary?.value(forKey: "address") as? String
        image = GetPitchesDictionary?.value(forKey: "image") as? String
        images_path = GetPitchesDictionary?.value(forKey: "images_path") as? String
        
         id = GetPitchesDictionary?.value(forKey: "id") as? String
         name_eng = GetPitchesDictionary?.value(forKey: "name_eng") as? String
         name_arb = GetPitchesDictionary?.value(forKey: "name_arb") as? String
         width = GetPitchesDictionary?.value(forKey: "width") as? String
         length = GetPitchesDictionary?.value(forKey: "length") as? String
         is_active = GetPitchesDictionary?.value(forKey: "is_active") as? String
         created_at = GetPitchesDictionary?.value(forKey: "created_at") as? String
         updated_at = GetPitchesDictionary?.value(forKey: "updated_at") as? String
        
        venue_pitch_id = GetPitchesDictionary?.value(forKey: "venue_pitch_id") as? String
        day = GetPitchesDictionary?.value(forKey: "day") as? String
        slot = GetPitchesDictionary?.value(forKey: "slot") as? String
        is_booked = GetPitchesDictionary?.value(forKey: "is_booked") as? String
        time_token = GetPitchesDictionary?.value(forKey: "time_token") as? String
    }
}
