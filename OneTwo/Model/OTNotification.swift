//
//  OTNotification.swift
//  OneTwo
//
//  Created by Ankit on 07/08/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import Foundation

import Foundation

class OTNotification: NSObject {
    
    var from_user_id: String?
    var id: String?
    var is_exist: String?
    var name: String?
    var phone: String?
    var status: String?
    var to_user_id: String?
    
    var request_id: String?
    var created_user_id: String?
    var team_created_username: String?
    var team_name: String?
    var team_id: String?
    
    var booking_id: String?
    var booking_user_id: String?
    var user_booking_name: String?
    var pitch_name: String?
    var payment_status: String?
    
    override init() {
        super.init()
    }
    
    init(OTNotificationDic: NSDictionary?) {
        super.init()
        
        from_user_id = OTNotificationDic?.value(forKey: "from_user_id") as? String
        id = OTNotificationDic?.value(forKey: "id") as? String
        is_exist = OTNotificationDic?.value(forKey: "is_exist") as? String
        name = OTNotificationDic?.value(forKey: "name") as? String
        phone = OTNotificationDic?.value(forKey: "phone") as? String
        status = OTNotificationDic?.value(forKey: "status") as? String
        to_user_id = OTNotificationDic?.value(forKey: "to_user_id") as? String
        
        request_id = OTNotificationDic?.value(forKey: "request_id") as? String
        created_user_id = OTNotificationDic?.value(forKey: "created_user_id") as? String
        team_created_username = OTNotificationDic?.value(forKey: "team_created_username") as? String
        team_name = OTNotificationDic?.value(forKey: "team_name") as? String
        team_id = OTNotificationDic?.value(forKey: "team_id") as? String
        
        booking_id = OTNotificationDic?.value(forKey: "booking_id") as? String
        booking_user_id = OTNotificationDic?.value(forKey: "booking_user_id") as? String
        user_booking_name = OTNotificationDic?.value(forKey: "user_booking_name") as? String
        pitch_name = OTNotificationDic?.value(forKey: "pitch_name") as? String
        payment_status = OTNotificationDic?.value(forKey: "payment_status") as? String
    }
}
