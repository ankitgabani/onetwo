
//
//  GetExistingPlayer.swift
//  OneTwo
//
//  Created by Ankit on 04/08/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import Foundation

class GetExistingPlayer: NSObject {
    
    var id: String?
    var user_id: String?
    var team_id: String?
    var player_id: String?
    var role: String?
    var status: String?
    var is_request: String?
    var name: String?
    var picture: String?
    var picture_url: String?

    override init() {
        super.init()
    }
    
    init(GetExistingPlayerDic: NSDictionary?) {
        super.init()
        
        id = GetExistingPlayerDic?.value(forKey: "id") as? String
        user_id = GetExistingPlayerDic?.value(forKey: "user_id") as? String
        team_id = GetExistingPlayerDic?.value(forKey: "team_id") as? String
        player_id = GetExistingPlayerDic?.value(forKey: "player_id") as? String
        role = GetExistingPlayerDic?.value(forKey: "role") as? String
        status = GetExistingPlayerDic?.value(forKey: "status") as? String
        is_request = GetExistingPlayerDic?.value(forKey: "is_request") as? String
        name = GetExistingPlayerDic?.value(forKey: "name") as? String
        picture = GetExistingPlayerDic?.value(forKey: "picture") as? String
        picture_url = GetExistingPlayerDic?.value(forKey: "picture_url") as? String

    }
}
