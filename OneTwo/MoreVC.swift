//
//  MoreVC.swift
//  OneTwo
//
//  Created by appt on 12/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class MoreVC: BaseViewController, SelectTime {
    
    @IBOutlet weak var lblDetails: UILabel!
    
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var bookbtn: UIButton!
    
    var objDetails: String?
    var objAddress: String?
    
    var venue_pitch_id: String?
    var objPitch_Size_id: String?
    var isBooking = false
    var objPitchName: String?
    
    var timeSlot_id: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let name = objDetails {
            self.lblDetails.text = name
        }
        
        if let lblAddress = objAddress {
            self.lblAddress.text = lblAddress
        }
        
        bookbtn.layer.cornerRadius = 20
        bookbtn.layer.borderColor = UIColor.pinkColor.cgColor
        bookbtn.layer.borderWidth = 3
        bookbtn.setTitleColor(UIColor.pinkColor, for: .normal)
        TimeViewTap()
        hideKeyboardWhenTappedAround()
        
    }
    @IBAction func backBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @objc func viewDateTapped(_ sender: UITapGestureRecognizer) {
        
        let vc = self.storyboard?.instantiateViewController(identifier: "SelectTimeVC") as! SelectTimeVC
        vc.delegate = self
        vc.objVanuePitchID = self.venue_pitch_id ?? ""
        self.present(vc, animated: true, completion: nil)

        
        //callGetPitchTimeslotsAPI()
    }
    func TimeViewTap() {
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(self.viewDateTapped(_:)))
        self.timeView.isUserInteractionEnabled = true
        self.timeView.addGestureRecognizer(viewTap)
    }
    
    @IBAction func bookBtnAct(_ sender: Any) {
        
        if timeLbl.text != "TIME" {
            let vc = self.storyboard?.instantiateViewController(identifier: "BookNowVC") as! BookNowVC
            vc.objPitch_Size_id = self.objPitch_Size_id
            vc.objPitch_id = self.venue_pitch_id
            vc.objTimeSlotId = self.timeSlot_id
            vc.objPicthcName = self.objPitchName
            vc.objTime = self.timeLbl.text
            vc.modalTransitionStyle = .coverVertical
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false, completion: nil)
        }
    }
    func selectTime(time: String, id: String) {
        timeLbl.text = time
        self.timeSlot_id = id
    }
    
    //MARK:- API
    func callGetPitchTimeslotsAPI() {
        
       // self.showLoader()
        let date = UserDefaults.standard.value(forKey: "SelectedDateHome") as? String
        let param = ["date": date ?? "","venue_pitch_id": venue_pitch_id ?? ""]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(GET_TIME_SLOT, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                if statusCode == 200 {
                    
                    if code == 1 {
                        
                        let dicResponseData = response?.value(forKey: "payload") as? NSDictionary
                        
                        let arrMorning = dicResponseData?.value(forKey: "Morning") as? NSArray
                        let arrEvening = dicResponseData?.value(forKey: "Evening") as? NSArray
                        let arrAfternoon = dicResponseData?.value(forKey: "Afternoon") as? NSArray
                        
                        for cartList in arrMorning! {
                            let list = GetPitchesList(GetPitchesDictionary: cartList as? NSDictionary)
                            
                            if list.is_booked == "1" {
                                self.isBooking = true
                            }
                        }
                        
                        for cartList in arrAfternoon! {
                            let list = GetPitchesList(GetPitchesDictionary: cartList as? NSDictionary)
                            
                            if list.is_booked == "1" {
                               self.isBooking = true
                            }
                        }
                        
                        for cartList in arrEvening! {
                            let list = GetPitchesList(GetPitchesDictionary: cartList as? NSDictionary)
                            
                            if list.is_booked == "1" {
                                self.isBooking = true
                            }
                        }
                        
                        if self.isBooking == true {
                            
                            let vc = self.storyboard?.instantiateViewController(identifier: "SelectTimeVC") as! SelectTimeVC
                            vc.delegate = self
                            vc.objVanuePitchID = self.venue_pitch_id ?? ""
                            self.present(vc, animated: true, completion: nil)
                            
                        } else {
                             self.view.makeToast("No slot available!")
                        }
                       
                    } else {
                        self.view.makeToast("No slot available!")
                    }
                    
                } else {
                    
                    self.view.makeToast("No slot available!")
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
}
