//
//  LanguageManager.swift
//  ClinicMall
//
//  Created by appt on 20/03/20.
//  Copyright © 2020 SILVER. All rights reserved.
//

import UIKit

class LanguageManager: NSObject {
      var availableLocales = [Locale1]()
        static let sharedInstance = LanguageManager()
        
        //initialize the languages details the app can support.
        override init() {
            let english = Locale1()
            let arabic = Locale1()
            self.availableLocales = [english, arabic]
        }
        
        // setting the bundle and bundle path.
        func getCurrentBundle() -> Bundle{
            let bundlePath = Bundle.main.path(forResource: getSelectedLocale(), ofType: "lproj")
            let bundle = Bundle(path: bundlePath!)
            return bundle!
            
        }
        
        //get selected language components.
        func getSelectedLocale() -> String {
            let userdef = UserDefaults.standard
            let langArray = userdef.object(forKey: "AppleLanguages") as! NSArray
            let current = langArray.firstObject as! String
            return current
        }
        
        //setting the NSUserDefault Property of the app.
        func setLocale(langCode:String){
            UserDefaults.standard.set([langCode, getSelectedLocale()], forKey: "AppleLanguages")
            UserDefaults.standard.synchronize()
            
        }
    }

    //Custom class for initializing Language Code and other details.
    class Locale1: NSObject {
        var name:String?
        var languageCode:String?
        
        func initWithLanguageCode(languageCode: String, name: String)->AnyObject{
            self.name = name
            self.languageCode = languageCode
            return self
        }
    }


    class L012Localizer: NSObject {
        func DoTheSwizzling() {
            // 1
            MethodSwizzleGivenClassName(cls: Bundle.self, originalSelector: #selector(Bundle.localizedString(forKey:value:table:)), overrideSelector: #selector(Bundle.specialLocalizedStringForKey(key:value:table:)))
        }
        
        func MethodSwizzleGivenClassName(cls: AnyClass, originalSelector: Selector, overrideSelector: Selector) {
            let origMethod: Method = class_getInstanceMethod(cls, originalSelector)!;
            let overrideMethod: Method = class_getInstanceMethod(cls, overrideSelector)!;
            if (class_addMethod(cls, originalSelector, method_getImplementation(overrideMethod), method_getTypeEncoding(overrideMethod))) {
                class_replaceMethod(cls, overrideSelector, method_getImplementation(origMethod), method_getTypeEncoding(origMethod));
            } else {
                method_exchangeImplementations(origMethod, overrideMethod);
            }
        }
}
