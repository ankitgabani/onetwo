//
//  ImageViewerVC.swift
//  OneTwo
//
//  Created by ANURAG SHARMA on 04/07/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import WebKit
@available(iOS 13.0, *)
class ImageViewerVC: BaseViewController {

    @IBOutlet weak var imageCollection: UICollectionView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var btnNext: UIButton!
    
    var objPithID: String?
    
    var arrProgramList: [GetPitchesList] = [GetPitchesList]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callGetPitchesImagesAPI()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedNext(_ sender: Any) {
        if self.arrProgramList != nil {
            if self.arrProgramList.count != 0 {
                self.scrollToPreviousOrNextCell(direction: "Next")
            }
        } else {
            
        }
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        if self.arrProgramList != nil {
            if self.arrProgramList.count != 0 {
                self.scrollToPreviousOrNextCell(direction: "Previous")
            }
        } else {
            
        }
    }
    
    
    @IBAction func backBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func scrollToPreviousOrNextCell(direction: String) {
        
        DispatchQueue.global(qos: .background).async {
            
            DispatchQueue.main.async {
                
                let firstIndex = 0
                let lastIndex = (self.arrProgramList.count) - 1
                
                let visibleIndices = self.imageCollection.indexPathsForVisibleItems
                
                let nextIndex = visibleIndices[0].row + 1
                let previousIndex = visibleIndices[0].row - 1
                
                let nextIndexPath: IndexPath = IndexPath.init(item: nextIndex, section: 0)
                let previousIndexPath: IndexPath = IndexPath.init(item: previousIndex, section: 0)
                
                if direction == "Previous" {
                    if previousIndex < firstIndex {
                        
                    } else {
                        self.imageCollection.scrollToItem(at: previousIndexPath, at: .centeredHorizontally, animated: true)
                    }
                } else if direction == "Next" {
                    
                    if nextIndex > lastIndex {

                    } else {
                        
                        self.imageCollection.scrollToItem(at: nextIndexPath, at: .centeredHorizontally, animated: true)
                    }
                }
            }
        }
    }
    
    //MARK:- API
       func callGetPitchesImagesAPI() {
           
        self.showLoader()
           
        let param = ["pitch_id": objPithID ?? ""]
           
           print(param)
           APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(GET_VENUE_IMAGES, parameters: param, completionHandler: { (response, error, statusCode) in
               
               if error == nil {
                   print("STATUS CODE \(String(describing: statusCode))")
                   print("Response \(String(describing: response))")
                   
                   let message = response?["message"] as? String ?? ""
                   let code = response?["code"] as? Int ?? 0
                   
                   self.hideLoader()
                   if statusCode == 200 {
                       
                       if code == 1 {
                           
                           self.arrProgramList.removeAll()
                           let arrRespose = response?.value(forKey: "payload") as? NSArray
                                                      
                           for cartList in arrRespose! {
                               let list = GetPitchesList(GetPitchesDictionary: cartList as? NSDictionary)
                               self.arrProgramList.append(list)
                           }
                           
                           //self.lblResult.text = "\(self.arrProgramList.count) result found"
                        self.imageCollection.reloadData()
                           
                       } else {
                           self.view.makeToast(message)
                       }
                       
                   } else {
                       
                       self.view.makeToast(message)
                   }
                   
               } else {
                   self.hideLoader()
                   print("Response \(String(describing: response))")
                   let message = response?["message"] as? String ?? ""
                   self.view.makeToast(message)
               }
           })
       }

}
@available(iOS 13.0, *)
extension ImageViewerVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrProgramList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath) as? ImageCollectionCell else {return UICollectionViewCell()}
        
        let objOrdersList = self.arrProgramList[indexPath.row]
        let image = objOrdersList.images ?? ""
        
        let finalStr = "http://onetwoco.inpro3.fcomet.com/dev/public/pitch_images/\(image)"
        
        if (finalStr != "")
        {
            if let url = URL(string: finalStr)
            {
                cell.venueImage.sd_setImage(with: url, placeholderImage: UIImage(named: "pitchcircle"))
            } else
            {
                cell.venueImage.image = UIImage(named: "")
            }
            
        }else
        {
            cell.venueImage.image = UIImage(named: "")
        }

        return cell
    }
}
class ImageCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var venueImage: UIImageView!
    @IBOutlet weak var imageWebView: WKWebView!
    override  func awakeFromNib() {
        super.awakeFromNib()
//        scrollView.delegate = self
//        scrollView.minimumZoomScale = 1
//        scrollView.maximumZoomScale = 3
//        scrollView.bounces = false

    }
//    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
//        return venueImage
//    }
}
