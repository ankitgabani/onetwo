//
//  ChangeNameVC.swift
//  OneTwo
//
//  Created by appt on 10/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
@available(iOS 13.0, *)
class ChangeNameVC: BaseViewController, UITextFieldDelegate {
    
    @IBOutlet weak var txtOldFirst: UITextField!
    @IBOutlet weak var txtNewFirst: UITextField!
    
    @IBOutlet weak var txtOldMiddle: UITextField!
    @IBOutlet weak var txtNewMiddle: UITextField!
    
    @IBOutlet weak var txtOldLast: UITextField!
    @IBOutlet weak var txtNewLast: UITextField!
    
    
    @IBOutlet weak var savebtn: UIButton!
    
    @IBOutlet weak var viewOne: UIView!
    
    var objOldFirst: String?
    var objNewFirst: String?
    
    var objOldMiddle: String?
    var objNewMidle: String?
    
    var objOldLast: String?
    var objNewLast: String?
    
    var delegate: LLForDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtNewFirst.delegate = self
        txtNewMiddle.delegate = self
        txtNewLast.delegate = self
        
        if let name = objOldFirst {
            txtOldFirst.text = name
        }
        
        if let name = objOldMiddle {
            txtOldMiddle.text = name
        }
        
        if let name = objOldLast {
            txtOldLast.text = name
        }
        
        viewOne.layer.cornerRadius = 20
        savebtn.layer.cornerRadius = 20
        savebtn.layer.borderColor = UIColor.white.cgColor
        savebtn.layer.borderWidth = 3
        hideKeyboardWhenTappedAround()
    }
    
    // MARK: - TextField Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtNewFirst {
            txtNewMiddle.becomeFirstResponder()
            return false
        } else if textField == txtNewMiddle {
            txtNewLast.becomeFirstResponder()
            return false
        }
        
        textField.resignFirstResponder()
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        callShowProfileAPI()
    }
    
    @IBAction func backBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveBtnAct(_ sender: UIButton) {
        
        if txtNewFirst.text! != "" || txtNewMiddle.text! != "" || txtNewLast.text! != "" {
            callChangeNameAPI()
        } else {
             self.view.makeToast("Enter New First Name")
        }
    }
    
    // MARK: - API Call
    func callChangeNameAPI() {
        
        let userID = UserDefaults.standard.value(forKey: "user_id") as? Int
        
        self.showLoader()
        
        let param = ["player_id": userID ?? 0,"f_name": txtNewFirst.text!,"m_name": txtNewMiddle.text!,"l_name": txtNewLast.text!] as [String : Any]
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(MY_PROFILE, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let IsSuccess = response?["success"] as? Bool
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                
                if code == 1 {
                    
                    self.delegate?.onLLForDelegateReady(type: message)
                    self.dismiss(animated: true, completion: nil)
                    
                } else {
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
    
    func callShowProfileAPI() {
        
       // self.showLoader()
        
        let player_id = UserDefaults.standard.value(forKey: "user_id") as? Int
        
        let param = ["player_id": player_id ?? 0]
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(SHOW_PROFILE, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let IsSuccess = response?["success"] as? Bool
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                
                if statusCode == 200 {
                    
                    if code == 1 {
                        
                        let objPayload = response?["payload"] as? NSDictionary
                        let f_name = objPayload?.value(forKey: "f_name") as? String
                        let l_name = objPayload?.value(forKey: "l_name") as? String
                        let m_name = objPayload?.value(forKey: "m_name") as? String
                        
                        self.txtOldLast.text = l_name?.uppercased()
                        self.txtOldFirst.text = f_name?.uppercased()
                        self.txtOldMiddle.text = m_name?.uppercased()
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
}
