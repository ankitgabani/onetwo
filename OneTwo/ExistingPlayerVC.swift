//
//  ExistingPlayerVC.swift
//  OneTwo
//
//  Created by appt on 18/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class ExistingPlayerVC: BaseViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate  {
    
    @IBOutlet weak var searchBar: UITextField!
    
    @IBOutlet weak var tblVieww: UITableView!
    @IBOutlet weak var lblTotalCount: UILabel!
    @IBOutlet weak var totalPlayerView: UIView!
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var viewOne: UIView!
    var isSearching: Bool = false
    var arrExistingPlayerList: [GetExistingPlayer] = [GetExistingPlayer]()
    var arrExistingPlayerListSearching: [GetExistingPlayer] = [GetExistingPlayer]()
    var objTeamId: String?
    var isFromGroup = false
    var delegate: AddPlyerTeamForDelegate?
    var arrIDColl = NSMutableArray()
    var arrNameColl = NSMutableArray()
    
    var isFromPlyaerList = false
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        searchView.layer.cornerRadius = 20
        viewOne.layer.cornerRadius = 25
        callGetPlayerListAPI()
        
        tblVieww.allowsMultipleSelection = true
        
        if isFromPlyaerList == false {
            self.lblTotalCount.text = "TOTAL NO. OF SELECTIONS (0)"
            totalPlayerViewTap()

        } else {
            self.lblTotalCount.isHidden = true
            self.totalPlayerView.isHidden = true
        }
        
        searchBar.addTarget(self, action: #selector(ExistingPlayerVC.textFieldDidChange(_:)),
                            for: UIControl.Event.editingChanged)
    }
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if self.searchBar.text!.isEmpty {
            
            self.isSearching = false
            
            self.tblVieww.reloadData()
            
        } else {
            self.isSearching = true
            
            self.arrExistingPlayerListSearching.removeAll(keepingCapacity: false)
            
            for i in 0..<self.arrExistingPlayerList.count {
                
                let listItem: GetExistingPlayer = self.arrExistingPlayerList[i]
                if listItem.name!.lowercased().range(of: self.searchBar.text!.lowercased()) != nil {
                    self.arrExistingPlayerListSearching.append(listItem)
                }
            }
            
            self.tblVieww.reloadData()
        }
        
    }
    
    @IBAction func backBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        hideKeyboardWhenTappedAround()
    }
    
    @IBAction func choooseselected(_ sender: Any) {
        weak var pvc = self.presentingViewController
        
        self.dismiss(animated: true, completion: {
            let vc = self.storyboard?.instantiateViewController(identifier: "AddPlayerToTeamListVC") as! AddPlayerToTeamListVC
            vc.objTeamId = self.objTeamId
            vc.objArrNameColl = self.arrNameColl
            vc.objArrLangColl = self.arrIDColl
            vc.modalTransitionStyle = .coverVertical
            vc.modalPresentationStyle = .overCurrentContext
            pvc?.present(vc, animated: true, completion: nil)
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isSearching == true {
            return arrExistingPlayerListSearching.count
        } else {
            return arrExistingPlayerList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExistingPlayerListCell", for: indexPath) as! ExistingPlayerListCell
        
        var objOrdersList: GetExistingPlayer!
        if self.isSearching == true {
            objOrdersList = self.arrExistingPlayerListSearching[indexPath.row]
        } else {
            objOrdersList = self.arrExistingPlayerList[indexPath.row]
        }
        
        cell.lblName.text = objOrdersList.name ?? ""
        
        if isFromPlyaerList == true {
            cell.viewCell.layer.cornerRadius = 18
            cell.viewCell.layer.borderWidth = 1
            cell.viewCell.layer.borderColor = UIColor.oceanColor.cgColor
            cell.viewCell.backgroundColor = UIColor.oceanColor

        } else {
            cell.viewCell.layer.cornerRadius = 18
            cell.viewCell.layer.borderWidth = 1
            cell.viewCell.layer.borderColor = UIColor.oceanColor.cgColor
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tblVieww.cellForRow(at: indexPath) as! ExistingPlayerListCell
        
        var objOrdersList: GetExistingPlayer!
        if self.isSearching == true {
            objOrdersList = self.arrExistingPlayerListSearching[indexPath.row]
        } else {
            objOrdersList = self.arrExistingPlayerList[indexPath.row]
        }
        
        if isFromPlyaerList == false {
            
            if cell.viewCell.layer.backgroundColor == UIColor.oceanColor.cgColor {
                cell.viewCell.layer.cornerRadius = 18
                cell.viewCell.layer.backgroundColor = UIColor.clear.cgColor
                cell.viewCell.layer.borderWidth = 1
                cell.viewCell.layer.borderColor = UIColor.oceanColor.cgColor
                self.arrIDColl.remove(objOrdersList.player_id ?? "")
                self.arrNameColl.remove(objOrdersList.name ?? "")
                self.lblTotalCount.text = "TOTAL NO. OF SELECTIONS (\(self.arrNameColl.count))"
                
            } else {
                cell.viewCell.layer.cornerRadius = 18
                cell.viewCell.layer.backgroundColor = UIColor.oceanColor.cgColor
                cell.viewCell.layer.borderWidth = 1
                cell.viewCell.layer.borderColor = UIColor.oceanColor.cgColor
                self.arrIDColl.add(objOrdersList.player_id ?? "")
                self.arrNameColl.add(objOrdersList.name ?? "")
                self.lblTotalCount.text = "TOTAL NO. OF SELECTIONS (\(self.arrNameColl.count ))"
            }
        }
        
        self.tblVieww.reloadData()
 
    }
    
    @objc func viewTotalPlayerTapped(_ sender: UITapGestureRecognizer) {
        let vc = self.storyboard?.instantiateViewController(identifier: "BookingDetailsVC") as! BookingDetailsVC
        vc.modalTransitionStyle = .coverVertical
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    func totalPlayerViewTap() {
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(self.viewTotalPlayerTapped(_:)))
        self.totalPlayerView.isUserInteractionEnabled = true
        self.totalPlayerView.addGestureRecognizer(viewTap)
    }
    
    
    //MARK:- API
    func callGetPlayerListAPI() {
        
        let userID = UserDefaults.standard.value(forKey: "user_id") as? Int
        
        self.showLoader()
        
        let param = ["user_id": userID ?? 0]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(GET_EXITING_PLAYER_LIST, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                if statusCode == 200 {
                    
                    if code == 1 {
                        
                        self.arrExistingPlayerList.removeAll()
                        let arrResponseData = response?.value(forKey: "payload") as? NSArray
                        
                        for cartList in arrResponseData! {
                            let list = GetExistingPlayer(GetExistingPlayerDic: cartList as? NSDictionary)
                            self.arrExistingPlayerList.append(list)
                        }
                        
                        print(self.arrExistingPlayerList)
                        self.tblVieww.reloadData()
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
    
}

class ExistingPlayerListCell : UITableViewCell {
    @IBOutlet weak var viewCell: UIView!
    
    @IBOutlet weak var lblName: UILabel!
    
}



