//
//  TotalPlayerVC.swift
//  OneTwo
//
//  Created by appt on 12/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import MessageUI

@available(iOS 13.0, *)
class TotalPlayerVC: BaseViewController,UITableViewDataSource,UITableViewDelegate, MFMessageComposeViewControllerDelegate {
    
    @IBOutlet weak var TBLVIEW: UITableView!
    @IBOutlet weak var lblTile: UILabel!
    @IBOutlet weak var insertName: UITextField!
    @IBOutlet weak var addPlayerView: UIView!
    @IBOutlet weak var insertNameHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var viewOne: UIView!
    
    var objArrLangColl = NSMutableArray()
    var objArrNameColl = NSMutableArray()

    var phoneStr = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let set = NSSet(array: objArrLangColl as! [Any])
        print(set)
        let strings1 = set.allObjects as? [String] // or as!
        
        let finalStr = strings1?.joined(separator: ",")
        self.phoneStr = finalStr!
        
        self.lblTile.text = "TOTAL NO. OF SELECTIONS (\(objArrLangColl.count))"
        searchView.layer.cornerRadius = 20
        viewOne.layer.cornerRadius = 25
        //setupViewTap()
        hideKeyboardWhenTappedAround()
        insertNameHeightConstraint.constant = 0
    }
//    @objc func viewTapped(_ sender: UITapGestureRecognizer) {
//        insertName.isHidden = false
//    }
//    func setupViewTap() {
//        let viewTap = UITapGestureRecognizer(target: self, action: #selector(self.viewTapped(_:)))
//        self.addPlayerView.isUserInteractionEnabled = true
//        self.addPlayerView.addGestureRecognizer(viewTap)
//    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
           //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
       }

    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
       }
    
    @IBAction func backBtn(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func addPlayer(_ sender: Any) {
        
        let firstName = UserDefaults.standard.value(forKey: "f_name") as? String
        let middleName = UserDefaults.standard.value(forKey: "m_name") as? String
        let lastName = UserDefaults.standard.value(forKey: "l_name") as? String

        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = "\(firstName ?? "") \(middleName ?? "") \(lastName ?? "") - Invited you to download this app and join the match. \n\nhttps://we.tl/t-HZq35cbSaM"
            controller.recipients = objArrLangColl as? [String]
            controller.messageComposeDelegate = self
            controller.modalPresentationStyle = .fullScreen
              self.present(controller, animated: true, completion: nil)
        }
        
        callChkUserExistOrNotAPI()
        //callSendInvitationMultipleAPI()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objArrNameColl.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TotalPlayerCell", for: indexPath) as! TotalPlayerCell
        
        let objName = objArrNameColl[indexPath.row]
        
        cell.lblName.text = objName as? String

        cell.viewCell.layer.cornerRadius = 18
        cell.viewCell.layer.backgroundColor = UIColor.oceanColor.cgColor
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    
    //MARK:- API
      func callChkUserExistOrNotAPI() {
            
           let userID = UserDefaults.standard.value(forKey: "user_id") as? Int
            
            self.showLoader()
            
            let param = ["user_id": userID ?? 0,"phone": self.phoneStr] as [String : Any]
            
            print(param)
            APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(CHECK_EXISTING_USER, parameters: param, completionHandler: { (response, error, statusCode) in
                
                if error == nil {
                    print("STATUS CODE \(String(describing: statusCode))")
                    print("Response \(String(describing: response))")
                    
                    let message = response?["message"] as? String ?? ""
                    let code = response?["code"] as? Int ?? 0
                    
                    self.hideLoader()
                    if statusCode == 200 {
                        
                        if code == 1 {
                           
                            self.callSendInvitationMultipleAPI()
                        } else {
                            self.view.makeToast(message)
                        }
                        
                    } else {
                        
                        self.view.makeToast(message)
                    }
                    
                } else {
                    self.hideLoader()
                    print("Response \(String(describing: response))")
                    let message = response?["message"] as? String ?? ""
                    self.view.makeToast(message)
                }
            })
        }
    
    func callSendInvitationMultipleAPI() {
        
       let userID = UserDefaults.standard.value(forKey: "user_id") as? Int
        
        self.showLoader()
        
        let param = ["user_id": userID ?? 0,"phone": self.phoneStr] as [String : Any]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(SEND_INVITATION_MULTIPLE, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                if statusCode == 200 {
                    
                    if code == 1 {
                                               
//                        if self.insertNameHeightConstraint.constant == 0 {
//                            UIView.animate(withDuration: 0.8) {
//                                self.insertNameHeightConstraint.constant = 40
//                                self.insertName.superview?.layoutIfNeeded()
//                            }
//                        }
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
    
}

class TotalPlayerCell : UITableViewCell {
    @IBOutlet weak var viewCell: UIView!
    
    @IBOutlet weak var lblName: UILabel!
}
