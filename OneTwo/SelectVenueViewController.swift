//
//  SelectVenueViewController.swift
//  OneTwo
//
//  Created by appt on 12/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
protocol SelectVenue {
    func selectVenue(Venue : String,pitch_id: String,pitch_size_id: String)
}
@available(iOS 13.0, *)
class SelectVenueViewController: BaseViewController , UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var selctVenueLbl: UILabel!
    @IBOutlet weak var view1: UIView!
    
    @IBOutlet weak var tblView: UITableView!
    
    var arrPintchList: [GetPitchesList] = [GetPitchesList]()
    
    var delegate : SelectVenue!
    override func viewDidLoad() {
        super.viewDidLoad()
        view1.layer.cornerRadius = 25
         hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
        setupViews()
        callGetPitcheListAPI()
    }
    private func setupViews() {
           selctVenueLbl.font = UIFont(name: "JerseyM54", size: 20.0)
       }
    @IBAction func didTapBack(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPintchList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectVenueCell", for: indexPath) as! SelectVenueCell
        
        let objOrdersList = self.arrPintchList[indexPath.row]
        let name = objOrdersList.name ?? ""
        cell.venueLbl.text = name.uppercased()
        
        cell.viewCell.layer.cornerRadius = 18
        cell.viewCell.layer.borderColor = UIColor.oceanColor.cgColor
        cell.viewCell.layer.borderWidth = 1
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectVenueCell", for: indexPath) as! SelectVenueCell
            let objOrdersList = self.arrPintchList[indexPath.row]
            let name = objOrdersList.name ?? ""
            let id = objOrdersList.pitch_id ?? ""
            let pitch_size_id = objOrdersList.pitch_size_id ?? ""
            self.delegate.selectVenue(Venue: name, pitch_id: id,pitch_size_id: pitch_size_id)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK:- API
    func callGetPitcheListAPI() {
        
        self.showLoader()
        
        let date = UserDefaults.standard.value(forKey: "SelectedDateHome") as? String
        
        let param = ["date": date ?? ""]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(GET_PITCH_LIST, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                if statusCode == 200 {
                    
                    if code == 1 {
                        
                        self.arrPintchList.removeAll()
                        let arrRespose = response?.value(forKey: "payload") as? NSArray
                                                
                        for cartList in arrRespose! {
                            let list = GetPitchesList(GetPitchesDictionary: cartList as? NSDictionary)
                            self.arrPintchList.append(list)
                        }
                        
                        self.tblView.reloadData()
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
}


class SelectVenueCell: UITableViewCell {
    
    @IBOutlet weak var venueLbl: UILabel!
    
    @IBOutlet weak var viewCell: UIView!
    
    
}
