//
//  ContactListVC.swift
//  OneTwo
//
//  Created by appt on 12/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import Contacts
@available(iOS 13.0, *)
class ContactListVC: BaseViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var lblTotalCount: UILabel!
    @IBOutlet weak var TBLVIEW: UITableView!
    @IBOutlet weak var totalPlayerView: UIView!
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var viewOne: UIView!
    
  //  var arrPhoneNumber = NSMutableArray()
    var arrNameNumber = NSMutableArray()
    var arrNameNumberSearching = NSMutableArray()

    var isSearching: Bool = false
    var arrExistingPlayerList: [GetExistingPlayer] = [GetExistingPlayer]()
    var arrExistingPlayerListSearching: [GetExistingPlayer] = [GetExistingPlayer]()
    
    var arrLangColl = NSMutableArray()
    var arrNameColl = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        self.lblTotalCount.text = "TOTAL NO. OF SELECTIONS (0)"
        searchView.layer.cornerRadius = 20
        viewOne.layer.cornerRadius = 25
        totalPlayerViewTap()
        hideKeyboardWhenTappedAround()
        
        let status = CNContactStore.authorizationStatus(for: .contacts)
        if status == .denied || status == .restricted {
            presentSettingsAlert()
            return
        }
        
        // open it
        
        let store = CNContactStore()
        store.requestAccess(for: .contacts) { granted, error in
            guard granted else {
                self.presentSettingsAlert()
                return
            }
            
            // get the contacts
            
            let request = CNContactFetchRequest(keysToFetch: [CNContactFormatter.descriptorForRequiredKeys(for: .fullName), CNContactPhoneNumbersKey as CNKeyDescriptor])
            do {
                try store.enumerateContacts(with: request) { contact, stop in
                    let name = CNContactFormatter.string(from: contact, style: .fullName)
                    print(name ?? "")
                    let dic = NSMutableDictionary()
                    dic.setValue(name, forKey: "name")
                //    self.arrNameNumber.add(name ?? "")
                    for phone in contact.phoneNumbers {
                        
                        var label = phone.label
                        if label != nil {
                            label = CNLabeledValue<CNPhoneNumber>.localizedString(forLabel: label!)
                        }
                        print(phone.value.stringValue)
                        dic.setValue(phone.value.stringValue, forKey: "phone")
                       // self.arrPhoneNumber.add(phone.value.stringValue)
                    }
                    
                    self.arrNameNumber.add(dic)
                    
                }
                

            } catch {
                print(error)
            }
            
        }
        
        searchBar.addTarget(self, action: #selector(ExistingPlayerVC.textFieldDidChange(_:)),
        for: UIControl.Event.editingChanged)

    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if self.searchBar.text!.isEmpty {
            
            self.isSearching = false
            
            self.TBLVIEW.reloadData()
            
        } else {
            self.isSearching = true
            
            self.arrNameNumberSearching.removeAllObjects()
            
            for i in 0..<self.arrNameNumber.count {
                
                let listItem = self.arrNameNumber[i] as! NSMutableDictionary
                if (listItem.value(forKey: "name") as? String)?.lowercased().range(of: self.searchBar.text!.lowercased()) != nil {
                    self.arrNameNumberSearching.add(listItem)
                }
            }
            
            self.TBLVIEW.reloadData()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        
    }
    
    private func presentSettingsAlert() {
        let settingsURL = URL(string: UIApplication.openSettingsURLString)!
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Permission to Contacts", message: "This app needs access to contacts in order to ...", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Go to Settings", style: .default) { _ in
                UIApplication.shared.openURL(settingsURL)
            })
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func backBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearching == true {
            
            return arrNameNumberSearching.count

        } else {
            
            return arrNameNumber.count

        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath) as! ContactCell
        
        
        if isSearching == true {
            
            let objName = arrNameNumberSearching[indexPath.row] as! NSMutableDictionary
            cell.lblName.text = objName.value(forKey: "name") as? String
            cell.lblCopntact.text = objName.value(forKey: "phone") as? String
        } else {
            
            let objName = arrNameNumber[indexPath.row] as! NSMutableDictionary
            cell.lblName.text = objName.value(forKey: "name") as? String
            cell.lblCopntact.text = objName.value(forKey: "phone") as? String
        }
        
        
        
        cell.viewCell.layer.cornerRadius = 25
        cell.viewCell.layer.borderWidth = 1
        cell.viewCell.layer.borderColor = UIColor.oceanColor.cgColor

        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = TBLVIEW.cellForRow(at: indexPath) as! ContactCell
        
        if isSearching == true {
            
            let objName = arrNameNumberSearching[indexPath.row] as! NSMutableDictionary
            
            cell.lblName.text = objName.value(forKey: "name") as? String
            
            
            cell.lblCopntact.text = objName.value(forKey: "phone") as? String
            
            if cell.viewCell.layer.backgroundColor == UIColor.oceanColor.cgColor {
                cell.viewCell.layer.cornerRadius = 18
                cell.viewCell.layer.backgroundColor = UIColor.clear.cgColor
                cell.viewCell.layer.borderWidth = 1
                cell.viewCell.layer.borderColor = UIColor.oceanColor.cgColor
                self.arrLangColl.remove(objName.value(forKey: "phone") as? String)
                self.arrNameColl.remove(objName.value(forKey: "name") as? String)
                self.lblTotalCount.text = "TOTAL NO. OF SELECTIONS (\(self.arrNameColl.count))"
                
            } else {
                cell.viewCell.layer.cornerRadius = 18
                cell.viewCell.layer.backgroundColor = UIColor.oceanColor.cgColor
                cell.viewCell.layer.borderWidth = 1
                cell.viewCell.layer.borderColor = UIColor.oceanColor.cgColor
                self.arrLangColl.add(objName.value(forKey: "phone") as? String)
                self.arrNameColl.add(objName.value(forKey: "name") as? String)
                self.lblTotalCount.text = "TOTAL NO. OF SELECTIONS (\(self.arrNameColl.count ))"
            }
        } else {
            
            let objName = arrNameNumber[indexPath.row] as! NSMutableDictionary
            
            cell.lblName.text = objName.value(forKey: "name") as? String
            
            
            cell.lblCopntact.text = objName.value(forKey: "phone") as? String
            
            if cell.viewCell.layer.backgroundColor == UIColor.oceanColor.cgColor {
                cell.viewCell.layer.cornerRadius = 18
                cell.viewCell.layer.backgroundColor = UIColor.clear.cgColor
                cell.viewCell.layer.borderWidth = 1
                cell.viewCell.layer.borderColor = UIColor.oceanColor.cgColor
                self.arrLangColl.remove(objName.value(forKey: "phone") as? String)
                self.arrNameColl.remove(objName.value(forKey: "name") as? String)
                self.lblTotalCount.text = "TOTAL NO. OF SELECTIONS (\(self.arrNameColl.count))"
                
            } else {
                cell.viewCell.layer.cornerRadius = 18
                cell.viewCell.layer.backgroundColor = UIColor.oceanColor.cgColor
                cell.viewCell.layer.borderWidth = 1
                cell.viewCell.layer.borderColor = UIColor.oceanColor.cgColor
                self.arrLangColl.add(objName.value(forKey: "phone") as? String)
                self.arrNameColl.add(objName.value(forKey: "name") as? String)
                self.lblTotalCount.text = "TOTAL NO. OF SELECTIONS (\(self.arrNameColl.count ))"
            }
        }
        
        self.TBLVIEW.reloadData()
        
    }
    
    @objc func viewTotalPlayerTapped(_ sender: UITapGestureRecognizer) {
        
        if self.lblTotalCount.text == "TOTAL NO. OF SELECTIONS (0)" {
            self.view.makeToast("The mobile number is required")
        } else {
            weak var pvc = self.presentingViewController
            self.dismiss(animated: true, completion: {
                let vc = self.storyboard?.instantiateViewController(identifier: "TotalPlayerVC") as! TotalPlayerVC
                vc.objArrLangColl = self.arrLangColl
                vc.objArrNameColl = self.arrNameColl
                vc.modalTransitionStyle = .coverVertical
                vc.modalPresentationStyle = .overCurrentContext
                pvc?.present(vc, animated: true, completion: nil)
            })
        }
        
    }
    
    func totalPlayerViewTap() {
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(self.viewTotalPlayerTapped(_:)))
        self.totalPlayerView.isUserInteractionEnabled = true
        self.totalPlayerView.addGestureRecognizer(viewTap)
    }
    
}
class ContactCell : UITableViewCell {
    
    
    @IBOutlet weak var lblCopntact: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var viewCell: UIView!
}
