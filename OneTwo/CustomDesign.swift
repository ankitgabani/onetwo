//
//  CustomDesign.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 24/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class CustomDesign: NSObject {

}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(withHex: Int) {
        self.init(red:(withHex >> 16) & 0xff, green:(withHex >> 8) & 0xff, blue:withHex & 0xff)
    }
    static let textColor = UIColor(withHex: 0x434444)
    static let grey = UIColor(withHex: 0xA8A8A4)
    static let darkgrey = UIColor(withHex: 0x8A8D88)
    static let lightGrey = UIColor(withHex: 0x272626)
    static let redColor = UIColor(withHex: 0xED2024)
     static let oceanColor = UIColor(withHex: 0xC9DD7B)
     static let orangeColor = UIColor(withHex: 0xF36E21)
     static let pinkColor = UIColor(withHex: 0xCA4B9B)

}
extension UIButton {
    func addTextSpacing(spacing: CGFloat){
       let attributedString = NSMutableAttributedString(string: (self.titleLabel?.text!)!)
        attributedString.addAttribute(NSAttributedString.Key.kern, value: spacing, range: NSRange(location: 0, length: (self.titleLabel?.text!.count)!))
       self.setAttributedTitle(attributedString, for: .normal)
   }
}

@IBDesignable class setCharacterSpaceLbl: UILabel {

    @IBInspectable public var spacing: CGFloat = 0.0 {
        didSet {
            applyKerning()
        }
    }

    override var text: String? {
        didSet {
            applyKerning()
        }
    }

    private func applyKerning() {
        let stringValue = self.text ?? ""
        let attrString = NSMutableAttributedString(string: stringValue)
        attrString.addAttribute(NSAttributedString.Key.kern, value: spacing, range: NSMakeRange(0, attrString.length))
        self.attributedText = attrString
    }
}

@IBDesignable class setGradientButton: UIButton {
    
    @IBInspectable var firstColr: UIColor = ProjectColor.yellow
    @IBInspectable var secColr: UIColor = ProjectColor.darkGreen
    @IBInspectable var titleColr: UIColor = UIColor.white
    
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath()
        //self.setTitleColor(titleColr, for: .normal)
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.startPoint = CGPoint(x: 0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1, y: 0.5)
        gradient.colors = [firstColr.cgColor, secColr.cgColor]
        self.layer.insertSublayer(gradient, at: 0)
        path.fill()
        path.close()
    }
}

@IBDesignable class setGradientView: UIView {
    
    @IBInspectable var firstColr: UIColor = ProjectColor.yellow
    @IBInspectable var secColr: UIColor = ProjectColor.darkGreen
    @IBInspectable var titleColr: UIColor = UIColor.white
    @IBInspectable var startPont: CGPoint = CGPoint(x: 0, y: 0)
    @IBInspectable var endPont: CGPoint = CGPoint(x: 0, y: 1)
    @IBInspectable var corners: CGFloat = 12
    
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath()
        //self.setTitleColor(titleColr, for: .normal)
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.startPoint = startPont//CGPoint(x: 0, y: 0.5)
        gradient.endPoint = endPont//CGPoint(x: 1, y: 0.5)
        gradient.colors = [firstColr.cgColor, secColr.cgColor]
        gradient.cornerRadius = corners
        self.layer.insertSublayer(gradient, at: 0)
        path.fill()
        path.close()
    }
}

