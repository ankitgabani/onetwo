//
//  ChatVC.swift
//  OneTwo
//
//  Created by appt on 15/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class ChatVC: UIViewController ,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var btMusic: UIButton!
    @IBOutlet weak var btnImage: UIButton!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var btnFil: UIButton!
    
    
    @IBOutlet weak var viewAllTpyes: UIView!
    
    @IBOutlet weak var allTypeViewConat: NSLayoutConstraint!
    
    @IBOutlet weak var allFileCont: NSLayoutConstraint!
    
    @IBOutlet weak var allImageCont: NSLayoutConstraint!
   
    @IBOutlet weak var allCameraCont: NSLayoutConstraint!
 
    @IBOutlet weak var allMusicCont: NSLayoutConstraint!
    
    @IBOutlet weak var textChat: UITextField!
    @IBOutlet weak var btnMessage: UIButton!
    @IBOutlet weak var btnVoice: UIButton!
    
    @IBOutlet weak var btnAddCancel: UIButton!
    
    var isCancelEvent = false
    override func viewDidLoad() {
        super.viewDidLoad()
        viewAllTpyes.isHidden = true
        btMusic.isHidden = true
        btnImage.isHidden = true
        btnCamera.isHidden = true
        btnFil.isHidden = true
        allTypeViewConat.constant = 0
        
       // allFileCont.constant = 0
       // allImageCont.constant = 0
       // allCameraCont.constant = 0
      //  allMusicCont.constant = 0

        isCancelEvent = false
        btnAddCancel.setImage(UIImage(named: "ic_Add"), for: .normal)

        btnVoice.isHidden = false
        btnMessage.isHidden = true
        
        textChat.layer.cornerRadius = 15
        textChat.layer.masksToBounds = true
        textChat.backgroundColor = UIColor.black
         hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white,
                          NSAttributedString.Key.font: UIFont(name: "Futura-MediumItalic", size: 17)]

        textChat.attributedPlaceholder = NSAttributedString(string: "message",
                                                            attributes: attributes)
        
        textChat.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)

    }
    
    @objc private func textFieldDidChange(textField: UITextField) {
        
        if textField.text == "" {
            btnVoice.isHidden = false
            btnMessage.isHidden = true
        } else {
            btnVoice.isHidden = true
            btnMessage.isHidden = false
         }
    }
    
    //MARK:- Action
    
    @IBAction func clickedMusic(_ sender: Any) {
    }
    
    @IBAction func clickedCamera(_ sender: Any) {
    }
    
    @IBAction func clickedImages(_ sender: Any) {
    }
    
    @IBAction func clickedFile(_ sender: Any) {
    }
    
    
    @IBAction func didTapBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickedSendMessage(_ sender: Any) {
        
    }
    
    @IBAction func clickedSendVoice(_ sender: Any) {
       
    }
    
    @IBAction func clickedAddCancel(_ sender: Any) {
        if isCancelEvent == true {
            isCancelEvent = false
            btnAddCancel.setImage(UIImage(named: "ic_Add"), for: .normal)
            perform(#selector(stopAnimLogo), with: nil, afterDelay: 0.0)
            
        } else {
            isCancelEvent = true
            btnAddCancel.setImage(UIImage(named: "ic_cancel"), for: .normal)
            allTypeViewConat.constant = 160
 
            perform(#selector(startAnimLogo), with: nil, afterDelay: 0.2)
        }
    }
    
    @objc func startAnimLogo() {
        
        viewAllTpyes.isHidden = false
        btMusic.isHidden = false
        btnImage.isHidden = false
        btnCamera.isHidden = false
        btnFil.isHidden = false

        viewAllTpyes.slideIn(from: kFTAnimationLeft, in: viewAllTpyes.superview, duration: 0.4, delegate: self, start:Selector("temp") , stop: Selector("temp"))
        
    }
    
    @objc func stopAnimLogo() {
        
        viewAllTpyes.slideOut(to: kFTAnimationLeft, duration: 0.8, delegate: self, start: Selector("temp"), stop: Selector("temp"))
        
        allTypeViewConat.constant = 0
        viewAllTpyes.isHidden = true
        btMusic.isHidden = true
        btnImage.isHidden = true
        btnCamera.isHidden = true
        btnFil.isHidden = true

    }
    
    @IBAction func imageBtnClick(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "CameraVC") as! CameraVC
               self.present(vc, animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0,1:
            return 1
        default:
           return 1
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
     return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       switch indexPath.section {
        case 0:
        let cellOne = tableView.dequeueReusableCell(withIdentifier: "ChatCellOne", for: indexPath) as! ChatCellOne
        return cellOne
        case 1:
        let cellTwo = tableView.dequeueReusableCell(withIdentifier: "ChatCellTwo", for: indexPath) as! ChatCellTwo
        return cellTwo
        default:
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCellTwo", for: indexPath) as! ChatCellTwo
        return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
}

class ChatCellOne : UITableViewCell {
    
}

class ChatCellTwo: UITableViewCell {
}
