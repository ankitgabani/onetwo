//
//  APIClient.swift
//  Luxury Library
//
//  Created by Gabani on 08/06/20.
//  Copyright © 2020 Ankit Gabani. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

@available(iOS 13.0, *)
class APIClient: NSObject {
    
    class var sharedInstance: APIClient {
        
        struct Static {
            static let instance: APIClient = APIClient()
        }
        return Static.instance
    }
    
    var responseData: NSMutableData!

    func MakeLoginAPICall(email: String, password: String, completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        let parameters = ["login": email, "password": password]
        print(parameters)
        
        UserDefaults.standard.set(email, forKey: "currentUserEmail")
        UserDefaults.standard.set(password, forKey: "currentUserPassword")
        UserDefaults.standard.synchronize()
        
        if NetConnection.isConnectedToNetwork() == true
        {
            
            //let headers: HTTPHeaders = [:]
            
            Alamofire.request(BASE_URL + USER_LOGIN, method: .post, parameters: parameters, encoding: URLEncoding(destination: .methodDependent), headers: [:]).responseJSON { response in
                
                switch(response.result) {
                    
                case .success:
                    if response.result.value != nil{
                        if let responseDict = ((response.result.value as AnyObject) as? NSDictionary) {
                            completionHandler(responseDict, nil, response.response?.statusCode)
                        }
                    }
                case .failure:
                    print(response.result.error!)
                    completionHandler(nil, response.result.error, response.response?.statusCode)
                }
            }
        }
        else
        {
            print("No Network Found!")
            
        }
    }
    
    func MakeAPICallWithoutAuthHeaderPost(_ url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        print("url = \(BASE_URL + url)")
        print(parameters)
        
        if NetConnection.isConnectedToNetwork() == true
        {
            Alamofire.request(BASE_URL + url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: [:]).responseJSON { response in
                
                switch(response.result) {
                    
                case .success:
                    if response.result.value != nil{
                        if let responseDict = ((response.result.value as AnyObject) as? NSDictionary) {
                            completionHandler(responseDict, nil, response.response?.statusCode)
                        }
                    }
                    
                case .failure:
                    print(response.result.error!)
                    print("Http Status Code: \(String(describing: response.response?.statusCode))")
                    completionHandler(nil, response.result.error, response.response?.statusCode )
                }
            }
        }
        else
        {
            print("No Network Found!")
           
        }
    }
    
    func MakeAPICallWithoutAuthHeaderGet(_ url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        print("url = \(BASE_URL + url)")
        
        if NetConnection.isConnectedToNetwork() == true
        {
            Alamofire.request(BASE_URL + url, method: .get, encoding: URLEncoding(destination: .methodDependent), headers: [:]).responseJSON { response in
                
                switch(response.result) {
                    
                case .success:
                    if response.result.value != nil{
                        if let responseDict = ((response.result.value as AnyObject) as? NSDictionary) {
                            completionHandler(responseDict, nil, response.response?.statusCode)
                        }
                    }
                    
                case .failure:
                    print(response.result.error!)
                    print("Http Status Code: \(String(describing: response.response?.statusCode))")
                    completionHandler(nil, response.result.error, response.response?.statusCode )
                }
            }
        }
        else
        {
            print("No Network Found!")
           
        }
    }
    
    func MakeAPICallWithoutAuthHeaderGetingLINK(_ url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
           
           print("url = \(url)")
           
           if NetConnection.isConnectedToNetwork() == true
           {
               Alamofire.request(url, method: .get, encoding: URLEncoding(destination: .methodDependent), headers: [:]).responseJSON { response in
                   
                   switch(response.result) {
                       
                   case .success:
                       if response.result.value != nil{
                           if let responseDict = ((response.result.value as AnyObject) as? NSDictionary) {
                               completionHandler(responseDict, nil, response.response?.statusCode)
                           }
                       }
                       
                   case .failure:
                       print(response.result.error!)
                       print("Http Status Code: \(String(describing: response.response?.statusCode))")
                       completionHandler(nil, response.result.error, response.response?.statusCode )
                   }
               }
           }
           else
           {
               print("No Network Found!")
              
           }
       }
    
    func MakeAPICallWihAuthHeaderGetForget(_ url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        print("url = \(BASE_URL + url)")
        
        if NetConnection.isConnectedToNetwork() == true
        {
            let user_token = UserDefaults.standard.value(forKey: "user_token") as? String
            
//            let headers: HTTPHeaders = ["Authorization":"Bearer \(user_token ?? "")"]
//            print(user_token ?? "")
            
            Alamofire.request(BASE_URL + url, method: .get, parameters: parameters, encoding: URLEncoding(destination: .methodDependent), headers: [:]).responseJSON { response in
                
                switch(response.result) {
                    
                case .success:
                    if response.result.value != nil{
                        if let responseDict = ((response.result.value as AnyObject) as? NSDictionary) {
                            completionHandler(responseDict, nil, response.response?.statusCode)
                        }
                    }
                    
                case .failure:
                    print(response.result.error!)
                    print("Http Status Code: \(String(describing: response.response?.statusCode))")
                    completionHandler(nil, response.result.error, response.response?.statusCode )
                }
            }
        }
        else
        {
            print("No Network Found!")
            
        }
    }
    
    func postProfileImageToServer(_ url: String, image: UIImage!, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void){
        
        let finalUrl = BASE_URL + url
        print("Requesting \(finalUrl)")
        print("Parameters: \(parameters)")
        
        let imageData = image.jpegData(compressionQuality: 1)
        
        if NetConnection.isConnectedToNetwork() == true
        {
            
            if (imageData != nil)
            {
                
                let user_token = UserDefaults.standard.value(forKey: "user_token") as? String
                
                let authorization = "Bearer \(user_token as! String)"
                
                let headers: HTTPHeaders = ["Authorization":authorization]
                
                //                let headers: HTTPHeaders = [
                //                    "Content-type": "multipart/form-data",
                //                    "Authorization": authorization
                //                ]
                
                Alamofire.upload(multipartFormData: { (multipartFormData) in
                    for (key, value) in parameters {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                    }
                    multipartFormData.append(imageData!, withName: "avatar", fileName: "photo.jpg", mimeType: "image/jpeg")
                    
                }, usingThreshold: UInt64.init(), to: finalUrl, method: .post, headers: headers) { (result) in
                    switch result{
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            print("Succesfully uploaded")
                            if let err = response.error{
                                completionHandler(nil, err, response.response?.statusCode )
                                
                                return
                            }
                            if let responseDict = ((response.result.value as AnyObject) as? NSDictionary) {
                                completionHandler(responseDict, nil, response.response?.statusCode)
                            }
                            
                            
                        }
                    case .failure(let error):
                        print("Error in upload: \(error.localizedDescription)")
                        completionHandler(nil, error, -1 )
                    }
                }
            }
        }else
        {
            
        }
    }
    
    
    func MakeAPICallWihAuthHeaderPost(_ url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        print("url = \(BASE_URL + url)")
        
        if NetConnection.isConnectedToNetwork() == true
        {
            let user_token = UserDefaults.standard.value(forKey: "user_token") as? String
            
            let headers: HTTPHeaders = ["Content-Type": "application/json", "Authorization":"Bearer \(user_token ?? "")"]
            print(user_token ?? "")
            
            Alamofire.request(BASE_URL + url, method: .post, encoding: URLEncoding(destination: .httpBody), headers: headers).responseJSON { response in
                
                switch(response.result) {
                    
                case .success:
                    if response.result.value != nil{
                        if let responseDict = ((response.result.value as AnyObject) as? NSDictionary) {
                            completionHandler(responseDict, nil, response.response?.statusCode)
                        }
                    }
                    
                case .failure:
                    print(response.result.error!)
                    print("Http Status Code: \(String(describing: response.response?.statusCode))")
                    completionHandler(nil, response.result.error, response.response?.statusCode )
                }
            }
        }
        else
        {
            print("No Network Found!")
           
        }
    }
    
    func MakeAPICallWihAuthHeaderGets(_ url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        print("url = \(BASE_URL + url)")
        
        if NetConnection.isConnectedToNetwork() == true
        {
            let user_token = UserDefaults.standard.value(forKey: "user_token") as? String
            
            let tokenID = UserDefaults.standard.value(forKey: "TokenID") as? String
            let customerID = UserDefaults.standard.value(forKey: "CustomerID") as? Int
                        
            let headers: HTTPHeaders = ["Content-Type": "application/json","TokenID": tokenID!,"CustomerID" : "\(customerID ?? 0)"]
            print(user_token ?? "")
            
            Alamofire.request(BASE_URL + url, method: .get, encoding: URLEncoding(destination: .methodDependent), headers: headers).responseJSON { response in
                
                switch(response.result) {
                    
                case .success:
                    if response.result.value != nil{
                        if let responseDict = ((response.result.value as AnyObject) as? NSDictionary) {
                            completionHandler(responseDict, nil, response.response?.statusCode)
                        }
                    }
                    
                case .failure:
                    print(response.result.error!)
                    print("Http Status Code: \(String(describing: response.response?.statusCode))")
                    completionHandler(nil, response.result.error, response.response?.statusCode )
                }
            }
        }
        else
        {
            print("No Network Found!")
           
        }
    }

    func MakeAPICallWihAuthHeaderToken(_ url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        print("url = \(BASE_URL + url)")
        
        if NetConnection.isConnectedToNetwork() == true
        {
            
            let tokenID = UserDefaults.standard.value(forKey: "TokenID") as? String
            
            let headers: HTTPHeaders = ["TokenID": tokenID!]
            print(tokenID ?? "")
            
            Alamofire.request(BASE_URL + url, method: .get, parameters: parameters, encoding: URLEncoding(destination: .methodDependent), headers: headers).responseJSON { response in
                
                switch(response.result) {
                    
                case .success:
                    if response.result.value != nil{
                        if let responseDict = ((response.result.value as AnyObject) as? NSDictionary) {
                            completionHandler(responseDict, nil, response.response?.statusCode)
                        }
                    }
                    
                case .failure:
                    print(response.result.error!)
                    print("Http Status Code: \(String(describing: response.response?.statusCode))")
                    completionHandler(nil, response.result.error, response.response?.statusCode )
                }
            }
        }
        else
        {
            print("No Network Found!")
           
        }
    }
    
//    func MakeAPICallWihAuthHeaderTokenPostMe(_ url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
//
//        print("url = \(BASE_URL + url)")
//
//        if NetConnection.isConnectedToNetwork() == true
//        {
//
//            let tokenID = UserDefaults.standard.value(forKey: "TokenID") as? String
//
//            let headers: HTTPHeaders = ["TokenID": tokenID!]
//            print(tokenID ?? "")
//
//            Alamofire.request(BASE_URL + url, method: .post, parameters: parameters, encoding: URLEncoding(destination: .methodDependent), headers: headers).responseJSON { response in
//
//                switch(response.result) {
//
//                case .success:
//                    if response.result.value != nil{
//                        if let responseDict = ((response.result.value as AnyObject) as? NSDictionary) {
//                            completionHandler(responseDict, nil, response.response?.statusCode)
//                        }
//                    }
//
//                case .failure:
//                    print(response.result.error!)
//                    print("Http Status Code: \(String(describing: response.response?.statusCode))")
//                    completionHandler(nil, response.result.error, response.response?.statusCode )
//                }
//            }
//        }
//        else
//        {
//            print("No Network Found!")
//            pushNetworkErrorVC()
//            SVProgressHUD.dismiss()
//        }
//    }
    
    func MakeAPICallWihAuthHeaderTokenQuery(_ url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        print("url = \(BASE_URL + url)")
        
        if NetConnection.isConnectedToNetwork() == true
        {
            
            let tokenID = UserDefaults.standard.value(forKey: "TokenID") as? String
            
            let headers: HTTPHeaders = ["TokenID": tokenID!]
            print(tokenID ?? "")
            
            Alamofire.request(BASE_URL + url, method: .get, parameters: parameters, encoding: URLEncoding(destination: .queryString), headers: headers).responseJSON { response in
                
                switch(response.result) {
                    
                case .success:
                    if response.result.value != nil{
                        if let responseDict = ((response.result.value as AnyObject) as? NSDictionary) {
                            completionHandler(responseDict, nil, response.response?.statusCode)
                        }
                    }
                    
                case .failure:
                    print(response.result.error!)
                    print("Http Status Code: \(String(describing: response.response?.statusCode))")
                    completionHandler(nil, response.result.error, response.response?.statusCode )
                }
            }
        }
        else
        {
            print("No Network Found!")
           
        }
    }
    
    func MakeAPICallWihAuthHeaderTokenPost(_ url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        print("url = \(BASE_URL + url)")
        
        if NetConnection.isConnectedToNetwork() == true
        {
            
            let tokenID = UserDefaults.standard.value(forKey: "TokenID") as? String
            
            let headers: HTTPHeaders = ["TokenID": tokenID!]
            print(tokenID ?? "")
            
            Alamofire.request(BASE_URL + url, method: .post, parameters: parameters, encoding: URLEncoding(destination: .queryString), headers: headers).responseJSON { response in
                
                switch(response.result) {
                    
                case .success:
                    if response.result.value != nil{
                        if let responseDict = ((response.result.value as AnyObject) as? NSDictionary) {
                            completionHandler(responseDict, nil, response.response?.statusCode)
                        }
                    }
                    
                case .failure:
                    print(response.result.error!)
                    print("Http Status Code: \(String(describing: response.response?.statusCode))")
                    completionHandler(nil, response.result.error, response.response?.statusCode )
                }
            }
        }
        else
        {
            print("No Network Found!")
            
        }
    }
    
    func MakeAPICallWihAuthHeaderTokenPostHTTP(_ url: String, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void) {
        
        print("url = \(BASE_URL + url)")
        
        if NetConnection.isConnectedToNetwork() == true
        {
            
            let tokenID = UserDefaults.standard.value(forKey: "TokenID") as? String
            
            let headers: HTTPHeaders = ["TokenID": tokenID!]
            print(tokenID ?? "")
            
            Alamofire.request(BASE_URL + url, method: .post, parameters: parameters, encoding: URLEncoding(destination: .httpBody), headers: headers).responseJSON { response in
                
                switch(response.result) {
                    
                case .success:
                    if response.result.value != nil{
                        if let responseDict = ((response.result.value as AnyObject) as? NSDictionary) {
                            completionHandler(responseDict, nil, response.response?.statusCode)
                        }
                    }
                    
                case .failure:
                    print(response.result.error!)
                    print("Http Status Code: \(String(describing: response.response?.statusCode))")
                    completionHandler(nil, response.result.error, response.response?.statusCode )
                }
            }
        }
        else
        {
            print("No Network Found!")
            
        }
    }
  
    
    func postImageToServer(_ url: String, image: UIImage!, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void){
        
        let finalUrl = BASE_URL + url
        print("Requesting \(finalUrl)")
        print("Parameters: \(parameters)")
        
        let imageData = image.jpegData(compressionQuality: 0.1)
        
        let tokenID = UserDefaults.standard.value(forKey: "TokenID") as? String
        
        // let headers: HTTPHeaders = ["TokenID": tokenID!]
        
        let authorizationStr = tokenID!
        
        if NetConnection.isConnectedToNetwork() == true
        {
            //        let imageData  = UIImagePNGRepresentation(image)
            
            if (imageData != nil)
            {
                
                let headers: HTTPHeaders = [
                    /* "Authorization": "your_access_token",  in case you need authorization header */
                    "Content-type": "multipart/form-data",
                    "Authorization": authorizationStr
                ]
                
                Alamofire.upload(multipartFormData: { (multipartFormData) in
                    for (key, value) in parameters {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                    }
                    multipartFormData.append(imageData!, withName: "fileupload", fileName: "photo.jpg", mimeType: "image/jpeg")
                    
                }, usingThreshold: UInt64.init(), to: finalUrl, method: .post, headers: headers) { (result) in
                    switch result{
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            print("Succesfully uploaded")
                            if let err = response.error{
                                completionHandler(nil, err, response.response?.statusCode)
                                
                                return
                            }
                            if let responseDict = ((response.result.value as AnyObject) as? NSDictionary) {
                                completionHandler(responseDict, nil, response.response?.statusCode)
                            }
                            
                            
                        }
                        upload.uploadProgress(closure: { (progrress) in
                            print(progrress.fractionCompleted)
                        })
                    case .failure(let error):
                        print("Error in upload: \(error.localizedDescription)")
                        completionHandler(nil, error, -1 )
                    }
                }
            }
        }else
        {
            
            
        }
    }
    
    func postImageToServerWithOutToken(_ url: String, fileName: String, image: UIImage!, parameters: [String: Any], completionHandler:@escaping (NSDictionary?, Error?, Int?) -> Void){
        
        let finalUrl = BASE_URL + url
        print("Requesting \(finalUrl)")
        print("Parameters: \(parameters)")
        
        let imageData = image.jpegData(compressionQuality: 0.1)
    
        if NetConnection.isConnectedToNetwork() == true
        {
            //        let imageData  = UIImagePNGRepresentation(image)
            
            if (imageData != nil)
            {
         
                Alamofire.upload(multipartFormData: { (multipartFormData) in
                    for (key, value) in parameters {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                    }
                    multipartFormData.append(imageData!, withName: "picture", fileName: fileName, mimeType: "image/jpeg")
                    
                }, usingThreshold: UInt64.init(), to: finalUrl, method: .post, headers: [:]) { (result) in
                    switch result{
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            print("Succesfully uploaded")
                            if let err = response.error{
                                completionHandler(nil, err, response.response?.statusCode)
                                
                                return
                            }
                            if let responseDict = ((response.result.value as AnyObject) as? NSDictionary) {
                                completionHandler(responseDict, nil, response.response?.statusCode)
                            }
                            
                            
                        }
                        upload.uploadProgress(closure: { (progrress) in
                            print(progrress.fractionCompleted)
                        })
                    case .failure(let error):
                        print("Error in upload: \(error.localizedDescription)")
                        completionHandler(nil, error, -1 )
                    }
                }
            }
        }else
        {
          
            
        }
    }
    
//    func showIndicator(){
//        SVProgressHUD.show()
//    }
//
//    func hideIndicator(){
//        SVProgressHUD.dismiss()
//    }
//
//    func showSuccessIndicator(message: String){
//        SVProgressHUD.showSuccess(withStatus: message)
//    }
}
