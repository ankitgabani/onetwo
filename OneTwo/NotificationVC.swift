//
//  NotificationVC.swift
//  OneTwo
//
//  Created by appt on 15/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
@available(iOS 13.0, *)
class NotificationVC: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var backgroundImg: UIImageView!
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var tblView: UITableView!
    
    var arrGetInvitationList: [OTNotification] = [OTNotification]()
    var arrLiveMatchList: [OTNotification] = [OTNotification]()
    var arrTeamRequestList: [OTNotification] = [OTNotification]()
    var arrMatchRequestList: [OTNotification] = [OTNotification]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
        setupBack()
        callGetInvitationListAPI()
        callLiveMatchListAPI()
        callMatchRequestListAPI()
        callTeamRequestListAPI()
    }
    private func setupBack() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapBack(_:)))
        viewBackground.isUserInteractionEnabled = true
        viewBackground.addGestureRecognizer(gesture)
    }
    
    
    @objc func didTapBack(_ sender: UIButton) {
        if navigationController != nil {
            self.navigationController?.popViewController(animated: true)
        }else {
            let transition = CATransition()
            transition.duration = 0.5
            transition.type = CATransitionType.fade
            transition.subtype = CATransitionSubtype.fromRight
            transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
            view.window!.layer.add(transition, forKey: kCATransition)
            self.dismiss(animated: false, completion: nil)
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return arrLiveMatchList.count
        } else if section == 1 {
            return arrMatchRequestList.count
        } else if section == 2 {
            return arrGetInvitationList.count
        } else {
            return arrTeamRequestList.count
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let objOrdersList = self.arrLiveMatchList[indexPath.row]
            let vc = self.storyboard?.instantiateViewController(identifier: "BookingDetailsVC") as! BookingDetailsVC
            
            let booking_id = objOrdersList.booking_id ?? ""
            UserDefaults.standard.setValue(Int(booking_id), forKey: "booking_id")
            UserDefaults.standard.synchronize()
            vc.ifFromNotification = true
            
           self.present(vc, animated: true, completion: nil)
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tblView.dequeueReusableCell(withIdentifier: "NotificationCellOne") as! NotificationCellOne
            let objOrdersList = self.arrLiveMatchList[indexPath.row]
            let name = objOrdersList.pitch_name ?? ""
            cell.liveMatcheslLbl.text = name.uppercased()
            
            return cell
        } else if indexPath.section == 1 {
            let cell = tblView.dequeueReusableCell(withIdentifier: "tblCell") as! tblCell
            let objOrdersList = self.arrMatchRequestList[indexPath.row]
            let name = objOrdersList.team_name ?? ""
            cell.liveMatcheslLbl.text = name.uppercased()
            
            cell.btnTrue.tag = indexPath.row
            cell.btnTrue.removeTarget(self, action: #selector(btnAccpetRequestClicked), for: .touchUpInside)
            cell.btnTrue.removeTarget(self, action: #selector(btnAccpetResponseTeamClicked), for: .touchUpInside)
            cell.btnTrue.addTarget(self, action: #selector(btnAccpetResponseMatchClicked(_:)), for: .touchUpInside)
            
            cell.btnFalse.tag = indexPath.row
            cell.btnFalse.removeTarget(self, action: #selector(btnCancelRequestClicked), for: .touchUpInside)
            cell.btnFalse.removeTarget(self, action: #selector(btnCancelResponseTeamClicked), for: .touchUpInside)
            cell.btnFalse.addTarget(self, action: #selector(btnCancelResponseMatchClicked(_:)), for: .touchUpInside)
            
            return cell
        } else if indexPath.section == 2 {
            let cell = tblView.dequeueReusableCell(withIdentifier: "tblCell") as! tblCell
            let objOrdersList = self.arrGetInvitationList[indexPath.row]
            let name = objOrdersList.name ?? ""
            cell.liveMatcheslLbl.text = name.uppercased()
            
            cell.btnTrue.tag = indexPath.row
            cell.btnTrue.removeTarget(self, action: #selector(btnAccpetResponseTeamClicked), for: .touchUpInside)
            cell.btnTrue.removeTarget(self, action: #selector(btnAccpetResponseMatchClicked(_:)), for: .touchUpInside)
            cell.btnTrue.addTarget(self, action: #selector(btnAccpetRequestClicked), for: .touchUpInside)
            
            cell.btnFalse.tag = indexPath.row
            cell.btnFalse.removeTarget(self, action: #selector(btnCancelResponseTeamClicked), for: .touchUpInside)
            cell.btnFalse.removeTarget(self, action: #selector(btnCancelResponseMatchClicked(_:)), for: .touchUpInside)
            cell.btnFalse.addTarget(self, action: #selector(btnCancelRequestClicked), for: .touchUpInside)
            
            return cell
        } else {
            let cell = tblView.dequeueReusableCell(withIdentifier: "tblCell") as! tblCell
            let objOrdersList = self.arrTeamRequestList[indexPath.row]
            let name = objOrdersList.team_name ?? ""
            cell.liveMatcheslLbl.text = name.uppercased()
            
            cell.btnTrue.tag = indexPath.row
            cell.btnTrue.removeTarget(self, action: #selector(btnAccpetRequestClicked), for: .touchUpInside)
            cell.btnTrue.removeTarget(self, action: #selector(btnAccpetResponseMatchClicked(_:)), for: .touchUpInside)
            cell.btnTrue.addTarget(self, action: #selector(btnAccpetResponseTeamClicked), for: .touchUpInside)
            
            cell.btnFalse.tag = indexPath.row
            cell.btnFalse.removeTarget(self, action: #selector(btnCancelRequestClicked), for: .touchUpInside)
            cell.btnFalse.removeTarget(self, action: #selector(btnCancelResponseMatchClicked(_:)), for: .touchUpInside)
            cell.btnFalse.addTarget(self, action: #selector(btnCancelResponseTeamClicked), for: .touchUpInside)
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 38
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            if arrLiveMatchList.count == 0 {
                return .leastNormalMagnitude
            } else {
                return 38
            }
        } else if section == 1 {
            if arrMatchRequestList.count == 0 {
                return .leastNormalMagnitude
            } else {
                return 38
            }
        } else if section == 2 {
            if arrGetInvitationList.count == 0 {
                return .leastNormalMagnitude
            } else {
                return 38
            }
        } else {
            if arrTeamRequestList.count == 0 {
                return .leastNormalMagnitude
            } else {
                return 38
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 {
            if arrLiveMatchList.count == 0 {
                return .leastNormalMagnitude
            } else {
                return 20
            }
        } else if section == 1 {
            if arrMatchRequestList.count == 0 {
                return .leastNormalMagnitude
            } else {
                return 20
            }
        } else if section == 2 {
            if arrGetInvitationList.count == 0 {
                return .leastNormalMagnitude
            } else {
                return 20
            }
        } else {
            if arrTeamRequestList.count == 0 {
                return .leastNormalMagnitude
            } else {
                return 20
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = Bundle.main.loadNibNamed("LiveMatach", owner: self, options: [:])?.first as! LiveMatach
        
        if section == 0 {
            headerView.headerLbl.textColor = UIColor(red: 201/255, green: 221/255, blue: 123/255, alpha: 0.83)
        } else {
            headerView.headerLbl.textColor = UIColor(red: 243/255, green: 110/255, blue: 33/255, alpha: 0.83)
        }
        
        if section == 0 {
            
            if arrLiveMatchList.count == 0 {
                headerView.headerLbl.text = ""
            } else {
                headerView.headerLbl.text = "LIVE MATCHES (\(arrLiveMatchList.count))"
            }
        } else if section == 1 {
            if arrMatchRequestList.count == 0 {
                headerView.headerLbl.text = ""
            } else {
                headerView.headerLbl.text = "MATCH REQUESTS (\(arrMatchRequestList.count))"
            }
        } else if section == 2 {
            
            if arrGetInvitationList.count == 0 {
                headerView.headerLbl.text = ""
            } else {
                headerView.headerLbl.text = "PLAYER REQUES TS (\(arrGetInvitationList.count))"
            }
            
        } else {
            if arrTeamRequestList.count == 0 {
                headerView.headerLbl.text = ""
            } else {
                headerView.headerLbl.text = "TEAM REQUESTS (\(arrTeamRequestList.count))"
            }
        }
        
        return headerView
    }
    
    
    
    @IBAction func btnCancelRequestClicked(_ sender: AnyObject) {
        
        if sender.tag < arrGetInvitationList.count {
            self.cancleRequestSentClicked(sender.tag)
        }
    }
    @IBAction func btnAccpetRequestClicked(_ sender: AnyObject) {
        
        if sender.tag < arrGetInvitationList.count {
            self.accpetRequestSentClicked(sender.tag)
        }
    }
    func cancleRequestSentClicked(_ index: Int) {
        let commentId = self.arrGetInvitationList[index]
        callCancleInvitationStatusAPI(invitation_id: commentId.id ?? "", status: "2")
    }
    func accpetRequestSentClicked(_ index: Int) {
        let commentId = self.arrGetInvitationList[index]
        callCancleInvitationStatusAPI(invitation_id: commentId.id ?? "", status: "1")
    }
    
    
    @IBAction func btnCancelResponseTeamClicked(_ sender: AnyObject) {
        
        if sender.tag < arrTeamRequestList.count {
            self.cancleResponseTeamClicked(sender.tag)
        }
    }
    @IBAction func btnAccpetResponseTeamClicked(_ sender: AnyObject) {
        
        if sender.tag < arrTeamRequestList.count {
            self.accpetResponseTeamClicked(sender.tag)
        }
    }
    func cancleResponseTeamClicked(_ index: Int) {
        let commentId = self.arrTeamRequestList[index]
        callResponseTeamStatusAPI(invitation_id: commentId.request_id ?? "", status: "2")
    }
    func accpetResponseTeamClicked(_ index: Int) {
        let commentId = self.arrTeamRequestList[index]
        callResponseTeamStatusAPI(invitation_id: commentId.request_id ?? "", status: "1")
    }
    
    @IBAction func btnCancelResponseMatchClicked(_ sender: AnyObject) {
        
        if sender.tag < arrMatchRequestList.count {
            self.cancleResponseMatchClicked(sender.tag)
        }
    }
    @IBAction func btnAccpetResponseMatchClicked(_ sender: AnyObject) {
        
        if sender.tag < arrMatchRequestList.count {
            self.accpetResponseMatchClicked(sender.tag)
        }
    }
    func cancleResponseMatchClicked(_ index: Int) {
        let commentId = self.arrMatchRequestList[index]
        callResponseMatchStatusAPI(invitation_id: commentId.request_id ?? "", status: "2")
    }
    func accpetResponseMatchClicked(_ index: Int) {
        let commentId = self.arrMatchRequestList[index]
        callResponseMatchStatusAPI(invitation_id: commentId.request_id ?? "", status: "1")
    }
    
    //MARK:- API
    
    func callResponseMatchStatusAPI(invitation_id: String,status: String) {
        
        self.showLoader()
        
        let param = ["request_id": invitation_id, "request_status": status] as [String : Any]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(RESPONSE_MATCH_INVITATION_STATUS, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                if statusCode == 200 {
                    
                    if code == 1 {
                        
                        self.view.makeToast(message)
                        self.callMatchRequestListAPI()
                        self.tblView.reloadData()
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
    
    func callResponseTeamStatusAPI(invitation_id: String,status: String) {
        
        self.showLoader()
        
        let param = ["request_id": invitation_id, "request_status": status] as [String : Any]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(RESPONSE_TEAM_INVITATION_STATUS, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                if statusCode == 200 {
                    
                    if code == 1 {
                        
                        self.view.makeToast(message)
                        self.callTeamRequestListAPI()
                        self.tblView.reloadData()
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
    func callCancleInvitationStatusAPI(invitation_id: String,status: String) {
        
        self.showLoader()
        
        let param = ["invitation_id": invitation_id, "status": status] as [String : Any]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(RESPONSE_INVITATION_STATUS, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                if statusCode == 200 {
                    
                    if code == 1 {
                        self.view.makeToast(message)
                        self.callGetInvitationListAPI()
                        self.tblView.reloadData()
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
    
    func callGetInvitationListAPI() {
        
        let userID = UserDefaults.standard.value(forKey: "user_id") as? Int
        
        self.showLoader()
        
        let param = ["to_user_id": userID ?? 0]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(GET_INVITATION_LIST, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                if statusCode == 200 {
                    
                    if code == 1 {
                        
                        self.arrGetInvitationList.removeAll()
                        let arrRespose = response?.value(forKey: "payload") as? NSArray
                        
                        for cartList in arrRespose! {
                            let list = OTNotification(OTNotificationDic: cartList as? NSDictionary)
                            self.arrGetInvitationList.append(list)
                        }
                        
                        self.tblView.reloadData()
                        
                    } else {
                        //self.view.makeToast(message)
                        self.arrGetInvitationList.removeAll()
                        self.tblView.reloadData()

                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
    
    func callLiveMatchListAPI() {
        
        let userID = UserDefaults.standard.value(forKey: "user_id") as? Int
        
        self.showLoader()
        
        let param = ["user_id": userID ?? 0]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(LIVE_MATCHES, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                if statusCode == 200 {
                    
                    if code == 1 {
                        
                        self.arrLiveMatchList.removeAll()
                        let arrRespose = response?.value(forKey: "payload") as? NSArray
                        
                        for cartList in arrRespose! {
                            let list = OTNotification(OTNotificationDic: cartList as? NSDictionary)
                            self.arrLiveMatchList.append(list)
                        }
                        
                        self.tblView.reloadData()
                        
                    } else {
                        //self.view.makeToast(message)
                        self.arrLiveMatchList.removeAll()
                        self.tblView.reloadData()

                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
    
    func callTeamRequestListAPI() {
        
        let userID = UserDefaults.standard.value(forKey: "user_id") as? Int
        
        self.showLoader()
        
        let param = ["user_id": userID ?? 0]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(TEAM_REQUEST, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                if statusCode == 200 {
                    
                    if code == 1 {
                        
                        self.arrTeamRequestList.removeAll()
                        let dicRespose = response?.value(forKey: "payload") as? NSDictionary
                        
                        let arrTeamRequest = dicRespose?.value(forKey: "team_request") as? NSArray
                        
                        for cartList in arrTeamRequest! {
                            let list = OTNotification(OTNotificationDic: cartList as? NSDictionary)
                            self.arrTeamRequestList.append(list)
                        }
                        
                        self.tblView.reloadData()
                        
                    } else {
                        // self.view.makeToast(message)
                        self.arrTeamRequestList.removeAll()
                        self.tblView.reloadData()

                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
    
    func callMatchRequestListAPI() {
        
        let userID = UserDefaults.standard.value(forKey: "user_id") as? Int
        
        self.showLoader()
        
        let param = ["user_id": userID ?? 0]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(MATCH_REQEST, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                if statusCode == 200 {
                    
                    if code == 1 {
                        
                        self.arrMatchRequestList.removeAll()
                        let dicRespose = response?.value(forKey: "payload") as? NSDictionary
                        
                        let arrTeamRequest = dicRespose?.value(forKey: "match_request") as? NSArray
                        
                        for cartList in arrTeamRequest! {
                            let list = OTNotification(OTNotificationDic: cartList as? NSDictionary)
                            self.arrMatchRequestList.append(list)
                        }
                        
                        self.tblView.reloadData()
                        
                    } else {
                        //self.view.makeToast(message)
                        self.arrMatchRequestList.removeAll()
                        self.tblView.reloadData()
                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
    
}
class NotificationCellOne : UITableViewCell {
    @IBOutlet weak var liveMatcheslLbl: UILabel!
    override func awakeFromNib() {
        setupViews()
    }
    private func setupViews() {
        // liveMatcheslLbl.font = UIFont(name: "JerseyM54", size: 22.0)
        
    }
}

class tblCell : UITableViewCell {
    @IBOutlet weak var liveMatcheslLbl: UILabel!
    @IBOutlet weak var btnFalse: UIButton!
    @IBOutlet weak var btnTrue: UIButton!
    override func awakeFromNib() {
        setupViews()
    }
    private func setupViews() {
        // liveMatcheslLbl.font = UIFont(name: "JerseyM54", size: 22.0)
        
    }
}

