//
//  SlashVideoViewController.swift
//  OneTwo
//
//  Created by Ankit on 08/08/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import SideMenuSwift
import MBProgressHUD
import AVFoundation
import AVKit

@available(iOS 13.0, *)
class SlashVideoViewController: BaseViewController,AVPlayerViewControllerDelegate {
    
    var avPlayer: AVPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let playerController = AVPlayerViewController()
        playerController.delegate = self

        let bundle = Bundle.main
        let moviePath: String? = bundle.path(forResource: "FootballVideo", ofType: "mp4")
        let movieURL = URL(fileURLWithPath: moviePath!)

        let player = AVPlayer(url: movieURL)
        playerController.player = player
        playerController.allowsPictureInPicturePlayback = true
        playerController.showsPlaybackControls = false
        self.addChild(playerController)
        self.view.addSubview(playerController.view)
        playerController.view.frame = self.view.frame

        player.play()

        perform(#selector(startAnimLogo), with: nil, afterDelay: 4.4)
        // Do any additional setup after loading the view.
    }
    
    @objc func startAnimLogo() {
        if let userLoggedIn = UserDefaults.standard.value(forKey: "isUserLoggedIn") as? Bool
        {
            if userLoggedIn == true {
                let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: SideMenuController.self)) as! SideMenuController
                SideMenuController.preferences.basic.menuWidth = self.view.frame.width
                SideMenuController.preferences.basic.statusBarBehavior = .slide
                SideMenuController.preferences.basic.position = .above
                SideMenuController.preferences.basic.direction = .left
                SideMenuController.preferences.basic.supportedOrientations = .portrait
                SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
                let window = UIApplication.shared.windows.first { $0.isKeyWindow }
                window?.rootViewController = vc
                window?.makeKeyAndVisible()
            } else {
                let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: LoginVC.self)) as! LoginVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: LoginVC.self)) as! LoginVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}
