//
//  MenuVC.swift
//  OneTwo
//
//  Created by Naveen Yadav on 01/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import SideMenuSwift
class MenuVC: BaseViewController {
    var menuArr = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuArr.append("HOME")
        menuArr.append("PLAYERS LIST")
        menuArr.append("POINTS")
        menuArr.append("MY PROFILE")
        menuArr.append("TERMS & CONDITIONS")
        menuArr.append("ABOUT US")
         hideKeyboardWhenTappedAround()
    }
    
    @IBAction func hideMenuBtn(_ sender: UIButton) {
        self.sideMenuController?.hideMenu()
    }
}

extension MenuVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MenuCell.self), for: indexPath) as? MenuCell else { return UITableViewCell() }
        cell.name.text = menuArr[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if #available(iOS 13.0, *) {
                                
                let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: HomeVC.self)) as! HomeVC
                (self.sideMenuController?.contentViewController as? UINavigationController)?.viewControllers[0] = vc
                self.sideMenuController?.hideMenu()
            }
        }
        else if indexPath.row == 1 {
            if #available(iOS 13.0, *) {
                
                if let userLoggedIn = UserDefaults.standard.value(forKey: "isUserLoggedIn") as? Bool
                {
                    if userLoggedIn == true {
                        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: TeamPlayerVC.self)) as! TeamPlayerVC
                        (self.sideMenuController?.contentViewController as? UINavigationController)?.viewControllers[0] = vc
                        self.sideMenuController?.hideMenu()
                    } else {
                        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: HomeVC.self)) as! HomeVC
                        vc.ifFromSide = true
                        (self.sideMenuController?.contentViewController as? UINavigationController)?.viewControllers[0] = vc
                        self.sideMenuController?.hideMenu()

                    }
                    
                } else {
                    let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: HomeVC.self)) as! HomeVC
                    vc.ifFromSide = true
                    (self.sideMenuController?.contentViewController as? UINavigationController)?.viewControllers[0] = vc
                    self.sideMenuController?.hideMenu()
                 }
             }
        }
        else if indexPath.row == 2 {
            if #available(iOS 13.0, *) {
                let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: PointVC.self)) as! PointVC
                (self.sideMenuController?.contentViewController as? UINavigationController)?.viewControllers[0] = vc
                self.sideMenuController?.hideMenu()
            }
        }
        else if indexPath.row == 3 {
            if #available(iOS 13.0, *) {
                
                if let userLoggedIn = UserDefaults.standard.value(forKey: "isUserLoggedIn") as? Bool
                {
                    if userLoggedIn == true {
                        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: SettingVC.self)) as! SettingVC
                        (self.sideMenuController?.contentViewController as? UINavigationController)?.viewControllers[0] = vc
                        self.sideMenuController?.hideMenu()
                    } else {
                        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: HomeVC.self)) as! HomeVC
                        vc.ifFromSide = true
                        (self.sideMenuController?.contentViewController as? UINavigationController)?.viewControllers[0] = vc
                        self.sideMenuController?.hideMenu()
                    }
                    
                } else {
                    let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: HomeVC.self)) as! HomeVC
                    vc.ifFromSide = true
                    (self.sideMenuController?.contentViewController as? UINavigationController)?.viewControllers[0] = vc
                    self.sideMenuController?.hideMenu()
                }
            }
        }
        else if indexPath.row == 4 {
            if #available(iOS 13.0, *) {
                let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: Term_ConditionVC.self)) as! Term_ConditionVC
                (self.sideMenuController?.contentViewController as? UINavigationController)?.viewControllers[0] = vc
                self.sideMenuController?.hideMenu()
            }
        }
        else if indexPath.row == 5 {
            if #available(iOS 13.0, *) {
                let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: AboutUsVC.self)) as! AboutUsVC
               (self.sideMenuController?.contentViewController as? UINavigationController)?.viewControllers[0] = vc
                self.sideMenuController?.hideMenu()
            }
        }
        
    }
    
}


//MARK:- CELL
class MenuCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

extension MenuVC: SlideDelegate {
    func menuBtnPressed() {
        self.sideMenuController?.revealMenu()
    }
    
    
}
