//
//  LoginVC.swift
//  OneTwo
//
//  Created by Naveen Yadav on 28/05/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import SideMenuSwift
import MBProgressHUD

@available(iOS 13.0, *)
@available(iOS 13.0, *)
class LoginVC: BaseViewController,UITextFieldDelegate {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var txtPhone: UITextField!
    
    @IBOutlet weak var txtPassword: UITextField!
    
    var isFromHome = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnBack.isHidden = true
        
        txtPhone.delegate = self
        txtPassword.delegate = self

//        txtPhone.text = "hemant@apptology.in"
//        txtPassword.text = "12345"
        hideKeyboardWhenTappedAround()
    }
    override func viewWillAppear(_ animated: Bool) {
//        if isFromHome == true {
//            btnBack.isHidden = false
//        }
    }
    
    // MARK: - TextField Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtPhone {
            txtPassword.becomeFirstResponder()
            return false
        }
        textField.resignFirstResponder()
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    @IBAction func resetPassword(_ sender: UIButton) {
        
        if self.isValidatedReset() {
            callResetPassAPI()
        }
        
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: SideMenuController.self)) as! SideMenuController
        SideMenuController.preferences.basic.menuWidth = self.view.frame.width
        SideMenuController.preferences.basic.statusBarBehavior = .slide
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.supportedOrientations = .portrait
        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
        let window = UIApplication.shared.windows.first { $0.isKeyWindow }
        window?.rootViewController = vc
        window?.makeKeyAndVisible()
    }
    
    @IBAction func loginAct(_ sender: UIButton) {
        
        if self.isValidatedLogin() {
            callLoginAPI()
        }
    }
    
    // MARK:- Validation
       func isValidatedReset() -> Bool {
        self.view.hideAllToasts()
           if txtPhone.text == "" {
               self.view.makeToast("Please enter your email")
               return false
           }
           return true
       }
    
    // MARK:- Validation
    func isValidatedLogin() -> Bool {
          self.view.hideAllToasts()
        if txtPhone.text == "" {
            self.view.makeToast("Please enter your phone number or email")
            return false
        } else if txtPassword.text == "" {
            self.view.makeToast("Please enter your password")
            return false
        }
        return true
    }
    
    @IBAction func registerAct(_ sender: UIButton) {
        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: RegisterVC.self)) as! RegisterVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - API Call
    func callLoginAPI() {
        
        self.showLoader()
        
        let deviceID = UIDevice.current.identifierForVendor?.uuidString
        
        let param = ["login": txtPhone.text!,"password": txtPassword.text!,"device_token": deviceID ?? ""]
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(USER_LOGIN, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let IsSuccess = response?["success"] as? Bool
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                
                if statusCode == 201 {
                    
                    if code == 1 {
                        
                        let objPayload = response?["payload"] as? NSDictionary
                        let email = objPayload?.value(forKey: "email") as? String
                        let f_name = objPayload?.value(forKey: "f_name") as? String
                        let l_name = objPayload?.value(forKey: "l_name") as? String
                        let m_name = objPayload?.value(forKey: "m_name") as? String
                        let phone = objPayload?.value(forKey: "phone") as? String
                        let token = objPayload?.value(forKey: "token") as? String
                        let user_id = objPayload?.value(forKey: "user_id") as? Int
                        let username = objPayload?.value(forKey: "username") as? String
                        let token_type = objPayload?.value(forKey: "token_type") as? String

                        UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
                        UserDefaults.standard.set(token_type, forKey: "token_type")
                        UserDefaults.standard.set(user_id, forKey: "user_id")
                        UserDefaults.standard.set(username, forKey: "username")
                        UserDefaults.standard.set(token, forKey: "token")
                        UserDefaults.standard.set(phone, forKey: "phone")
                        UserDefaults.standard.set(m_name, forKey: "m_name")
                        UserDefaults.standard.set(l_name, forKey: "l_name")
                        UserDefaults.standard.set(f_name, forKey: "f_name")
                        UserDefaults.standard.set(email, forKey: "email")
                        UserDefaults.standard.synchronize()
                        
                        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: SideMenuController.self)) as! SideMenuController
                        SideMenuController.preferences.basic.menuWidth = self.view.frame.width
                        SideMenuController.preferences.basic.statusBarBehavior = .slide
                        SideMenuController.preferences.basic.position = .above
                        SideMenuController.preferences.basic.direction = .left
                        SideMenuController.preferences.basic.supportedOrientations = .portrait
                        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
                        let window = UIApplication.shared.windows.first { $0.isKeyWindow }
                        window?.rootViewController = vc
                        window?.makeKeyAndVisible()
                        //        self.navigationController?.pushViewController(vc, animated: true)
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    self.view.makeToast("Email id or password does not match")
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
    
    func callResetPassAPI() {
          
          self.showLoader()
          
          let param = ["email": txtPhone.text!]
          
          APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(RESET_PASSWORD, parameters: param, completionHandler: { (response, error, statusCode) in
              
              if error == nil {
                  print("STATUS CODE \(String(describing: statusCode))")
                  print("Response \(String(describing: response))")
                  
                  let message = response?["message"] as? String ?? ""
                  let IsSuccess = response?["success"] as? Bool
                  let code = response?["code"] as? Int ?? 0
                  
                  self.hideLoader()
                  
                  if statusCode == 201 {
                      
                      if code == 1 {
                          self.view.makeToast(message)
                      } else {
                          self.view.makeToast(message)
                      }
                      
                  } else {
                      
                      self.view.makeToast(message)
                  }
                  
              } else {
                  self.hideLoader()
                  print("Response \(String(describing: response))")
                  let message = response?["message"] as? String ?? ""
                  self.view.makeToast(message)
              }
          })
      }
}
@IBDesignable
extension UITextField {

    @IBInspectable var paddingLeftCustom: CGFloat {
        get {
            return leftView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            leftView = paddingView
            leftViewMode = .always
        }
    }

    @IBInspectable var paddingRightCustom: CGFloat {
        get {
            return rightView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            rightView = paddingView
            rightViewMode = .always
        }
    }
}
