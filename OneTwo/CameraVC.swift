//
//  CameraVC.swift
//  OneTwo
//
//  Created by appt on 25/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class CameraVC: UIViewController {

    @IBOutlet weak var viewHide: UIView!
    @IBOutlet weak var libraryView: UILabel!
    @IBOutlet weak var cameraLibarary: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
     hideKeyboardWhenTappedAround()
        cameraLibarary.layer.cornerRadius = 15
        cameraLibarary.layer.borderColor = UIColor.oceanColor.cgColor
        cameraLibarary.layer.borderWidth = 1
        libraryView.layer.cornerRadius = 15
        libraryView.layer.backgroundColor = UIColor.oceanColor.cgColor
        libraryView.layer.borderWidth = 1
        hideViewTap()
    }
    @objc func viewHideTapped(_ sender: UITapGestureRecognizer) {
          dismiss(animated: true, completion: nil)
       }
    func hideViewTap() {
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(self.viewHideTapped(_:)))
        self.viewHide.isUserInteractionEnabled = true
        self.viewHide.addGestureRecognizer(viewTap)
    }
}
