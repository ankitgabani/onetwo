//
//  BookingDetailsVC.swift
//  OneTwo
//
//  Created by appt on 17/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class BookingDetailsVC: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var viewPayCor: UIView!
    
    @IBOutlet weak var lblPaymeny: UILabel!
    
    @IBOutlet weak var bookingDetailTbl: UITableView!
   
    @IBOutlet weak var lblDateTop: UILabel!
    @IBOutlet weak var lblDateBootom: UILabel!
    var unchecked = true
    
    var arrPlayerList: [GetPitchesList] = [GetPitchesList]()
    
    var objTop: String?
    var objDate: String?
    
    var ifFromNotification = false
    var arrAddSub = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()

        viewPayCor.layer.cornerRadius = 5
        viewPayCor.layer.borderColor = UIColor(red: 202/255, green: 75/255, blue: 155/255, alpha: 1)
            .cgColor
        viewPayCor.layer.borderWidth = 1
        viewPayCor.clipsToBounds = true
        
      
         hideKeyboardWhenTappedAround()
        
        
        if ifFromNotification == true {
            callBookingPlayerStatusAPI()

        } else {
            callBookingPlayerStatusAPI12()

        }
        
        
        // Do any additional setup after loading the view.
    }
    
    func getDayNameBy(_ today: String) -> String
    {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let date = df.date(from: today)!
        df.dateFormat = "EEEE dd MMMM yyyy"
        return df.string(from: date)
    }

    func getDayHourseBy(_ time: String) -> String
    {
        let df = DateFormatter()
        df.dateFormat = "HH:mm:ss"
        let date = df.date(from: time)!
        df.dateFormat = "h:mm a"
        return df.string(from: date)
    }
    
    @IBAction func didTapBack(_ sender: UIButton) {
        
        if ifFromNotification == true {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        }
        
    }
    @IBAction func addBtn(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(identifier: "ExistingPlayerVC") as! ExistingPlayerVC
        vc.modalTransitionStyle = .coverVertical
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func payShareBtn(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "PayNowVC") as! PayNowVC
        vc.modalTransitionStyle = .coverVertical
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func chatBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ChatVC") as! ChatVC
        vc.modalTransitionStyle = .coverVertical
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func cancelBooking(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "CancelBookingVC") as! CancelBookingVC
        vc.modalTransitionStyle = .coverVertical
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return arrPlayerList.count
        case 1:
            return 1
        case 2:
            return arrAddSub.count
        default:
            return 1
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    @objc func addSubClick(sender: UIButton) {
        
        guard let cell = sender.superview?.superview as? BookingCellOne else {
            return // or fatalError() or whatever
        }
        
        let indexPath = bookingDetailTbl.indexPath(for: cell)
        
        let objCratList = self.arrPlayerList[indexPath!.row]
        let name = objCratList.name ?? ""
        
        self.arrAddSub.add(name)
        self.arrPlayerList.remove(at: indexPath!.row)
        self.bookingDetailTbl.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cellOne = tableView.dequeueReusableCell(withIdentifier: "BookingCellOne", for: indexPath) as! BookingCellOne
            
             let objOrdersList = self.arrPlayerList[indexPath.row]
            
            let name = objOrdersList.name ?? ""
            
            cellOne.lblName.text = name.uppercased()
            
            if unchecked == false {
                cellOne.paidBtn.setImage(UIImage(named:"plusicon-1"), for: .normal)
                cellOne.paidBtn.setTitle("", for: .normal)
                
                cellOne.paidBtn.addTarget(self, action: #selector(addSubClick(sender:)), for: .touchUpInside)
            }
            else {
                cellOne.paidBtn.setTitle("", for: .normal)
                cellOne.paidBtn.setImage(UIImage(named:""), for: .normal)
                cellOne.paidBtn.removeTarget(self, action: #selector(addSubClick(sender:)), for: .touchUpInside)

            }
            return cellOne
        case 1:
            let cellTwo = tableView.dequeueReusableCell(withIdentifier: "BookingCellTwo", for: indexPath) as! BookingCellTwo
            cellTwo.editBtn.tag = indexPath.row
            cellTwo.editBtn.addTarget(self, action: #selector(cellEditBtnTapped(sender:)), for: .touchUpInside)
            if unchecked {
            cellTwo.editBtn.setImage(UIImage(named:"edit-final"), for: .normal)
            }
            else {
                cellTwo.editBtn.setImage(UIImage(named:"doneicon"), for: .normal)
            }
            return cellTwo
        case 2:
            let cellthree = tableView.dequeueReusableCell(withIdentifier: "BookingCellThree", for: indexPath) as! BookingCellThree
            cellthree.lblName.text = arrAddSub.object(at: indexPath.row) as? String
            return cellthree
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BookingCellThree", for: indexPath) as! BookingCellThree
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0 :
            return 45
        case 1 :
            return 80
        default :
            return 45
        }
    }
    @objc
    func cellEditBtnTapped(sender: UIButton) {
        if unchecked {
            sender.setImage(UIImage(named:"doneicon"), for: .normal)
            unchecked = false
            
        }
        else {
            sender.setImage( UIImage(named:"edit-final"), for: .normal)
            unchecked = true
        }
        self.bookingDetailTbl.reloadData()
    }
    
    //MARK:- API
      func callBookingPlayerStatusAPI12() {
           
           self.showLoader()
           
           let booking_id = UserDefaults.standard.value(forKey: "booking_id") as? Int
           
           let param = ["booking_id": booking_id ?? 0]
           
           print(param)
           APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(BOOKING_PLAYER_STATUS, parameters: param, completionHandler: { (response, error, statusCode) in
               
               if error == nil {
                   print("STATUS CODE \(String(describing: statusCode))")
                   print("Response \(String(describing: response))")
                   
                   let message = response?["message"] as? String ?? ""
                   let code = response?["code"] as? Int ?? 0
                   
                   self.hideLoader()
                   if statusCode == 200 {
                       
                       if code == 1 {
                           
                           self.arrPlayerList.removeAll()
                           let dicResponseData = response?.value(forKey: "payload") as? NSDictionary
                           
                           let pitch_name = dicResponseData?.value(forKey: "pitch_name") as? String
                           let pitch_fare = dicResponseData?.value(forKey: "pitch_fare") as? String
                           let booking_date = dicResponseData?.value(forKey: "booking_date") as? String
                           let from = dicResponseData?.value(forKey: "from") as? String
                           
                           let weekday = self.getDayNameBy(booking_date ?? "")
                           print(weekday)
                           
                           let timeing = self.getDayHourseBy(from ?? "")
                           
                           let objTopDate = "\(pitch_name ?? "") @ \(timeing)"
                           self.lblDateTop.text = objTopDate.uppercased()
                           self.lblDateBootom.text = weekday.uppercased()
                           
                           self.lblPaymeny.text = "PAY \(pitch_fare ?? "") KD"
    
                           let arrRespose = dicResponseData?.value(forKey: "player_list_status") as? NSArray
                           
                           for cartList in arrRespose! {
                               let list = GetPitchesList(GetPitchesDictionary: cartList as? NSDictionary)
                               self.arrPlayerList.append(list)
                           }
                           
                           self.bookingDetailTbl.reloadData()
                           
                       } else {
                           self.view.makeToast(message)
                       }
                       
                   } else {
                       
                       self.view.makeToast(message)
                   }
                   
               } else {
                   self.hideLoader()
                   print("Response \(String(describing: response))")
                   let message = response?["message"] as? String ?? ""
                   self.view.makeToast(message)
               }
           })
       }
    
    func callBookingPlayerStatusAPI() {
        
        self.showLoader()
        
        let booking_id = UserDefaults.standard.value(forKey: "booking_id") as? Int
        
        let param = ["booking_id": booking_id ?? 0]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(BOOKING_PLAYER_STATUS, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                if statusCode == 200 {
                    
                    if code == 1 {
                        
                        self.arrPlayerList.removeAll()
                        let dicResponseData = response?.value(forKey: "payload") as? NSDictionary
                        
                        let pitch_name = dicResponseData?.value(forKey: "pitch_name") as? String
                        let pitch_fare = dicResponseData?.value(forKey: "pitch_fare") as? String
                        let booking_date = dicResponseData?.value(forKey: "booking_date") as? String
                        let from = dicResponseData?.value(forKey: "from") as? String
                        
                        let weekday = self.getDayNameBy(booking_date ?? "")
                        print(weekday)
                        
                        let timeing = self.getDayHourseBy(from ?? "")
                        
                        let objTopDate = "\(pitch_name ?? "") @ \(timeing)"
                        self.lblDateTop.text = objTopDate.uppercased()
                        self.lblDateBootom.text = weekday.uppercased()
                        
                        self.lblPaymeny.text = "PAY \(pitch_fare ?? "") KD"
 
                        let arrRespose = dicResponseData?.value(forKey: "player_list_status") as? NSArray
                        
                        for cartList in arrRespose! {
                            let list = GetPitchesList(GetPitchesDictionary: cartList as? NSDictionary)
                            self.arrPlayerList.append(list)
                        }
                        
                        self.bookingDetailTbl.reloadData()
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
}
class BookingCellOne : UITableViewCell {
    @IBOutlet weak var paidBtn: UIButton!
    @IBOutlet weak var lblName: UILabel!
}
class BookingCellTwo : UITableViewCell {
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var lblName: UILabel!
}
class BookingCellThree : UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
}
extension Date {
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized
        // or use capitalized(with: locale) if you want
    }
}
