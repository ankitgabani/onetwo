//
//  PayNowVC.swift
//  OneTwo
//
//  Created by appt on 17/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

class PayNowVC: UIViewController {

    @IBOutlet weak var payShareView: UIView!
    @IBOutlet weak var payFullKentView: UIView!
    @IBOutlet weak var viewOne: UIView!
    @IBOutlet weak var payCashView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        SetUpView()
    }
    func SetUpView() {
        viewOne.layer.cornerRadius = 15
        payCashView.layer.cornerRadius = 15
        payCashView.layer.borderColor = UIColor.pinkColor.cgColor
        payCashView.layer.borderWidth = 2
        payFullKentView.layer.cornerRadius = 15
        payFullKentView.layer.borderColor = UIColor.pinkColor.cgColor
        payFullKentView.layer.borderWidth = 2
        payShareView.layer.cornerRadius = 15
        payShareView.layer.borderColor = UIColor.pinkColor.cgColor
        payShareView.layer.borderWidth = 2
         hideKeyboardWhenTappedAround()
    }
    @IBAction func closeBtn(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
   
    
}
