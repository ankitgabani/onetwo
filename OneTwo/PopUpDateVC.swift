//
//  PopUpDateVC.swift
//  OneTwo
//
//  Created by appt on 14/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
protocol SelectDate {
    func selectDate(Date : String)
}
protocol SelectDateGetPitch {
    func selectDatePitch(Date : String)
}

class PopUpDateVC: UIViewController {
    @IBOutlet weak var viewOne: UIView!
    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    var onSave: ((_ data: String) -> ())?
    var showTimePickerView: Bool = false
    var showDate: Bool = false
    var delegate : SelectDate!
    var delegatePinch: SelectDateGetPitch!
    var isOnlyDate = false
    
    var objOrigleDate: String?
    
    var isSelectedOri = false
    
    var isDOB = false
    var isSeelcteJoin = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isDOB == true {
            datePicker.maximumDate = Date()
        }
        
        if isSelectedOri == true {
            datePicker.minimumDate = Date()
        }
        
        okBtn.layer.cornerRadius = 15
        okBtn.layer.borderWidth = 1
        okBtn.layer.borderColor = UIColor.black.cgColor
        viewOne.layer.cornerRadius = 20
        hideKeyboardWhenTappedAround()
        if showTimePickerView {
            datePicker.datePickerMode = .time
        }
        else {
            datePicker.datePickerMode = .date
        }
    }
    var formateDate: String {
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "yyyy-MM-dd"
        objOrigleDate = formatter1.string(from: datePicker.date)
        
        let formatter = DateFormatter()
        formatter.dateFormat  = "dd MM"
        return formatter.string(from: datePicker.date)
    }
    var formateDateMonth: String{
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "yyyy-MM-dd"
        objOrigleDate = formatter1.string(from: datePicker.date)
        
        let formatter = DateFormatter()
        formatter.dateFormat  = "dd-MMM-yyyy"
        
     
        
        return formatter.string(from: datePicker.date)
    }
    var formateTime: String {
        get {
            let formatter = DateFormatter()
            formatter.timeStyle  = .short
            return formatter.string(from: datePicker.date)
        }
    }
    @IBAction func okActBtn(_ sender: UIButton) {
        
        if isOnlyDate == true {
            if showDate{
                onSave?(formateDateMonth)
                delegate?.selectDate(Date: formateDateMonth)
            }
        } else {
            if showTimePickerView {
                onSave?(formateTime)
                delegate?.selectDate(Date: formateTime)
            }
            else {
                onSave?(formateDate)
                delegate?.selectDate(Date: formateDate)
                
                if isSelectedOri == true {
                    delegatePinch.selectDatePitch(Date: objOrigleDate!)
                }
            }
            if showDate{
                onSave?(formateDateMonth)
                delegate?.selectDate(Date: formateDateMonth)
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
}
