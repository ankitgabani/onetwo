//
//  ExistingTeamListVC.swift
//  OneTwo
//
//  Created by appt on 12/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class ExistingTeamListVC: BaseViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var addTeamList: UIView!
    @IBOutlet weak var totalPlayerView: UILabel!
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var viewOne: UIView!
     var arrExistingTeamList: [GetExistingPlayer] = [GetExistingPlayer]()
    var arrExistingTeamListSearching: [GetExistingPlayer] = [GetExistingPlayer]()
    
    var delegate: AddPlyerTeamForDelegate?
    var isSearching: Bool = false
    
    var isFromBooking: Bool = false
    var objTeamID: String?
    var objTeamName: String?

    var objselectedIndex = Int()
    var objobj = false
    @IBOutlet weak var imgOOP: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        if isFromBooking == true {
            totalPlayerView.text = "BOOK NOW FOR SELECTED TEAM"
            imgOOP.image = UIImage(named: "arrowrightsencod")
        }
        
        searchBar.delegate = self
         hideKeyboardWhenTappedAround()
        searchView.layer.cornerRadius = 20
        viewOne.layer.cornerRadius = 25
        AddListViewTap()
        
        callGetTeamListAPI()
        
        searchBar.addTarget(self, action: #selector(ExistingTeamListVC.textFieldDidChange(_:)),
                                     for: UIControl.Event.editingChanged)
    }
    @IBAction func backBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @objc func addListTapped(_ sender: UITapGestureRecognizer) {
        
        if isFromBooking == true {
            weak var pvc = self.presentingViewController

            self.dismiss(animated: true, completion: {
                let vc = self.storyboard?.instantiateViewController(identifier: "TeamPlayerAddVC") as! TeamPlayerAddVC
                vc.ifFromBookOK = true
                vc.objTeamId = self.objTeamID ?? ""
                vc.objTeamName = self.objTeamName ?? ""
                vc.modalTransitionStyle = .coverVertical
                vc.modalPresentationStyle = .overCurrentContext
                pvc?.present(vc, animated: true, completion: nil)
            })
        } else {

            weak var pvc = self.presentingViewController
            self.dismiss(animated: true, completion: {
                let vc = self.storyboard?.instantiateViewController(identifier: "ExistingPlayerVC") as! ExistingPlayerVC
                vc.modalTransitionStyle = .coverVertical
                vc.modalPresentationStyle = .overCurrentContext
                pvc?.present(vc, animated: true, completion: nil)
            })
        }
        
    }
    
    func AddListViewTap() {
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(self.addListTapped(_:)))
        self.addTeamList.isUserInteractionEnabled = true
        self.addTeamList.addGestureRecognizer(viewTap)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isSearching == true {
            return arrExistingTeamListSearching.count
        } else {
            return arrExistingTeamList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExistingTeamCell", for: indexPath) as! ExistingTeamCell
        
        var objOrdersList: GetExistingPlayer!
        if self.isSearching == true {
            objOrdersList = self.arrExistingTeamListSearching[indexPath.row]
        } else {
            objOrdersList = self.arrExistingTeamList[indexPath.row]
        }
        
        let name = objOrdersList.name ?? ""
        
        cell.lblName.text = name.uppercased()
        
        if objobj == true {
            if objselectedIndex == indexPath.row {
                cell.viewCell.layer.cornerRadius = 18
                cell.viewCell.layer.backgroundColor = UIColor.oceanColor.cgColor
            } else {
                cell.viewCell.layer.cornerRadius = 18
                cell.viewCell.layer.borderWidth = 1
                cell.viewCell.layer.borderColor = UIColor.oceanColor.cgColor
                cell.viewCell.layer.backgroundColor = UIColor.clear.cgColor
            }
            
        } else {
            cell.viewCell.layer.cornerRadius = 18
            cell.viewCell.layer.borderWidth = 1
            cell.viewCell.layer.borderColor = UIColor.oceanColor.cgColor
            cell.viewCell.layer.backgroundColor = UIColor.clear.cgColor
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        objobj = true
                
        var objOrdersList: GetExistingPlayer!
        if self.isSearching == true {
            objOrdersList = self.arrExistingTeamListSearching[indexPath.row]
        } else {
            objOrdersList = self.arrExistingTeamList[indexPath.row]
        }
        
        
        if isFromBooking == false {
            weak var pvc = self.presentingViewController
            
            self.dismiss(animated: true, completion: {
                let vc = self.storyboard?.instantiateViewController(identifier: "TeamPlayerAddVC") as! TeamPlayerAddVC
                vc.objTeamId = objOrdersList.id ?? ""
                vc.objTeamName = objOrdersList.name ?? ""
                vc.modalTransitionStyle = .coverVertical
                vc.modalPresentationStyle = .overCurrentContext
                pvc?.present(vc, animated: true, completion: nil)
            })
        }
        
        self.objTeamID = objOrdersList.id ?? ""
        self.objTeamName = objOrdersList.name ?? ""

        objselectedIndex = indexPath.row
        
        self.tblView.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if self.searchBar.text!.isEmpty {
            
            self.isSearching = false
            
            self.tblView.reloadData()
            
        } else {
            self.isSearching = true
            
            self.arrExistingTeamListSearching.removeAll(keepingCapacity: false)
            
            for i in 0..<self.arrExistingTeamList.count {
                
                let listItem: GetExistingPlayer = self.arrExistingTeamList[i]
                if listItem.name!.lowercased().range(of: self.searchBar.text!.lowercased()) != nil {
                    self.arrExistingTeamListSearching.append(listItem)
                }
            }
            
            self.tblView.reloadData()
        }
        
    }
    
    func callGetTeamListAPI() {
           
          let userID = UserDefaults.standard.value(forKey: "user_id") as? Int
           
           self.showLoader()
           
           let param = ["user_id": userID ?? 0]
           
           print(param)
           APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(GET_EXITING_TEAM_LIST, parameters: param, completionHandler: { (response, error, statusCode) in
               
               if error == nil {
                   print("STATUS CODE \(String(describing: statusCode))")
                   print("Response \(String(describing: response))")
                   
                   let message = response?["message"] as? String ?? ""
                   let code = response?["code"] as? Int ?? 0
                   
                   self.hideLoader()
                   if statusCode == 200 {
                       
                       if code == 1 {
                           
                           self.arrExistingTeamList.removeAll()
                           let arrResponseData = response?.value(forKey: "payload") as? NSArray
                                                   
                           for cartList in arrResponseData! {
                               let list = GetExistingPlayer(GetExistingPlayerDic: cartList as? NSDictionary)
                               self.arrExistingTeamList.append(list)
                           }
                           
                           self.tblView.reloadData()
                           
                       } else {
                           self.view.makeToast(message)
                       }
                       
                   } else {
                       
                       self.view.makeToast(message)
                   }
                   
               } else {
                   self.hideLoader()
                   print("Response \(String(describing: response))")
                   let message = response?["message"] as? String ?? ""
                   self.view.makeToast(message)
               }
           })
       }
}

class ExistingTeamCell: UITableViewCell {
    @IBOutlet weak var viewCell: UIView!
    
    @IBOutlet weak var lblName: UILabel!
    
}
