//
//  HomeVC.swift
//  OneTwo
//
//  Created by Naveen Yadav on 01/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import SideMenuSwift

extension Notification.Name {
    
    public static let myNotificationKeyCancelBook = Notification.Name(rawValue: "myNotificationCancelBook")
}

@available(iOS 13.0, *)
class HomeVC: BaseViewController,SelectDate,SelectDateGetPitch {
   
    
    @IBOutlet weak var viewBG: UIView!
    
    @IBOutlet weak var viewpoup: UIView!
    
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnJoin: UIButton!
    
    @IBOutlet weak var lblView: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var sideMenuBtn: UIButton!
    
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var pitchViewSelection: UIView!
    @IBOutlet weak var ballImage: UIImageView!
    private var processStarted = false
    var pickerView = UIPickerView()
    var gameTimer: Timer!
    
    var ifFromSide = false
    var objDateSelect = String()
    override func viewDidLoad()
    {
        
        
        
        lblView.layer.cornerRadius = 6
        lblView.clipsToBounds = true
        lblView.isHidden = true
        lblTitle.isHidden = true
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        pitchViewSelection.layer.cornerRadius = 15
        pitchViewSelection.layer.borderWidth = 1
        pitchViewSelection.layer.borderColor = UIColor.white.cgColor
        dateView.layer.cornerRadius = 15
        dateView.layer.borderWidth = 1
        dateView.layer.borderColor = UIColor.white.cgColor
        
        NotificationCenter.default.addObserver(self, selector: #selector(onNotification(notification:)), name: Notification.Name.myNotificationKeyCancelBook, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        viewBG.isHidden = true
        viewpoup.isHidden = true
        
        if ifFromSide == true {
            
            if let userLoggedIn = UserDefaults.standard.value(forKey: "isUserLoggedIn") as? Bool
            {
                if userLoggedIn == true {
                    viewBG.isHidden = true
                    viewpoup.isHidden = true
                } else {
                    viewBG.isHidden = false
                    viewpoup.isHidden = false
                }
                
            } else {
                viewBG.isHidden = false
                viewpoup.isHidden = false
            }
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
            viewBG.addGestureRecognizer(tap)
            viewBG.isUserInteractionEnabled = true
            
            viewpoup.layer.cornerRadius = 5
            btnJoin.layer.cornerRadius = 5
            btnLogin.layer.cornerRadius = 5
        }
        
        viewpoup.layer.cornerRadius = 5
        btnJoin.layer.cornerRadius = 5
        btnLogin.layer.cornerRadius = 5

        
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
           viewBG.isHidden = true
           viewpoup.isHidden = true
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
           return .lightContent
       }
    
    //MARK:- Notification
    @objc func onNotification(notification:Notification)
    {
        let objStrPlayerID = notification.userInfo!["text"] as? String

        DispatchQueue.main.async { [weak self] in
            self!.lblView.isHidden = false
            self!.lblTitle.isHidden = false

            self!.view.makeToast("Cancel Booking order successfully")
        }
        
    }
    
    @IBAction func clickedLgoin(_ sender: Any) {
        let home = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: LoginVC.self)) as! LoginVC
        home.isFromHome = true
        self.navigationController?.pushViewController(home, animated: true)
    }
    
    @IBAction func clickedJoin(_ sender: Any) {
        let home = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: RegisterVC.self)) as! RegisterVC
        self.navigationController?.pushViewController(home, animated: true)

    }
    
    @IBAction func notificationBtn(_ sender: UIButton) {
        
        if let user_id = UserDefaults.standard.value(forKey: "user_id") as? Int {
            
            if user_id != 0 {
                
                let vc = self.storyboard?.instantiateViewController(identifier: "NotificationVC") as! NotificationVC
                let transition = CATransition()
                transition.duration = 0.5
                transition.type = CATransitionType.fade
                transition.subtype = CATransitionSubtype.fromLeft
                transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
                view.window!.layer.add(transition, forKey: kCATransition)
                vc.modalPresentationStyle = .overFullScreen
                
                self.sideMenuController?.present(vc, animated: false, completion: nil)
            }            
        }
        
        //self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func menuToggle(_ sender: UIButton) {
        self.sideMenuController?.revealMenu()
    }
    @IBAction func datePopUpBtn(_ sender: Any) {
        if processStarted == false {
            let vc = self.storyboard?.instantiateViewController(identifier: "PopUpDateVC") as! PopUpDateVC
            vc.delegate = self
            vc.delegatePinch = self
            vc.isSelectedOri = true
            vc.showTimePickerView = false
            vc.modalPresentationStyle = .overFullScreen
            vc.view.backgroundColor = .clear
            self.present(vc, animated: true, completion: nil)
        }
    }
    @IBAction func goToPitchList(_ sender: UIButton) {
        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: PitchListVC.self)) as! PitchListVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func selectDate(Date: String) {
        if processStarted == false {
            processStarted = true
            dateLbl.text = Date
            ballImage.loadGif(name: "ball-rotation-without-glow")
             print(Date)
            if gameTimer != nil {
                gameTimer.invalidate()
                gameTimer = nil
                gameTimer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(timeaction), userInfo: nil, repeats: false)
            } else {
                gameTimer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(timeaction), userInfo: nil, repeats: false)
            }
        }
    }
    
    func selectDatePitch(Date: String) {
        self.objDateSelect = Date
        UserDefaults.standard.set(Date, forKey: "SelectedDateHome")
        UserDefaults.standard.synchronize()
    }
    
    @objc func timeaction(){
        //code for move next VC
        processStarted = false
        let secondVC = storyboard?.instantiateViewController(withIdentifier: "PitchListVC") as! PitchListVC
        secondVC.objPitchDate = self.objDateSelect
        self.navigationController?.pushViewController(secondVC, animated: true)
        gameTimer.invalidate()//after that timer invalid
    }
 
}
