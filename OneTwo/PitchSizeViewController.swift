//
//  PitchSizeViewController.swift
//  OneTwo
//
//  Created by appt on 12/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
protocol SelectPitch {
    func SelectPitch(Pitch : String)
}
@available(iOS 13.0, *)
class PitchSizeViewController: BaseViewController ,UITableViewDataSource,UITableViewDelegate{
    
    @IBOutlet weak var tlbView: UITableView!
    
    @IBOutlet weak var selectPitchSize: UILabel!
    
    @IBOutlet weak var view1: UIView!
    @IBAction func backBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
     var delegate : SelectPitch!
    
    var arrPintchList: [GetPitchesList] = [GetPitchesList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view1.layer.cornerRadius = 25
         hideKeyboardWhenTappedAround()
        setupViews()
        
        callGetPitcheListAPI()
    }
    private func setupViews() {
           selectPitchSize.font = UIFont(name: "JerseyM54", size: 20.0)
       }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPintchList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PitchSizeViewCell", for: indexPath) as! PitchSizeViewCell
        
        let objOrdersList = self.arrPintchList[indexPath.row]
        let lenght = objOrdersList.length ?? ""
        let width = objOrdersList.width ?? ""
        
        cell.pitchSizeLbl.text = "\(lenght) * \(width)"
        
        cell.cellView.layer.cornerRadius = 18
        cell.cellView.layer.borderColor = UIColor.oceanColor.cgColor
        cell.cellView.layer.borderWidth = 1
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PitchSizeViewCell", for: indexPath) as! PitchSizeViewCell
        
        let objOrdersList = self.arrPintchList[indexPath.row]
        let lenght = objOrdersList.length ?? ""
        let width = objOrdersList.width ?? ""
        
        let finalStr = "\(lenght) * \(width)"
        
        delegate.SelectPitch(Pitch: finalStr)
        
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:- API
    func callGetPitcheListAPI() {
        
        self.showLoader()
        
        let date = UserDefaults.standard.value(forKey: "SelectedDateHome") as? String
        
        let param = ["date": date ?? ""]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderGet(GET_PITCH_SIZE_LIST, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                if statusCode == 200 {
                    
                    if code == 1 {
                        
                        self.arrPintchList.removeAll()
                        let arrRespose = response?.value(forKey: "payload") as? NSArray
                                                
                        for cartList in arrRespose! {
                            let list = GetPitchesList(GetPitchesDictionary: cartList as? NSDictionary)
                            self.arrPintchList.append(list)
                        }
                        
                        self.tlbView.reloadData()
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
}
class PitchSizeViewCell: UITableViewCell {
    
    @IBOutlet weak var pitchSizeLbl: UILabel!
    
    
    @IBOutlet weak var cellView: UIView!
}
