//
//  Constant.swift
//  TeeduTutor
//
//  Created by Naveen Yadav on 24/03/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

func BASE_URL(child: String) -> String {
    return "http://api.teedu.in/api/"+child
}

//let BASE_URL = "http://onetwoco.inpro3.fcomet.com/dev/api/auth/"
let BASE_URL = "http://one-two.com.kw/dev/api/auth/"

// ---------------------------------------------

let USER_LOGIN = "login"
let USER_SIGUP = "signup"
let My_PROFILE = "my_profile"
let RESET_PASSWORD = "forgot_password"
let GET_VENUE_LIST = "get_pitches"
let GET_VENUE_INFO = "get_pitches_details"
let GET_VENUE_IMAGES = "get_pitch_images"
let SHOW_PROFILE = "my_profile_show"

// ---------------------------------------------
let GET_EXITING_TEAM_LIST = "get_existing_team_list"
let GET_EXITING_PLAYER_LIST = "get_existing_player_list"
let GET_LOYATY_POINT = "get_loyalty_point"
let GET_PITCH_SIZE_LIST = "get_pitch_size_list"
let GET_PITCH_LIST = "get_pitch_list"
let FILTER_PITCH_LIST = "get_filter_search"
let GET_TIME_SLOT = "get_pitch_timeslots"

// ---------------------------------------------
let GET_PLAYERLIST_OF_TEAM = "get_playerlist_of_team"
let MY_PROFILE = "my_profile"
let UPDATE_PLAYER_STATUS = "update_player_status"
let SEND_INVITATION_MULTIPLE = "send_invitation_multiple"
let CREATE_TEAM_WITH_PLAYERS = "create_team_with_players"
let GET_INVITATION_LIST = "get_invitation_list"
let TEAM_REQUEST = "team_request"
let LIVE_MATCHES = "live_matches"
let MATCH_REQEST = "match_request"


// ---------------------------------------------
let RESPONSE_INVITATION_STATUS = "response_invitation_status"

let RESPONSE_TEAM_INVITATION_STATUS = "response_team_invitation_status"

let RESPONSE_MATCH_INVITATION_STATUS = "response_match_invitation_status"

// ---------------------------------------------
// ---------------------------------------------
let CHECK_EXISTING_USER = "chk_userexist_or_not"

let ABOUT_US = "http://onetwoco.inpro3.fcomet.com/dev/api/about_us"

let TnC = "http://onetwoco.inpro3.fcomet.com/dev/api/terms_conditions"

let BOOKING = "booking_order_player"

let CANCEL_BOOKING = "cancel_booking_order"

let BOOKING_PLAYER_STATUS = "booking_player_status"


let SEND_DEVICE_TOKEN = "sendMessage"
// ---------------------------------------------
// ---------------------------------------------

class Constant: NSObject {
     static let APPNAME = Bundle.main.infoDictionary![kCFBundleNameKey as String] as! String
}

class savedKeys: NSObject {
    static let username = "username"
    static let mobile = "mobile"
    static let device_token = "device_token"
    static let api_token = "api_token"
    static let userID = "userID"
    static let profile_image = "profile_image"
    static let email = "email"
    static let status = "status"
    static let lat = "lat"
    static let long = "long"
    
    static let tutorDetail = "tutorDetail"
    static let startDate = "startDate"
    static let quantity = "quantity"
    static let sessionList = "sessionList"
}

class ProjectUrl: NSObject {
    
}

class ProjectColor: NSObject {
    static let darkGreen = UIColor(red: 108/255, green: 173/255, blue: 29/255, alpha: 1.0) //6cad1d
    static let lightGreen = UIColor(red: 149/255, green: 210/255, blue: 74/255, alpha: 1.0) //93d143
    static let yellow = UIColor(red: 233/255, green: 226/255, blue: 77/255, alpha: 1.0) //e9e24d
    static let darkBlue = UIColor(red: 34/255, green: 43/255, blue: 104/255, alpha: 1.0) //222b68
}

class ProjectFont: NSObject {
    static func setFont(size: CGFloat) -> UIFont {
         return UIFont(name: "Quicksand-Regular", size: size)!
    }
}

class ProjectImages: NSObject {
    static let tutorial1 = UIImage(named: "tutorial1")
    static let tutorial2 = UIImage(named: "tutorial2")
    static let tutorial3 = UIImage(named: "tutorial3")
}
