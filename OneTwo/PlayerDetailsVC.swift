//
//  PlayerDetailsVC.swift
//  OneTwo
//
//  Created by appt on 17/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import SideMenuSwift
@available(iOS 13.0, *)
class PlayerDetailsVC: BaseViewController {
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var removeBtn: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    var objUserID: String?
    var objUserName: String?
    var objProfile: String?
    var dalagate: RemovedForDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        removeBtn.layer.cornerRadius = 7
        removeBtn.layer.borderWidth = 2
        removeBtn.layer.borderColor = UIColor.red.cgColor
        // Do any additional setup after loading the view.
        hideKeyboardWhenTappedAround()
        
        self.lblName.text = objUserName
        
        imgProfile.layer.cornerRadius = imgProfile.frame.height / 2
        imgProfile.clipsToBounds = true
        imgProfile.backgroundColor = UIColor.white
        
        if let img = objProfile {
            var finalStr = "http://one-two.com.kw/dev/public/user_picture/\(img)"
            finalStr = finalStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!

            if (finalStr != "")
            {
                if let url = URL(string: finalStr)
                {
                    imgProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "football_men"))
                } else
                {
                    imgProfile.image = UIImage(named: "football_men")
                }
                
            }else
            {
                imgProfile.image = UIImage(named: "football_men")
            }
        }
    }
    
    @IBAction func closeBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func removeBtn(_ sender: UIButton) {
        callUpdatePlayerStatusAPI()
    }
    
    func callUpdatePlayerStatusAPI() {
        
        let userID = UserDefaults.standard.value(forKey: "user_id") as? Int
        
        self.showLoader()
        
        let param = ["player_id": userID ?? 0,"user_id": objUserID ?? "","status":"0"] as [String : Any]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(UPDATE_PLAYER_STATUS, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                if statusCode == 200 {
                    
                    if code == 1 {
                       
                        self.dalagate?.onRemovedForDelegateReady(type: message)
                        self.dismiss(animated: true, completion: nil)

                    } else {
                        self.dalagate?.onRemovedForDelegateReady(type: message)
                        self.dismiss(animated: true, completion: nil)

                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
    
}
