//
//  ChangeDobVC.swift
//  OneTwo
//
//  Created by appt on 11/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import SwiftyPickerPopover
@available(iOS 13.0, *)
class ChangeDobVC: BaseViewController, SelectDate {
    
    @IBOutlet weak var savebtn: UIButton!
    @IBOutlet weak var viewOne: UIView!
    @IBOutlet weak var dateViewTap: UIView!
    @IBOutlet weak var dateTF: UITextField!
    
    @IBOutlet weak var txtNewDOB: UITextField!
    
    var objRealDate = String()
    
    var delegate: LLForDelegate?
    
    var isChanges = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewOne.layer.cornerRadius = 20
        savebtn.layer.cornerRadius = 20
        savebtn.layer.borderColor = UIColor.white.cgColor
        savebtn.layer.borderWidth = 3
        setupViewTap()
        hideKeyboardWhenTappedAround()
        
        callShowProfileAPI()
        //dateTF.text = "\(selectedDate)"
    }
    
    @IBAction func backBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func saveBtnAct(_ sender: UIButton) {
        
        if isChanges == true {
            callChangeDOBAPI()
        } else {
            self.view.makeToast("Please change date of birth")
        }
        
    }
    @objc func viewDateTapped(_ sender: UITapGestureRecognizer) {
        let vc = self.storyboard?.instantiateViewController(identifier: "PopUpDateVC") as! PopUpDateVC
        vc.delegate = self
        vc.showDate = true
        vc.isDOB = true
        self.present(vc, animated: true, completion: nil)
    }
    func setupViewTap() {
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(self.viewDateTapped(_:)))
        self.dateViewTap.isUserInteractionEnabled = true
        self.dateViewTap.addGestureRecognizer(viewTap)
    }
    func selectDate(Date: String) {
        txtNewDOB.text = Date
        isChanges = true
        let charset = CharacterSet(charactersIn: "-")
        if Date.rangeOfCharacter(from: charset) != nil {
            
            var dateFromString = ""
            
            var objMonth = String()
            let arrSplit1 = Date.components(separatedBy: "-")
            let strDay = arrSplit1[0] as! String
            let strMonth = arrSplit1[1] as! String
            let strYear = arrSplit1[2] as! String
            
            if strMonth == "Jul" {
                objMonth = "01"
            } else if strMonth == "Feb" {
                objMonth = "02"
            } else if strMonth == "Mar" {
                objMonth = "03"
            } else if strMonth == "Apr" {
                objMonth = "04"
            } else if strMonth == "May" {
                objMonth = "05"
            } else if strMonth == "Jun" {
                objMonth = "06"
            } else if strMonth == "Jul" {
                objMonth = "07"
            } else if strMonth == "Aug" {
                objMonth = "08"
            } else if strMonth == "Sep" {
                objMonth = "09"
            } else if strMonth == "Oct" {
                objMonth = "10"
            } else if strMonth == "Nov" {
                objMonth = "11"
            } else if strMonth == "Dec" {
                objMonth = "12"
            }
            
            dateFromString = String.init(format: "%@-%@-%@", strYear,objMonth,strDay)
            
            print(dateFromString)
            objRealDate = dateFromString
        }
        
        
        
    }
    
    func callChangeDOBAPI() {
        
        self.showLoader()
        
        let userID = UserDefaults.standard.value(forKey: "user_id") as? Int
        
        let param = ["player_id": userID ?? 0, "dob": objRealDate] as [String : Any]
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(MY_PROFILE, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let IsSuccess = response?["success"] as? Bool
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                
                if statusCode == 200 {
                    
                    if code == 1 {
                        
                        self.delegate?.onLLForDelegateReady(type: message)
                        self.dismiss(animated: true, completion: nil)
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
    
    func callShowProfileAPI() {
           
          // self.showLoader()
           
           let player_id = UserDefaults.standard.value(forKey: "user_id") as? Int
           
           let param = ["player_id": player_id ?? 0]
           
           APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(SHOW_PROFILE, parameters: param, completionHandler: { (response, error, statusCode) in
               
               if error == nil {
                   print("STATUS CODE \(String(describing: statusCode))")
                   print("Response \(String(describing: response))")
                   
                   let message = response?["message"] as? String ?? ""
                   let IsSuccess = response?["success"] as? Bool
                   let code = response?["code"] as? Int ?? 0
                   
                   self.hideLoader()
                   
                if statusCode == 200 {
                    
                    if code == 1 {
                        
                        let objPayload = response?["payload"] as? NSDictionary
                        let dob = objPayload?.value(forKey: "dob") as? String
                        
                        var dateFromString = ""
                        var objMonth = String()
                        let arrSplit1 = dob!.components(separatedBy: "-")
                        let strDay = arrSplit1[0] as! String
                        let strMonth = arrSplit1[1] as! String
                        let strYear = arrSplit1[2] as! String
                        
                        
                        if strMonth == "Jul" {
                            objMonth = "Jul"
                        } else if strMonth == "02" {
                            objMonth = "Feb"
                        } else if strMonth == "03" {
                            objMonth = "Mar"
                        } else if strMonth == "04" {
                            objMonth = "Apr"
                        } else if strMonth == "05" {
                            objMonth = "May"
                        } else if strMonth == "06" {
                            objMonth = "Jun"
                        } else if strMonth == "07" {
                            objMonth = "Jul"
                        } else if strMonth == "08" {
                            objMonth = "Aug"
                        } else if strMonth == "09" {
                            objMonth = "Sep"
                        } else if strMonth == "10" {
                            objMonth = "Oct"
                        } else if strMonth == "11" {
                            objMonth = "Nov"
                        } else if strMonth == "12" {
                            objMonth = "Dec"
                        }
                        
                        dateFromString = String.init(format: "%@-%@-%@", strYear,objMonth,strDay)
                        
                        print(dateFromString)
                        self.dateTF.text = dateFromString
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                       
                       self.view.makeToast(message)
                   }
                   
               } else {
                   self.hideLoader()
                   print("Response \(String(describing: response))")
                   let message = response?["message"] as? String ?? ""
                   self.view.makeToast(message)
               }
           })
       }
}
