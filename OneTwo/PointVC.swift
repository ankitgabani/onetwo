//
//  PointVC.swift
//  OneTwo
//
//  Created by appt on 15/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class PointVC: BaseViewController {
    
    @IBOutlet weak var viewBG: UIView!
    
    @IBOutlet weak var viewPopup: UIView!
    
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var btnJoin: UIButton!
    
    @IBOutlet weak var lblPoint: UILabel!
    
    @IBOutlet weak var viewCont: NSLayoutConstraint!
    
    @IBOutlet weak var viwBottom: UIView!
    
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        viewBG.isHidden = true
        viewPopup.isHidden = true
    }
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        viewBG.addGestureRecognizer(tap)
        viewBG.isUserInteractionEnabled = true
        
        viewPopup.layer.cornerRadius = 5
        btnJoin.layer.cornerRadius = 5
        btnLogin.layer.cornerRadius = 5

        
        hideKeyboardWhenTappedAround()
        
        callPointAPI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        if let userLoggedIn = UserDefaults.standard.value(forKey: "isUserLoggedIn") as? Bool
               {
                   if userLoggedIn == true {
                       viewCont.constant = 0
                       viewBG.isHidden = true
                       viewPopup.isHidden = true
                       viwBottom.isHidden = true
                   } else {
                       viewCont.constant = 35
                       viewBG.isHidden = false
                       viewPopup.isHidden = false
                       viwBottom.isHidden = false
                   }
                   
               } else {
                   viewCont.constant = 35
                   viewBG.isHidden = false
                   viewPopup.isHidden = false
                   viwBottom.isHidden = false

               }
    }
    
    
    @IBAction func clickedBotttom(_ sender: Any) {
        let home = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: LoginVC.self)) as! LoginVC
        home.isFromHome = true
        self.navigationController?.pushViewController(home, animated: true)
    }
    
    @IBAction func clickedLogin(_ sender: Any) {
        let home = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: LoginVC.self)) as! LoginVC
        home.isFromHome = true
        self.navigationController?.pushViewController(home, animated: true)
    }
    
    
    @IBAction func clickedJoin(_ sender: Any) {
        let home = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: RegisterVC.self)) as! RegisterVC
        self.navigationController?.pushViewController(home, animated: true)

    }
    
    
    @IBAction func menuBtn(_ sender: UIButton) {
        self.navigationController?.sideMenuController?.revealMenu()
    }
    @IBAction func notificationBtn(_ sender: UIButton) {
        
       if let user_id = UserDefaults.standard.value(forKey: "user_id") as? Int {
            
            if user_id != 0 {
                
                let vc = self.storyboard?.instantiateViewController(identifier: "NotificationVC") as! NotificationVC
                let transition = CATransition()
                transition.duration = 0.5
                transition.type = CATransitionType.fade
                transition.subtype = CATransitionSubtype.fromLeft
                transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
                view.window!.layer.add(transition, forKey: kCATransition)
                vc.modalPresentationStyle = .overFullScreen
                
                self.sideMenuController?.present(vc, animated: false, completion: nil)
            }
        }
        
        
    }
    
    // MARK: - API Call
    func callPointAPI() {
        
        let userID = UserDefaults.standard.value(forKey: "user_id") as? Int
        
        self.showLoader()
        
        let param = ["user_id": userID ?? 0]
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(GET_LOYATY_POINT, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let IsSuccess = response?["success"] as? Bool
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                
                if code == 1 {
                    let arrResponseData = response?.value(forKey: "payload") as? NSDictionary
                    
                    let yourPoint = arrResponseData?.value(forKey: "points") as? String
                    self.lblPoint.text = yourPoint
                } else {
                    //self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
    
    
}
