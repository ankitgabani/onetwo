//
//  FilterVC.swift
//  OneTwo
//
//  Created by appt on 12/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class FilterVC: BaseViewController, SelectVenue,SelectPitch{
  
    
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var refineBtn: UIButton!
    
    @IBOutlet weak var venueView: UIView!
    @IBOutlet weak var pitchSizeView: UIView!
    
    @IBOutlet weak var TimeLbl: UILabel!
    
    @IBOutlet weak var PitchSizeLbl: UILabel!
    
    @IBOutlet weak var venueLbl: UILabel!
    var delegate: PitchForDelegate?
    
    var objVanue: String?
    var objTime: String?
    var objSize: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        refineBtn.layer.cornerRadius = 20
        refineBtn.layer.borderColor = UIColor.lightGray.cgColor
        refineBtn.layer.borderWidth = 3
        VanueViewTap()
        PitchSizeViewTap()
        TimeViewTap()
        hideKeyboardWhenTappedAround()
    }
    
    
    @IBAction func backBtn(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func refineSearchBtn(_ sender: Any) {
        
        if refineBtn.layer.borderColor == UIColor.lightGray.cgColor {
            
        } else {
            print(self.objTime ?? "")
            print(self.objSize ?? "")
            print(self.objVanue ?? "")
            self.delegate?.onPitchForDelegateReady(time: self.objTime ?? "", size: self.objSize ?? "", venue: self.objVanue ?? "")
            self.dismiss(animated: true, completion: nil)

        }
    }
    
    @objc func viewVenueTapped(_ sender: UITapGestureRecognizer) {
        let vc = self.storyboard?.instantiateViewController(identifier: "SelectVenueViewController") as! SelectVenueViewController
        vc.modalTransitionStyle = .coverVertical
        vc.modalPresentationStyle = .overCurrentContext
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    @objc func viewPitchTapped(_ sender: UITapGestureRecognizer) {
        let vc = self.storyboard?.instantiateViewController(identifier: "PitchSizeViewController") as! PitchSizeViewController
        vc.modalTransitionStyle = .coverVertical
        vc.modalPresentationStyle = .overCurrentContext
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    @objc func viewDateTapped(_ sender: UITapGestureRecognizer) {
        let vc = self.storyboard?.instantiateViewController(identifier: "PopUpDateVC") as! PopUpDateVC
        vc.showTimePickerView = true
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    func VanueViewTap() {
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(self.viewVenueTapped(_:)))
        self.venueView.isUserInteractionEnabled = true
        self.venueView.addGestureRecognizer(viewTap)
    }
    func PitchSizeViewTap() {
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(self.viewPitchTapped(_:)))
        self.pitchSizeView.isUserInteractionEnabled = true
        self.pitchSizeView.addGestureRecognizer(viewTap)
    }
    func TimeViewTap() {
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(self.viewDateTapped(_:)))
        self.timeView.isUserInteractionEnabled = true
        self.timeView.addGestureRecognizer(viewTap)
    }
    func selectVenue(Venue: String, pitch_id: String, pitch_size_id: String) {
        venueLbl.text = Venue.uppercased()
        self.objVanue = pitch_id
        self.objSize = pitch_size_id
        refineBtn.layer.cornerRadius = 20
        refineBtn.layer.borderColor = UIColor.pinkColor.cgColor
        refineBtn.layer.borderWidth = 3
        refineBtn.setTitleColor(UIColor.pinkColor, for: .normal)
    }
     
    func SelectPitch(Pitch: String) {
        PitchSizeLbl.text = Pitch
        refineBtn.layer.cornerRadius = 20
        refineBtn.layer.borderColor = UIColor.pinkColor.cgColor
        refineBtn.setTitleColor(UIColor.pinkColor, for: .normal)
        refineBtn.layer.borderWidth = 3
    }
    
}
@available(iOS 13.0, *)
extension FilterVC : SelectDate {
    
    func selectDate(Date: String) {
        TimeLbl.text = Date
        self.objTime = Date
        refineBtn.layer.cornerRadius = 20
        refineBtn.layer.borderColor = UIColor.pinkColor.cgColor
        refineBtn.setTitleColor(UIColor.pinkColor, for: .normal)
        refineBtn.layer.borderWidth = 3
    }
}
