//
//  AppDelegate.swift
//  OneTwo
//
//  Created by Naveen Yadav on 28/05/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import SideMenuSwift
import Toast_Swift

@available(iOS 13.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let menuViewController = MenuVC()
        let homeVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeNavigation")
        let contentViewController = homeVC
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = SideMenuController(contentViewController: contentViewController,
                                                        menuViewController: menuViewController)
        
        // Override point for customization after application launch.
        var toast_style = ToastStyle()
        toast_style.messageColor = UIColor.white
        toast_style.backgroundColor = UIColor.black
        ToastManager.shared.style = toast_style
        UIApplication.shared.windows.forEach { window in
            window.overrideUserInterfaceStyle = .light
            let localizer : L012Localizer = L012Localizer()
            localizer.DoTheSwizzling()
        }
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    
}

