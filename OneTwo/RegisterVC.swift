//
//  RegisterVC.swift
//  OneTwo
//
//  Created by Naveen Yadav on 01/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import SideMenuSwift
import MBProgressHUD
import MobileCoreServices

import Photos
@available(iOS 13.0, *)
class RegisterVC: BaseViewController, SelectDate, UITextFieldDelegate {
    
    @IBOutlet weak var dateOfBirthFld: UITextField!
    @IBOutlet weak var lastNameFld: UITextField!
    @IBOutlet weak var middleNameFld: UITextField!
    @IBOutlet weak var firstNameFld: UITextField!
    @IBOutlet weak var passwordFld: UITextField!
    @IBOutlet weak var phoneFld: UITextField!
    @IBOutlet weak var emailFld: UITextField!
    @IBOutlet weak var usernameFld: UITextField!
    @IBOutlet weak var dateTF: UITextField!
    @IBOutlet weak var dateViewTap: UIView!
    @IBOutlet weak var checkBtn: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnProfile: UIButton!
    
    var selectedImage = UIImage()
    
    var imagePicker = UIImagePickerController()
    
    var objRealDate = String()
    var unchecked = true
    
    var fileName = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lastNameFld.delegate = self
        middleNameFld.delegate = self
        firstNameFld.delegate = self
        passwordFld.delegate = self
        phoneFld.delegate = self
        emailFld.delegate = self
        usernameFld.delegate = self

        imgProfile.layer.cornerRadius = imgProfile.frame.height / 2
        imgProfile.clipsToBounds = true
        
        imagePicker.delegate = self
        setupViewTap()
        hideKeyboardWhenTappedAround()
        setupViews()
    }
    private func setupViews() {
        dateOfBirthFld.placeholderColor(color: AppColors.sharedInstance.lightGrey.hexToColor)
        lastNameFld.placeholderColor(color: AppColors.sharedInstance.lightGrey.hexToColor)
        middleNameFld.placeholderColor(color: AppColors.sharedInstance.lightGrey.hexToColor)
        firstNameFld.placeholderColor(color: AppColors.sharedInstance.lightGrey.hexToColor)
        passwordFld.placeholderColor(color: AppColors.sharedInstance.lightGrey.hexToColor)
        phoneFld.placeholderColor(color: AppColors.sharedInstance.lightGrey.hexToColor)
        emailFld.placeholderColor(color: AppColors.sharedInstance.lightGrey.hexToColor)
        usernameFld.placeholderColor(color: AppColors.sharedInstance.lightGrey.hexToColor)
        dateTF.placeholderColor(color: AppColors.sharedInstance.lightGrey.hexToColor)
    }
    
    // MARK: - TextField Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == usernameFld {
            emailFld.becomeFirstResponder()
            return false
        } else if textField == emailFld {
            phoneFld.becomeFirstResponder()
            return false
        } else if textField == phoneFld {
            passwordFld.becomeFirstResponder()
            return false
        } else if textField == passwordFld {
            firstNameFld.becomeFirstResponder()
            return false
        } else if textField == firstNameFld {
            middleNameFld.becomeFirstResponder()
            return false
        } else if textField == middleNameFld {
            lastNameFld.becomeFirstResponder()
            return false
        }
        
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    @IBAction func didTapBack(_ sender: UIButton) {
        
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC {
            controller.isFromHome = true
            let viewcontrollers = self.navigationController?.viewControllers
            var isExist = false
            for viewcontroller in viewcontrollers! {
                if viewcontroller.isKind(of: LoginVC.self) {
                    isExist = true
                    break
                }
            }
            if isExist == true {
                self.navigationController?.popViewController(animated: true)
            } else {
                self.navigationController?.viewControllers.insert(controller, at: (viewcontrollers?.count)!)
                self.navigationController?.popToViewController(controller, animated: true)
            }
        }
        
    }
    
    @IBAction func checkBtn(_ sender: UIButton) {
        if unchecked {
            sender.setImage(UIImage(named:"checkwithcheck.png"), for: .normal)
            unchecked = false
        }
        else {
            sender.setImage( UIImage(named:"checkbox.png"), for: .normal)
            unchecked = true
        }
    }
    
    @IBAction func signUpAct(_ sender: UIButton) {
        
        if self.isValidatedLogin() {
            
            if imgProfile.image == nil {
                callSignupAPI()
            } else {
                startUploadImageVideo()

            }
            
        }
        
    }
    
    @IBAction func clickedTermsUse(_ sender: Any) {
//         let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: PVNEWViewController.self)) as! PVNEWViewController
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickedPolicy(_ sender: Any) {
//         let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: PVNEWViewController.self)) as! PVNEWViewController
//        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @objc func viewDateTapped(_ sender: UITapGestureRecognizer) {
        let vc = self.storyboard?.instantiateViewController(identifier: "PopUpDateVC") as! PopUpDateVC
        vc.delegate = self
        vc.showDate = true
        vc.isOnlyDate = true
        vc.isDOB = true
        vc.modalTransitionStyle = .coverVertical
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    func setupViewTap() {
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(self.viewDateTapped(_:)))
        self.dateViewTap.isUserInteractionEnabled = true
        self.dateViewTap.addGestureRecognizer(viewTap)
    }
    func selectDate(Date: String) {
        dateTF.text = Date
        
        print(Date)
        
        var dateFromString = ""
        
        var objMonth = String()
        let arrSplit1 = Date.components(separatedBy: "-")
        let strDay = arrSplit1[0] as! String
        let strMonth = arrSplit1[1] as! String
        let strYear = arrSplit1[2] as! String
        
        if strMonth == "Jul" {
            objMonth = "01"
        } else if strMonth == "Feb" {
            objMonth = "02"
        } else if strMonth == "Mar" {
            objMonth = "03"
        } else if strMonth == "Apr" {
            objMonth = "04"
        } else if strMonth == "May" {
            objMonth = "05"
        } else if strMonth == "Jun" {
            objMonth = "06"
        } else if strMonth == "Jul" {
            objMonth = "07"
        } else if strMonth == "Aug" {
            objMonth = "08"
        } else if strMonth == "Sep" {
            objMonth = "09"
        } else if strMonth == "Oct" {
            objMonth = "10"
        } else if strMonth == "Nov" {
            objMonth = "11"
        } else if strMonth == "Dec" {
            objMonth = "12"
        }
        
        dateFromString = String.init(format: "%@-%@-%@", strYear,objMonth,strDay)
        
        print(dateFromString)
        objRealDate = dateFromString
    }
    
    @IBAction func btnChooseProfile(_ sender: Any) {
        openActionSheet()
    }
    // MARK:- Validation
    func isValidatedLogin() -> Bool {
        
        if usernameFld.text == "" {
            self.view.makeToast("Please enter username")
            return false
        } else if emailFld.text == "" {
            self.view.makeToast("Please enter email address")
            return false
        } else if AppUtilites.isValidEmail(testStr: (emailFld.text)!) == false {
            self.view.makeToast("Please enter email address correctly.")
            return false
        } else if phoneFld.text == "" {
            self.view.makeToast("Please enter mobile number")
            return false
        } else if (phoneFld.text?.count)! < 8 {
            self.view.makeToast("Please enter valid mobile number")
            return false
        } else if passwordFld.text == "" {
            self.view.makeToast("Please enter password")
            return false
        } else if firstNameFld.text == "" {
            self.view.makeToast("Please enter first name")
            return false
        } else if lastNameFld.text == "" {
            self.view.makeToast("Please enter last name")
            return false
        } else if dateOfBirthFld.text == "" {
            self.view.makeToast("Please select Date of Birth")
            return false
        } else if unchecked == true {
            self.view.makeToast("Please check privacy policy & terms of use")
            return false
        }
        return true
    }
    
    // MARK: - API Call
    func callSignupAPI() {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let deviceID = UIDevice.current.identifierForVendor?.uuidString
        
         let param = ["username": usernameFld.text!,"f_name": firstNameFld.text!,"m_name": middleNameFld.text!,"l_name": lastNameFld.text!,"email" : emailFld.text!,"phone": phoneFld.text!,"dob": objRealDate,"bin01": "ccc","bin02":"vvv","password": passwordFld.text!, "device_token": deviceID ?? ""]
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(USER_SIGUP, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let IsSuccess = response?["success"] as? Bool
                let code = response?["code"] as? Int ?? 0
                
                MBProgressHUD.hide(for: self.view, animated: true)
                                    
                    if statusCode == 201 {
                        
                        if code == 1 {
                            let objPayload = response?["payload"] as? NSDictionary
                            let email = objPayload?.value(forKey: "email") as? String
                            let dob = objPayload?.value(forKey: "dob") as? String
                            let f_name = objPayload?.value(forKey: "f_name") as? String
                            let l_name = objPayload?.value(forKey: "l_name") as? String
                            let m_name = objPayload?.value(forKey: "m_name") as? String
                            let phone = objPayload?.value(forKey: "phone") as? String
                            let token = objPayload?.value(forKey: "token") as? String
                            let user_id = objPayload?.value(forKey: "user_id") as? Int
                            let username = objPayload?.value(forKey: "username") as? String
                            let token_type = objPayload?.value(forKey: "token_type") as? String
                            
                            UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
                            UserDefaults.standard.set(token_type, forKey: "token_type")
                            UserDefaults.standard.set(user_id, forKey: "user_id")
                            UserDefaults.standard.set(username, forKey: "username")
                            UserDefaults.standard.set(token, forKey: "token")
                            UserDefaults.standard.set(phone, forKey: "phone")
                            UserDefaults.standard.set(m_name, forKey: "m_name")
                            UserDefaults.standard.set(l_name, forKey: "l_name")
                            UserDefaults.standard.set(f_name, forKey: "f_name")
                            UserDefaults.standard.set(dob, forKey: "dob")
                            UserDefaults.standard.set(email, forKey: "email")
                            UserDefaults.standard.synchronize()
                            
                            let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: SideMenuController.self)) as! SideMenuController
                            SideMenuController.preferences.basic.menuWidth = self.view.frame.width
                            SideMenuController.preferences.basic.statusBarBehavior = .slide
                            SideMenuController.preferences.basic.position = .above
                            SideMenuController.preferences.basic.direction = .left
                            SideMenuController.preferences.basic.supportedOrientations = .portrait
                            SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
                            self.navigationController?.pushViewController(vc, animated: true)
                            
                        } else {
                            self.view.makeToast(message)
                        }
                        
                    } else {
                        self.view.makeToast(message)
                    }
                
                
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
}


//MARK:- UIImagePickerControllerDelegate
@available(iOS 13.0, *)
extension RegisterVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate  {
    
    // MARK: - Camera & Photo Picker
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            UIView.animate(withDuration: 10.5) {
                self.view.endEditing(true)
            }
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.mediaTypes = [kUTTypeImage as String]
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            UIView.animate(withDuration: 10.5) {
                self.view.endEditing(true)
            }
            self.view.makeToast("You don't have camera")
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.mediaTypes = ["public.image"]
        self.present(imagePicker, animated: true, completion: nil)
    
    }
    
    func removeSpecialCharsFromString(text: String) -> String {
         let okayChars = Set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890")
         return text.filter {okayChars.contains($0) }
     }

    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            selectedImage = image
            imgProfile.image = image
            btnProfile.setImage(nil, for: .normal)
        }
        else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            selectedImage = image
            imgProfile.image = image
            btnProfile.setImage(nil, for: .normal)
        }
        
                       
        picker.dismiss(animated: true, completion: nil)
    }
    
    private func openActionSheet(){
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            self.openGallary()
        }))
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler:{ (UIAlertAction)in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    func randomString(length: Int) -> String {
      let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
      return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    func startUploadImageVideo() {
        
        let deviceID = UIDevice.current.identifierForVendor?.uuidString
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let param = ["username": usernameFld.text!,"f_name": firstNameFld.text!,"m_name": middleNameFld.text!,"l_name": lastNameFld.text!,"email" : emailFld.text!,"phone": phoneFld.text!,"dob": objRealDate,"bin01": "ccc","bin02":"vvv","password": passwordFld.text!, "device_token": deviceID ?? ""]
        
        let randomString = self.randomString(length: 15)
 
        print(param)
        APIClient.sharedInstance.postImageToServerWithOutToken(USER_SIGUP, fileName: "Profile\(randomString).jpg", image: selectedImage, parameters: param) { (response, error, statusCode) in
            print("STATUS CODE \(String(describing: statusCode))")
            print("Response \(String(describing: response))")
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if error == nil {
                
                let message = response?["message"] as? String ?? ""
                let IsSuccess = response?["success"] as? Bool
                let code = response?["code"] as? Int ?? 0
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                if statusCode == 201 {
                    
                    if code == 1 {
                        let objPayload = response?["payload"] as? NSDictionary
                        let email = objPayload?.value(forKey: "email") as? String
                        let dob = objPayload?.value(forKey: "dob") as? String
                        let f_name = objPayload?.value(forKey: "f_name") as? String
                        let l_name = objPayload?.value(forKey: "l_name") as? String
                        let m_name = objPayload?.value(forKey: "m_name") as? String
                        let phone = objPayload?.value(forKey: "phone") as? String
                        let picture = objPayload?.value(forKey: "picture") as? String
                        let picture_url = objPayload?.value(forKey: "picture_url") as? String
                        let token = objPayload?.value(forKey: "token") as? String
                        let user_id = objPayload?.value(forKey: "user_id") as? Int
                        let username = objPayload?.value(forKey: "username") as? String
                        let token_type = objPayload?.value(forKey: "token_type") as? String

                        UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
                        UserDefaults.standard.set(token_type, forKey: "token_type")
                        UserDefaults.standard.set(user_id, forKey: "user_id")
                        UserDefaults.standard.set(username, forKey: "username")
                        UserDefaults.standard.set(token, forKey: "token")
                        UserDefaults.standard.set(picture_url, forKey: "picture_url")
                        UserDefaults.standard.set(picture, forKey: "picture")
                        UserDefaults.standard.set(phone, forKey: "phone")
                        UserDefaults.standard.set(m_name, forKey: "m_name")
                        UserDefaults.standard.set(l_name, forKey: "l_name")
                        UserDefaults.standard.set(f_name, forKey: "f_name")
                        UserDefaults.standard.set(dob, forKey: "dob")
                        UserDefaults.standard.set(email, forKey: "email")
                        UserDefaults.standard.synchronize()
                        
                        let vc = self.storyBoardMain().instantiateViewController(withIdentifier: String(describing: SideMenuController.self)) as! SideMenuController
                        SideMenuController.preferences.basic.menuWidth = self.view.frame.width
                        SideMenuController.preferences.basic.statusBarBehavior = .slide
                        SideMenuController.preferences.basic.position = .above
                        SideMenuController.preferences.basic.direction = .left
                        SideMenuController.preferences.basic.supportedOrientations = .portrait
                        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    self.view.makeToast(message)
                }
                
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        }
        
    }
}

/*
 
 STATUS CODE Optional(201)
 Response Optional({
     code = 1;
     message = "Successfully created user!";
     payload =     {
         dob = "1997-03-15";
         email = "ankit77@gmail.com";
         "f_name" = Ankit;
         "l_name" = Dev;
         "m_name" = iOS;
         phone = 96320145;
         picture = "photo.jpg";
         "picture_url" = "public/user_picture/photo.jpg";
         token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMjE1NDhlOTRhNGI1YTU0MWU4YTFhMGVhOTUxNjUzODdlMjM0MGZiN2IyY2E4MWZmYjhjMzFjYzdiMzgwZmMzZTEwNWZiMzg1OWU1M2I2ZjEiLCJpYXQiOjE1OTYxOTY5MzYsIm5iZiI6MTU5NjE5NjkzNiwiZXhwIjoxNjI3NzMyOTM2LCJzdWIiOiI2MyIsInNjb3BlcyI6W119.pK_ssFvf6ygTDoweAPQg7UdY-jzIURsrAnYcU2HXIarKucHDpQyWfH0jUJtQswqlPhR5yoZxSKKD4Fl-xOqAAzFgLstGTMYuMHkt9Y6J9cI0h32vsIBSXS3856nBIcWXnmLyh4-s8q0oouumZOzH1fNyWU_kVReErzLNecRzGf7XNkhPwPE4a9AVzUPlTmhZehZNLf6q533bHJUFAbUxh1Mb6l_wxtBCFezcPSJcLYWYdSubW3I3cqcsQ8PCqLOS7J_bcPpQ-G2W5hmC2HVSbQ53RVzSrkWADPns6b6vU7QNM-xo-rDCWMpRc-9msrpwNsoemSYLIW_n58j8KEMg8zOCFofg_MGNTNzDIcLiokbOIHzZtrg4yoEE-ukx1S1csvgPwhBswuO9F_a28zz4LDu67SEkhKLwg1g95To7ulHOqbT6UDNZ34vOzQNZOE637ryVBCNiewkSWKZ5VazAyg34ekYFG5_0YdcRKPeE6P-gDSFPlreXQTB48akvsNyn6YM6abpimlk6sf44iq8GIyO38S8kiG56Qarx4hY7cBilN0mx_ROUXUELV08O-UuAcPi0mFrGk--7KeWZ0gcLssmkna7d0lihdyGGJZTS2xXZQHHXZAmq-SlJDExwT5oSuvxgPhZJINYARGK9wx5OxX9dL1F9aARWnK1yS_Hvi3M";
         "token_type" = Bearer;
         "user_id" = 63;
         username = 021660;
     };
     success = true;
 })

 
 */
