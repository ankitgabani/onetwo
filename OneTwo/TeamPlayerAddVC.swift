//
//  TeamPlayerAddVC.swift
//  OneTwo
//
//  Created by appt on 22/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
import Contacts
@available(iOS 13.0, *)
class TeamPlayerAddVC: BaseViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var insertName: UITextField!
    @IBOutlet weak var addPlayerView: UIView!
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var viewOne: UIView!
    
    @IBOutlet weak var lblBottomFix: UILabel!
    
    @IBOutlet weak var imgBotomFix: UIImageView!
    
    
    var arrExistingPlayerList: [GetExistingPlayer] = [GetExistingPlayer]()
    var arrExistingPlayeListSearching: [GetExistingPlayer] = [GetExistingPlayer]()
    
    var arrIDColl = NSMutableArray()
    var arrNameColl = NSMutableArray()

    var isSearching: Bool = false
    var objTeamId: String?
    var objTeamName: String?
    
    var ifFromBookOK = false
    
    
    var objPitch_id: String?
    var objPitch_Size_id: String?
    var objTimeSlotId: String?
    var objPicthcName: String?
    var objTime: String?

    override func viewDidLoad() {
        super.viewDidLoad()
         
        
        if ifFromBookOK == true {
            imgBotomFix.image = UIImage(named: "arrowrightsencod")
            lblBottomFix.text = "BOOK NOW FOR TEAM PLAYER"
        }
        
        searchBar.delegate = self
        
        searchView.layer.cornerRadius = 20
        viewOne.layer.cornerRadius = 25
        hideKeyboardWhenTappedAround()
        callGetTeamListAPI()
        
        tblView.allowsMultipleSelection = true

        
        let teamName = objTeamName ?? ""
        insertName.text = teamName.uppercased()
                
        searchBar.addTarget(self, action: #selector(TeamPlayerAddVC.textFieldDidChange(_:)),
        for: UIControl.Event.editingChanged)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
           
           if self.searchBar.text!.isEmpty {
               
               self.isSearching = false
               
               self.tblView.reloadData()
               
           } else {
               self.isSearching = true
               
               self.arrExistingPlayeListSearching.removeAll(keepingCapacity: false)
               
               for i in 0..<self.arrExistingPlayerList.count {
                   
                   let listItem: GetExistingPlayer = self.arrExistingPlayerList[i]
                   if listItem.name!.lowercased().range(of: self.searchBar.text!.lowercased()) != nil {
                       self.arrExistingPlayeListSearching.append(listItem)
                   }
               }
               
               self.tblView.reloadData()
           }
    }
           
    @IBAction func btnPlayerList(_ sender: Any) {
        
        if ifFromBookOK == true {
            callBookingOderAPI()
        } else {

             weak var pvc = self.presentingViewController
            self.dismiss(animated: true, completion: {
                let vc = self.storyboard?.instantiateViewController(identifier: "ExistingPlayerVC") as! ExistingPlayerVC
                vc.objTeamId = self.objTeamId
                vc.isFromGroup = true
                vc.modalTransitionStyle = .coverVertical
                vc.modalPresentationStyle = .overCurrentContext
                pvc?.present(vc, animated: true, completion: nil)
            })
        }
        
    }
    
    @IBAction func backBtn(_ sender: Any) {
        // self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        dismiss(animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isSearching == true {
            return arrExistingPlayeListSearching.count
        } else {
            return arrExistingPlayerList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerCell", for: indexPath) as! PlayerCell
        
        var objOrdersList: GetExistingPlayer!
        if self.isSearching == true {
            objOrdersList = self.arrExistingPlayeListSearching[indexPath.row]
        } else {
            objOrdersList = self.arrExistingPlayerList[indexPath.row]
        }
        
        let name = objOrdersList.name ?? ""
        
        cell.lblName.text = name.uppercased()
 
        if ifFromBookOK == true {
            cell.viewCell.layer.cornerRadius = 18
            cell.viewCell.layer.borderWidth = 1
            cell.viewCell.layer.borderColor = UIColor.oceanColor.cgColor
 
        } else
        {
            cell.viewCell.layer.cornerRadius = 18
            cell.viewCell.layer.backgroundColor = UIColor.oceanColor.cgColor
            
        }
 
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                
        let cell = tblView.cellForRow(at: indexPath) as! PlayerCell
        
        var objOrdersList: GetExistingPlayer!
        if self.isSearching == true {
            objOrdersList = self.arrExistingPlayeListSearching[indexPath.row]
        } else {
            objOrdersList = self.arrExistingPlayerList[indexPath.row]
        }
        
        if ifFromBookOK == true {
            if cell.viewCell.layer.backgroundColor == UIColor.oceanColor.cgColor {
                cell.viewCell.layer.cornerRadius = 18
                cell.viewCell.layer.backgroundColor = UIColor.clear.cgColor
                cell.viewCell.layer.borderWidth = 1
                cell.viewCell.layer.borderColor = UIColor.oceanColor.cgColor
                self.arrIDColl.remove(objOrdersList.player_id ?? "")
                self.arrNameColl.remove(objOrdersList.name ?? "")
                self.lblTitle.text = "TOTAL NO. OF SELECTIONS (\(self.arrNameColl.count))"
                
            } else {
                cell.viewCell.layer.cornerRadius = 18
                cell.viewCell.layer.backgroundColor = UIColor.oceanColor.cgColor
                cell.viewCell.layer.borderWidth = 1
                cell.viewCell.layer.borderColor = UIColor.oceanColor.cgColor
                self.arrIDColl.add(objOrdersList.player_id ?? "")
                self.arrNameColl.add(objOrdersList.name ?? "")
                self.lblTitle.text = "TOTAL NO. OF SELECTIONS (\(self.arrNameColl.count ))"
            }
            
            self.tblView.reloadData()

        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func callGetTeamListAPI() {
        
        self.showLoader()
        
        let param = ["team_id": objTeamId ?? ""]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(GET_PLAYERLIST_OF_TEAM, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                if statusCode == 200 {
                    
                    if code == 1 {
                        
                        self.arrExistingPlayerList.removeAll()
                        let arrResponseData = response?.value(forKey: "payload") as? NSArray
                        
                        for cartList in arrResponseData! {
                            let list = GetExistingPlayer(GetExistingPlayerDic: cartList as? NSDictionary)
                            self.arrExistingPlayerList.append(list)
                        }
                                                
                        self.tblView.reloadData()
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }
    
    func callBookingOderAPI() {
           
           let userID = UserDefaults.standard.value(forKey: "user_id") as? Int
           let date = UserDefaults.standard.value(forKey: "SelectedDateHome") as? String
         
        let objPitch_id1 = UserDefaults.standard.value(forKey: "objPitch_id")
        let objTimeSlotId1 = UserDefaults.standard.value(forKey: "objTimeSlotId")
        let objPitch_Size_id1 = UserDefaults.standard.value(forKey: "objPitch_Size_id")

        
        
           let set = NSSet(array: arrIDColl as! [Any])
           print(set)
           let strings1 = set.allObjects as? [String] // or as!
           
           let finalStr = strings1?.joined(separator: ",")
           
           self.showLoader()
           
           let param = ["user_id": userID ?? 0,"pitch_id": objPitch_id1 ?? "","timeslot_id": objTimeSlotId1 ?? "","pitch_sizes_id": objPitch_Size_id1 ?? "","player_list": finalStr ?? "","booking_date": date ?? ""] as [String : Any]
           
           print(param)
           APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(BOOKING, parameters: param, completionHandler: { (response, error, statusCode) in
               
               if error == nil {
                   print("STATUS CODE \(String(describing: statusCode))")
                   print("Response \(String(describing: response))")
                   
                   let message = response?["message"] as? String ?? ""
                   let code = response?["code"] as? Int ?? 0
                   
                   self.hideLoader()
                   if statusCode == 200 {
                       
                       if code == 1 {
                           let dicPlayload = response?.value(forKey: "payload") as? NSDictionary
                           
                           let str_BookID = dicPlayload?.value(forKey: "booking_id") as? Int
                           
                           UserDefaults.standard.set(str_BookID, forKey: "booking_id")
                           UserDefaults.standard.synchronize()
                           
                           let vc = self.storyboard?.instantiateViewController(identifier: "BookingDetailsVC") as! BookingDetailsVC
                           vc.objTop = "\(self.objPicthcName ?? "") @ \(self.objTime ?? "")"
                           vc.objDate = date
                           vc.modalTransitionStyle = .coverVertical
                           vc.modalPresentationStyle = .overCurrentContext
                           vc.modalPresentationStyle = .fullScreen
                           self.present(vc, animated: true, completion: nil)
                           self.hideKeyboardWhenTappedAround()

                           
                           self.view.makeToast(message)
                       } else {
                           self.view.makeToast(message)
                       }
                       
                   } else {
                       self.view.makeToast(message)
                   }
                   
               } else {
                   self.hideLoader()
                   print("Response \(String(describing: response))")
                   let message = response?["message"] as? String ?? ""
                   self.view.makeToast(message)
               }
           })
       }
}

class PlayerCell : UITableViewCell {
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var lblName: UILabel!
    
}
