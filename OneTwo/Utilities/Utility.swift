//
//  Utility.swift
//  GTS
//
//  Created by appt on 6/12/19.
//  Copyright © 2019 Apptology. All rights reserved.
//

import UIKit
class Utility: NSObject {
    
    static func imageOrientation(_ src:UIImage)->UIImage {
        if src.imageOrientation == UIImage.Orientation.up {
            return src
        }
        var transform: CGAffineTransform = CGAffineTransform.identity
        switch src.imageOrientation {
        case UIImage.Orientation.down, UIImage.Orientation.downMirrored:
            transform = transform.translatedBy(x: src.size.width, y: src.size.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
            break
        case UIImage.Orientation.left, UIImage.Orientation.leftMirrored:
            transform = transform.translatedBy(x: src.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(Double.pi/2))
            break
        case UIImage.Orientation.right, UIImage.Orientation.rightMirrored:
            transform = transform.translatedBy(x: 0, y: src.size.height)
            transform = transform.rotated(by: CGFloat(-Double.pi/2))
            break
        case UIImage.Orientation.up, UIImage.Orientation.upMirrored:
            break
        }
        
        switch src.imageOrientation {
        case UIImage.Orientation.upMirrored, UIImage.Orientation.downMirrored:
            transform.translatedBy(x: src.size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case UIImage.Orientation.leftMirrored, UIImage.Orientation.rightMirrored:
            transform.translatedBy(x: src.size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case UIImage.Orientation.up, UIImage.Orientation.down, UIImage.Orientation.left, UIImage.Orientation.right:
            break
        }
        
        let ctx:CGContext = CGContext(data: nil, width: Int(src.size.width), height: Int(src.size.height), bitsPerComponent: (src.cgImage)!.bitsPerComponent, bytesPerRow: 0, space: (src.cgImage)!.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch src.imageOrientation {
        case UIImage.Orientation.left, UIImage.Orientation.leftMirrored, UIImage.Orientation.right, UIImage.Orientation.rightMirrored:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.height, height: src.size.width))
            break
        default:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.width, height: src.size.height))
            break
        }
        
        let cgimg:CGImage = ctx.makeImage()!
        let img:UIImage = UIImage(cgImage: cgimg)
        
        return img
    }
    func getDifferencebetweenTwoDates(fromDate: String, toDate: String) -> Int {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let from = dateFormatter.date(from: fromDate)
        let to = dateFormatter.date(from: toDate)
        if let fromD = from , let toD = to {
            return Calendar.current.dateComponents([.hour], from: fromD, to: toD).hour ?? 0
        }
        return 0
    }
    
    //MARK:- JSON request
//    func getJSONData(memberData: eventInterestForm) -> Any {
//        //Create JSON
//        var json: Any?
//        let encodedData = try? JSONEncoder().encode(memberData)
//
//        if let data = encodedData {
//            json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
//            //            if let json = json {
//            //            }
//        }
//        return json!
//    }
    
    //MARK:- Date Functions
    class func GetStringFromDate(date: Date, format: String) -> String {
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = format
        formatter.timeZone = .current     //american time zone

        let myString = formatter.string(from: date) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = format

        let locale = LanguageManager.sharedInstance.getSelectedLocale()
        if locale == "ar" {
            formatter.locale = Locale(identifier: "en")
        }
        else {
            formatter.locale = Locale(identifier: "en")
        }

        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)

        return myStringafd
    }
    
    class func GetStringFromDateEnglish(date: Date, format: String) -> String {
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = format
        formatter.timeZone = .current     //american time zone
        
        let myString = formatter.string(from: date) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = format

        formatter.locale = Locale(identifier: "en")
        
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        
        return myStringafd
    }
    
    class func GetDateFromString(stringDate: String, format: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = .current
        let locale = LanguageManager.sharedInstance.getSelectedLocale()
        if locale == "ar" {
            dateFormatter.locale = Locale(identifier: "en")
        }
        else {
            dateFormatter.locale = Locale(identifier: "en")
        }
        guard let date = dateFormatter.date(from:stringDate) else {
           fatalError("ERROR: Date conversion failed due to mismatched format.")
        }
        return date
    }
    
    class func convertFarenheit(toCelcius: Int) -> Double {
        let Fval = toCelcius
        let Cval = Double(Fval - 32) * 0.5
        return Cval
    }
}
struct AppColors {
    static let sharedInstance = AppColors()
    var customBlack: String = "121312"
    var grey: String = "A8A8A4"
    var venueGrey: String = "434444"
    var lightGrey: String = "8A8D88"
    var darkGrey: String = "272626"
    var red: String = "ED2024"
    var oceanGreen: String = "C9DD7B"
    var orange: String = "F36E21"
    var pink: String = "CA4B9B"
    var dialogBg: String = "1a1818"
    var searchBg: String = "DAD9D2"
}
