//
//  SelectTimeVC.swift
//  OneTwo
//
//  Created by appt on 12/06/20.
//  Copyright © 2020 Naveen Yadav. All rights reserved.
//

import UIKit
protocol SelectTime {
    func selectTime(time : String, id: String)
}
@available(iOS 13.0, *)
class SelectTimeVC: BaseViewController,UITableViewDataSource,UITableViewDelegate {
   
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var selectTimeLbl: UILabel!
    @IBOutlet weak var view1: UIView!
    var delegate : SelectTime!
    
    var objVanuePitchID: String?
    
    var arrTimeSlotList: [GetPitchesList] = [GetPitchesList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view1.layer.cornerRadius = 25
         hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
        setupViews()
        
        callGetPitchTimeslotsAPI()
    }
    private func setupViews() {
        selectTimeLbl.font = UIFont(name: "JerseyM54", size: 20.0)
    }
    @IBAction func backBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    func getDayHourseBy(_ time: String) -> String
    {
        let df = DateFormatter()
        df.dateFormat = "HH:mm:ss"
        let date = df.date(from: time)!
        df.dateFormat = "h:mm a"
        return df.string(from: date)
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTimeSlotList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectTimeCell", for: indexPath) as! SelectTimeCell
        
        let objOrdersList = self.arrTimeSlotList[indexPath.row]
        
//        var dateFromString = ""
//        if let createDateStr = objOrdersList.slot
//        {
//            let arrSplit2 = createDateStr.components(separatedBy: ":")
//            let strHour = arrSplit2[0] as! String
//            let strMin = arrSplit2[1] as! String
//
//            dateFromString = String.init(format: "%@:%@", strHour,strMin)
//        }
        
        cell.timeSelect.text = getDayHourseBy(objOrdersList.slot!)
      
       // cell.timeSelect.text = "\(dateFromString) AM"
        
        if objOrdersList.is_booked == "0" {
            cell.cellView.layer.cornerRadius = 18
            cell.cellView.layer.borderColor = UIColor.oceanColor.cgColor
            cell.cellView.layer.borderWidth = 1
            cell.cellView.layer.backgroundColor = UIColor.oceanColor.cgColor

        } else {
            cell.cellView.layer.cornerRadius = 18
            cell.cellView.layer.borderColor = UIColor.oceanColor.cgColor
            cell.cellView.layer.borderWidth = 1
        }
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objOrdersList = self.arrTimeSlotList[indexPath.row]
        
        if objOrdersList.is_booked == "1" {
            
            dismiss(animated: true) {
                let cell = self.tblView.cellForRow(at: indexPath) as! SelectTimeCell
                let objOrdersList = self.arrTimeSlotList[indexPath.row]

                self.delegate.selectTime(time: cell.timeSelect.text!, id: objOrdersList.id ?? "")
            }

        } else {
            self.view.makeToast("This slot is booked")
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    //MARK:- API
    func callGetPitchTimeslotsAPI() {
        
       // self.showLoader()
        let date = UserDefaults.standard.value(forKey: "SelectedDateHome") as? String
        let param = ["date": date ?? "","venue_pitch_id": objVanuePitchID ?? ""]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(GET_TIME_SLOT, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["message"] as? String ?? ""
                let code = response?["code"] as? Int ?? 0
                
                self.hideLoader()
                if statusCode == 200 {
                    
                    if code == 1 {
                        
                        let dicResponseData = response?.value(forKey: "payload") as? NSDictionary
                        
                        let arrMorning = dicResponseData?.value(forKey: "Morning") as? NSArray
                        let arrEvening = dicResponseData?.value(forKey: "Evening") as? NSArray
                        let arrAfternoon = dicResponseData?.value(forKey: "Afternoon") as? NSArray

                        for cartList in arrMorning! {
                            let list = GetPitchesList(GetPitchesDictionary: cartList as? NSDictionary)
                            
                            self.arrTimeSlotList.append(list)
                        }
                        
                        for cartList in arrAfternoon! {
                            let list = GetPitchesList(GetPitchesDictionary: cartList as? NSDictionary)
                            
                            self.arrTimeSlotList.append(list)
                        }
                        
                        for cartList in arrEvening! {
                            let list = GetPitchesList(GetPitchesDictionary: cartList as? NSDictionary)
                            
                             self.arrTimeSlotList.append(list)
                        }
                        
                        self.tblView.reloadData()
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    self.view.makeToast(message)
                }
                
            } else {
                self.hideLoader()
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                self.view.makeToast(message)
            }
        })
    }

    
}
class SelectTimeCell: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
     @IBOutlet weak var timeSelect: UILabel!
    override func awakeFromNib() {
        setupViews()
    }
    private func setupViews() {
        timeSelect.text = "6:30 PM"
    }
    
}
