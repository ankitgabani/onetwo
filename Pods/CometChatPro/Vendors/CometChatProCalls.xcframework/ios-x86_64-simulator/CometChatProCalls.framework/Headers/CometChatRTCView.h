//
//  NSObject+CometChatRTCView.h
//  CometChatRTC
//
//  Created by Jitvar on 1/30/20.
//  Copyright © 2020 Microsoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
//#import <React/RCTRootView.h>

NS_ASSUME_NONNULL_BEGIN

@protocol RTCEventDelegate <NSObject>
@required
- (void)onCallEndedFromRTC;
- (void)onCallEndButtonPressedFromRTC;
@end

@interface CometChatRTCViewBuilder : NSObject

@property (nonatomic,copy) NSString *sessionID;
@property (nonatomic,copy) NSString *mode;
@property (nonatomic,copy) NSString *region;
@property (nonatomic) BOOL defaultLayout;
@property (nonatomic) BOOL isAudioOnly;
@property (nonatomic) BOOL isInitiator;
@property (nonatomic) BOOL muteAudioButtonDisable;
@property (nonatomic) BOOL endCallButtonDisable;
@property (nonatomic) BOOL pauseVideoButtonDisable;
@property (nonatomic) BOOL switchCameraButtonDisable;
@property (nonatomic) BOOL audioModeButtonDisable;
@property (nonatomic) BOOL isConference;
@property (nonatomic) BOOL isSingleMode;
@property (nonatomic) NSMutableDictionary *rtcUser;
@property (nonatomic) NSMutableDictionary *rtcReveiver;
@property (nonatomic) NSMutableDictionary *rtcInitiator;

@property (weak, nonatomic) UIView *view;
@end


@interface CometChatRTCView : NSObject

@property (nonatomic,copy,readonly) NSString *sessionID;
@property (nonatomic,copy) NSString *mode;
@property (nonatomic,readonly) BOOL defaultLayout;
@property (nonatomic,copy) NSString *region;
@property (weak, nonatomic) UIView *view;
@property (nonatomic) BOOL isAudioOnly;
@property (nonatomic) BOOL isInitiator;
@property (nonatomic) BOOL muteAudioButtonDisable;
@property (nonatomic) BOOL endCallButtonDisable;
@property (nonatomic) BOOL pauseVideoButtonDisable;
@property (nonatomic) BOOL switchCameraButtonDisable;
@property (nonatomic) BOOL audioModeButtonDisable;
@property (nonatomic) BOOL isConference;
@property (nonatomic) BOOL isSingleMode;
@property (nonatomic) NSMutableDictionary *rtcUser;
@property (nonatomic) NSMutableDictionary *rtcReveiver;
@property (nonatomic) NSMutableDictionary *rtcInitiator;
@property (nonatomic,weak)NSObject<RTCEventDelegate>* delegate;
//@property (nonatomic, weak) id <RTCEventDelegate> delegate;

-(instancetype)initWithBuilder:(CometChatRTCViewBuilder *)builder;

-(void)startSession;
-(void)endCallSession;
-(void)switchCameraSource;
-(void)muteAudio;
-(void)unMuteAudio;
-(void)pauseVideo;
-(void)unPauseVideo;
-(void)setAudioModeToSpeaker;
-(void)setAudioModeToEarPiece;

@end

NS_ASSUME_NONNULL_END
