//
//  CallingEvents.h
//  RTCCometChat
//
//  Created by Nishant Tiwari on 15/04/20.
//  Copyright © 2020 Nishant Tiwari. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CallingEvents : NSObject

//-(void)callingEventOfCallEnded;
-(void)callingEventOfCallEndButtonPressed;

//-(void)RTCEvents:(void (^) (NSDictionary *response))onCallEnded
//onCallEndButtonPressed:(void (^) (NSDictionary *response))onCallEndButtonPressed;

-(void)RTCEvents:(void (^) (void))onCallEndButtonPressed;
//onCallEndButtonPressed:(void (^) (void))onCallEndButtonPressed;

-(void)newRTCEvents:(NSString* (^) (NSString*))onCallEndButtonPressed;

@end

NS_ASSUME_NONNULL_END
